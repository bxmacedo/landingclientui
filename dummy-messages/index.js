"use strict";

const WebSocket = require('ws');
const TICKER = 30;
const TRADE = 60;
const ORDER = 100;
let counter = 0;

class DummyServer {

    constructor(port) {
        this.port = port;
        this.orders = {};
        this.websocket = new WebSocket.Server({
            port: 8005
        });

        this.websocket.broadcast = function (data) {
            this.websocket.clients.forEach(function (client) {
                if (client.readyState == WebSocket.OPEN) {
                    client.send(data);
                }
            });
        }.bind(this);

        this.websocket.on('connection', function (ws, req) {
            console.log('Client connected: ' + req.connection.remoteAddress);
        });
    }

    start(frequencyPerMinute, symbol) {
        this.symbol = symbol;
        const milliseconds = 60000 / frequencyPerMinute;
        this.timer(this.sendDummyMessage.bind(this), milliseconds);
    }

    timer(callback, milliseconds) {
        setTimeout(() => {
            callback();
            this.timer(callback, milliseconds);
        }, milliseconds);
    }

    sendDummyMessage() {
        const rank = Math.random() * 100;
        let message = {};

        const randValue = function (min, max) {
            let uniform = Math.random();
            let beta = Math.pow(Math.sin(uniform * (Math.PI / 2.0)), 2.0);
            let beta_small = (beta < 0.5) ? 2.0 * beta : 2.0 * (1.0 - beta);
            return (min + (beta_small * (max - min))).toFixed(2);
        };

        /*if (Math.random() * 100 < 50) {
            this.symbol = 'ETH';
        } else {
            this.symbol = 'BTC';
        }
*/
        if (rank < TICKER) {
            message = {
                'MessageId': parseFloat(randValue(1.0, 100.0)),
                'MessageType': 'Ticker',
                'Symbol': this.symbol + ':BRL',
                'Ticker': {
                    "Symbol": this.symbol,
                    "LastPrice": randValue(5000.0, 10000.0),
                    "NetChange": randValue(-100.00, 100.00),
                    'TradeVolume': randValue(5000.0, 10000.0)
                }
            };
        } else if (rank < TRADE) {
            message = {
                'MessageId': parseFloat(randValue(1.0, 100.0)),
                'MessageType': 'Trade',
                'Symbol': this.symbol + ':BRL',
                'Trade': {
                    'Symbol': this.symbol,
                    'Size': parseFloat(randValue(5.0, 50.0)),
                    'Amount': parseFloat(randValue(0.0, 50.0)),
                    'Price': parseFloat(randValue(1, 32500.0)),
                    'TimeStamp': Date.now()
                }
            };
        } else {
            // const orderId = String(parseInt(Math.random() * 500.0));
            const orderId = counter++;
            if (orderId in this.orders) {
                message = {
                    'MessageId': parseFloat(randValue(1.0, 100.0)),
                    'MessageType': 'CancelOrder',
                    'Symbol': this.symbol + ':BRL',
                    'Order': {
                        'OrderId': orderId,
                        'Symbol': this.symbol,
                        'Side': this.orders[orderId].Side,
                        'Amount': this.orders[orderId].Amount,
                        'Price': this.orders[orderId].Price
                    }
                };

                delete this.orders[orderId];
            } else {
                let side;
                let price;
                if (Math.random() > 0.5) {
                    side = 'B';
                    price = randValue(31000.0, 31000.1);
                } else {
                    side = 'S';
                    price = randValue(32000.0, 32000.1);
                }

                message = {
                    'MessageId': parseFloat(randValue(1.0, 100.0)),
                    'MessageType': 'NewOrder',
                    'Symbol': this.symbol + ':BRL',
                    'Order': {
                        'OrderId': orderId,
                        'Symbol': this.symbol,
                        'Side': side,
                        'Amount': (side === 'B')?randValue(.1, 1) : randValue(100, 1000),
                        'Price': price
                    }
                };

                this.orders[orderId] = message;
            }
        }

        this.websocket.broadcast(JSON.stringify(message));
    }
}

let frequency = 60;
let symbol = 'ETH';

if (process.argv.length > 2) {
    frequency = parseInt(process.argv[2]);
}
if (process.argv.length > 3) {
    symbol = process.argv[3];
}

console.log('Starting server with ' + symbol + ' at ' + frequency + ' messages per minute...');

const dummyServer = new DummyServer(8005);
dummyServer.start(frequency, symbol);
