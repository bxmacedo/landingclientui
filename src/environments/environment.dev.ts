export const environment = {
  production: true,
  broker: {
    name: 'ABAKATE'
  },
  ws_mktdata: 'wss://mktdata.dev.h3.exchange',
  ws_trade:   'wss://trading.dev.h3.exchange',
  Auth0: {
    allowSignUp: false,
    clientId: 'wrxmtLR4ADihQvJfH5Hk9CP8cTLVVdol',
    domain: 'h3exchange.auth0.com',
    responseType: 'token id_token',
    audience: 'https://h3exchange.auth0.com/userinfo',
    redirectUri: 'https://app.dev.abakate.com.br/',
    scope: 'openid profile '// Learn about scopes: https://auth0.com/docs/scopes
  },
};
