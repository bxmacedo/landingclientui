export const environment = {
    production: false,
    broker: {
        name: 'ABAKATE'
      },
    ws_mktdata: 'ws://localhost:8005',
    ws_trade:   'ws://localhost:9005',
    Auth0: {
        allowSignUp: false,
        clientId: 'wrxmtLR4ADihQvJfH5Hk9CP8cTLVVdol',
        domain: 'h3exchange.auth0.com',
        responseType: 'token id_token',
        audience: 'https://h3exchange.auth0.com/userinfo',
        redirectUri: 'http://localhost:4200/home/trade',
        scope: 'openid profile '// Learn about scopes: https://auth0.com/docs/scopes
      },

};
