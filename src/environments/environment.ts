// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  broker: {
    name: 'ABAKATE'
  },
  ws_mktdata: 'ws://localhost:8005',
  ws_trade:   'ws://localhost:9005',
  Auth0: {
    allowSignUp: false,
    clientId: 'wrxmtLR4ADihQvJfH5Hk9CP8cTLVVdol',
    domain: 'h3exchange.auth0.com',
    responseType: 'token id_token',
    audience: 'https://h3exchange.auth0.com/userinfo',
    redirectUri: 'http://localhost:4200/home/trade',
    scope: 'openid profile '// Learn about scopes: https://auth0.com/docs/scopes
  },
};
