export const environment = {
  production: true,
  broker: {
    name: 'ABAKATE'
  },
  ws_mktdata: 'wss://mktdata.beta.spo01.h3.exchange',
  ws_trade:   'wss://trading.beta.spo01.h3.exchange',
  Auth0: {
    allowSignUp: false,
    clientId: 'iuMPYENV2zZ5DnIhgLEu4w56m068GCRr',
    domain: 'abakatebeta.auth0.com',
    responseType: 'token id_token',
    audience: 'https://abakatebeta.auth0.com/userinfo',
    redirectUri: 'https://beta.abakate.com.br/',
    scope: 'openid profile '// Learn about scopes: https://auth0.com/docs/scopes
  },
};
