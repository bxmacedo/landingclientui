import { TestBed, inject } from '@angular/core/testing';

import { MktDataService } from './mktdata.service';

describe('MktDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MktDataService]
    });
  });

  it('should be created', inject([MktDataService], (service: MktDataService) => {
    expect(service).toBeTruthy();
  }));
});
