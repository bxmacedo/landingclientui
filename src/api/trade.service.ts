import { Injectable, Inject, TestabilityRegistry } from '@angular/core';
import * as Redux from 'redux';
import { AppStore } from '../app/app.store';
import { BasicWebSocket } from '../api/basic-web-socket';
import {
    OrderActions,
    TransferActions,
    WalletActions,
    HistoryActions,
    AccountActions,
    UserActions,
    TickerActions
} from '../app/app.actions';
import { v4 as uuid } from 'uuid';
import { Auth0Service } from '../app/auth/auth0.service';
import { environment } from '../environments/environment';
import { OrderStatus } from '../app/shared/models/order/order.status';
import { MessageType } from '../app/shared/models/message/message.type';
import { TransferType } from '../app/shared/models/transfer/transfer.type';
import { TransferStatus } from '../app/shared/models/transfer/transfer.status';
import { setWsHeartbeat } from 'ws-heartbeat/client';
import { SnackBarMessage } from '../app/shared/message/snackbar-message.component';

import { MatSnackBar } from '@angular/material';
import { OrderSide } from '../app/shared/models/order/order.side';

@Injectable()
export class TradeService {
    private idToken: string;
    private sentOrderTime: number;

    public ws: WebSocket;
    public validToken = false;

    constructor(
        @Inject(AppStore) private store,
        @Inject(Auth0Service) public auth: Auth0Service,
        public snackBar: MatSnackBar,
    ) {
        this.start(environment.ws_trade);

        store.subscribe(() => this.updateState());
    }

    start(url) {
        this.ws = new WebSocket(url);

        setWsHeartbeat(this.ws, 'h3ping', {
            pingTimeout: 500000, // in 50 seconds, if no message accepted from server, close the connection.
            pingInterval: 30000, // every 30 seconds, send a ping message to the server.
        });

        this.ws.onopen = this.authenticate.bind(this);
        this.ws.onclose = () => {
            if (environment.production) {
                this.store.dispatch(UserActions.logOut());
            } else {
                this.store.dispatch(UserActions.logIn());
            }
            // reconnect now
            this.validToken = false;
            this.check(url);
        };
        this.ws.onerror = () => {
            if (environment.production) {
                this.store.dispatch(UserActions.logOut());
            } else {
                this.store.dispatch(UserActions.logIn());
            }
            this.validToken = false;
        };
        this.ws.onmessage = this.messageReceived.bind(this);
    }

    check(url) {
        if (!this.ws || this.ws.readyState === 3) {
            this.start(url);
        }
    }

    public sendMessage(message: string): boolean {
        if (this.ws.readyState === WebSocket.OPEN) {
            this.ws.send(message);
            return true;
        } else {
            console.warn('WS is not open to send message', message);
            return false;
        }
    }

    private updateState() {
        const state = this.store.getState();
        this.idToken = state.getIn(['auth', 'idToken']);
        if (!this.validToken) {
            this.authenticate();
        }
    }

    private authenticate() {
        if (
            this.idToken === '' || this.idToken === undefined || this.idToken === null
            || this.idToken === 'undefined'
        ) {
            this.validToken = false;
        } else {
            this.validToken = true;

            const authMessage = {
                MessageType: MessageType.Logon,
                Token: this.idToken,
                Broker: environment.broker.name,
                CreationTime: Date.now(),
                MessageId: uuid()
            };

            this.sendMessage(JSON.stringify(authMessage));
        }
    }

    private messageReceived(event: any) {
        if (event.data !== 'h3pong') {
            console.log(event);
            console.log('Sent -> Receive Message');
            console.log(Date.now() - this.sentOrderTime);
            const message = JSON.parse(event.data);
            JSON.parse(event.data);
            switch (message.MessageType) {
                case MessageType.OpenOrder:
                case MessageType.Trade:
                    if (message.Error) {
                        this.handleOpenOrderError(message);
                        break;
                    } else {
                        this.handleOpenOrder(message);
                        break;
                    }
                case MessageType.OrderError:
                    this.handleOpenOrderError(message);
                    break;
                case MessageType.Transfer:
                    this.store.dispatch(TransferActions.myTransfer(
                        message.MessageId,
                        message.Symbol,
                        message.Address,
                        message.Amount,
                        message.Status,
                        message.CreationTime
                    ));
                    break;
                case MessageType.BalanceChanged:
                    this.handleBalanceChanged(message);
                    break;
                case MessageType.DepositConfirmation:
                case MessageType.WithdrawalConfirmation:
                    this.store.dispatch(TransferActions.updateTransfer(
                        message.OperationId,
                        message.ApprovalTime,
                        TransferStatus.Approved,
                        ''
                    ));

                    this.snackBar.openFromComponent(SnackBarMessage, {
                        duration: 4500,
                        data:
                        (message.MessageType === MessageType.DepositConfirmation) ?
                        'Depósito confirmado' : 'Saque confirmado',
                        panelClass: ['snackbar-messsage']
                    });
                    break;
                case MessageType.OpenOrderConfirmation:
                    this.handleOpenOrderConfirmation(message);
                    break;
                case MessageType.ExecutionMaker:
                case MessageType.ExecutionTaker:
                    this.handleExecution(message);
                    break;
                case MessageType.CancelOrderConfirmation:
                    this.handleCancelOrderConfirmation(message);
                    break;
                case MessageType.UserLoggedIn:
                    if (!message.Error) {
                        this.store.dispatch(UserActions.logIn());
                    }
                    const allowedPairs = message.AllowedPairs;
                    if (allowedPairs !== undefined) {
                        allowedPairs.forEach(pair => {
                            console.log(pair);
                            this.store.dispatch(TickerActions.newPair(pair));
                        });
                    }
                    const history = message.Statements;
                    history.forEach(statement => {
                        if (statement.MessageType === MessageType.BalanceChanged) {
                            this.handleBalanceChanged(statement);
                        }

                        this.store.dispatch(HistoryActions.newHistory(
                            statement.OperationId,
                            statement.UserId,
                            statement.Symbol,
                            statement.Currency,
                            statement.Notional,
                            statement.Balance,
                            statement.SeqId,
                            statement.ParentOperationId,
                            statement.TimeStamp,
                            statement.OperationType
                        ));
                    });
                    message.Accounts.forEach(account => {
                        this.store.dispatch(AccountActions.newAccount(
                            account.AccId,
                            account.AccId,
                            account.Symbol,
                            true
                        ));
                    });
                    try {
                        const orders = message.Orders;
                        orders.forEach(orderMessage => {
                            switch (orderMessage.MessageType) {
                                case MessageType.OpenOrderConfirmation :
                                    this.handleOpenOrderConfirmation(orderMessage);
                                    break;
                                case MessageType.OpenOrder:
                                case MessageType.Trade:
                                    if (orderMessage.Error) {
                                        this.handleOpenOrderError(orderMessage);
                                        break;
                                    } else {
                                        this.handleOpenOrder(orderMessage);
                                        break;
                                    }
                                case MessageType.CancelOrderConfirmation:
                                    this.handleCancelOrderConfirmation(orderMessage);
                                    break;
                            }
                        });
                    } catch (e) {
                        console.warn('No Orders on UserLoggedIn Message', e);
                    }
                    try {
                        const executions = message.Executions;
                        executions.forEach(executionMessage => {
                            this.handleExecution(executionMessage);
                        });
                    } catch (e) {
                        console.warn('No Executions on UserLoggedIn Message', e);
                    }
                    break;
                case MessageType.NewAddress:
                    message.Accounts.forEach(account => {
                        this.store.dispatch(AccountActions.newAccount(
                            account.AccId,
                            account.AccId,
                            account.Symbol,
                            true
                        ));
                    });
                    break;
                case MessageType.PendingOperation:
                    switch (message.Operation.MessageType) {
                        case TransferType.ClientDeposit:
                        case TransferType.ClientWithdrawal:
                            const operation = message.Operation;
                            const transfer = {
                                transferId: operation.OperationId,
                                symbol: operation.Symbol,
                                amount: operation.Notional,
                                address: '',
                                status: TransferStatus.New,
                                time: Date.now(),
                                type: operation.MessageType
                            };
                            if (message.Operation.CurrencyType === 'Fiat') {
                                this.store.dispatch(TransferActions.newFiatTransfer(transfer));
                            }
                            if (message.Operation.CurrencyType === 'Crypto') {
                                transfer.address = operation.Address;
                                this.store.dispatch(TransferActions.newCryptoTransfer(transfer));
                            }
                            break;
                        default:
                            break;

                    }
                    break;
                default:
                    break;
            }
        }
    }

    public send(order, pair = 'ETH:BRL', orderType: MessageType) {
        this.sentOrderTime = Date.now();
        const orderMessage = {
            MessageType: orderType,
            Broker: environment.broker.name,
            CreationTime: Date.now(),
            MessageId: (orderType === MessageType.CancelOrder) ? uuid() : order.orderId,
            OperationId: order.orderId,
            Symbol: pair,
            Subtype: order.subtype,
            Side: (order.side === OrderSide.Buy) ? 'B' : 'S',
            Size: parseFloat(order.size),
            Price: parseFloat(order.price)
        };

        if (this.sendMessage(JSON.stringify(orderMessage))) {
            this.store.dispatch(OrderActions.updateOrderStatus(
                orderMessage.OperationId,
                OrderStatus.Sent,
                orderMessage.CreationTime
            ));
        }

    }

    public sendTransferNotification(transfer) {
        const transferMessage = {
            MessageType: transfer.type,
            Broker: environment.broker.name,
            CreationTime: transfer.time,
            MessageId: transfer.transferId,
            OperationId: transfer.transferId,
            Symbol: transfer.symbol,
            Address: transfer.address,
            Amount: transfer.amount
        };

        this.sendMessage(JSON.stringify(transferMessage));
    }

    public sendTransferAccounts(transferMessage) {
        this.sendMessage(JSON.stringify(transferMessage));
    }

    public handleOpenOrderConfirmation(message) {
        if (this.orderStatusIsOlder(message)) {
            this.store.dispatch(OrderActions.myOrder(
                message.OperationId,
                message.Symbol,
                (message.Side === 'B') ? OrderSide.Buy : OrderSide.Sell,
                OrderStatus.Confirmed,
                message.Size,
                message.Price,
                0, // message.Filled
                message.CreationTime,
                message.OrderId
            ));

            this.snackBar.openFromComponent(SnackBarMessage, {
                duration: 4500,
                data: 'Ordem de ' + message.Size + ' aberta!',
                panelClass: ['snackbar-messsage']
            });
        }
    }

    public handleCancelOrderConfirmation(message) {
        if (this.orderStatusIsOlder(message)) {
            this.store.dispatch(OrderActions.myOrder(
                message.OperationId,
                message.Symbol,
                (message.Side === 'B') ? OrderSide.Buy : OrderSide.Sell,
                OrderStatus.Cancelled,
                message.Size,
                message.Price,
                0, // message.Filled,
                message.CreationTime,
                message.OrderId
            ));
        }
    }

    public handleOpenOrderError(message) {
        if (this.orderStatusIsOlder(message)) {
            this.store.dispatch(OrderActions.myOrder(
                message.OperationId,
                message.Symbol,
                (message.Side === 'B') ? OrderSide.Buy : OrderSide.Sell,
                message.ErrorMessage,
                message.Size,
                message.Price,
                0, // message.Filled,
                message.CreationTime,
                message.OrderId
            ));
        }
    }

    public handleOpenOrder(message) {
        if (this.orderStatusIsOlder(message)) {
            this.store.dispatch(OrderActions.myOrder(
                message.OperationId,
                message.Symbol,
                (message.Side === 'B') ? OrderSide.Buy : OrderSide.Sell,
                OrderStatus.New,
                message.Size,
                message.Price,
                0, // message.Filled,
                message.CreationTime,
                message.OrderId
            ));
        }
    }

    public handleBalanceChanged(message) {
        if (message.SeqId > this.store.getState().get('balances').toJS()[message.Currency].sequence) {
            this.store.dispatch(WalletActions.updateBalance(
                message.SeqId,
                message.Currency,
                message.Balance
            ));
        }
    }

    public handleExecution(message) {
        try {
            if (this.orderStatusIsOlder(message)) {
                this.store.dispatch(OrderActions.updateOrderExecution(
                    message.OperationId,
                    message.Symbol,
                    (message.Side === 'B') ? OrderSide.Buy : OrderSide.Sell,
                    (message.OrderStatus === 'Fill' || message.OrderStatus === OrderStatus.Filled) ?
                        OrderStatus.Filled :
                        (
                            (message.OrderStatus === 'Ptl' || message.OrderStatus === OrderStatus.PartiallyFilled) ?
                            OrderStatus.PartiallyFilled :
                            (
                                (message.OrderStatus === OrderStatus.InsufficientBrokerFunds
                                || message.OrderStatus === OrderStatus.InsufficientClientFunds) ? OrderStatus.Rejected : message.OrderStatus
                            )
                        ),
                    message.Size,
                    message.Price,
                    message.Size, // message.Filled
                    message.CreationTime,
                    message.OrderId
                ));
            }
        } catch (e) {
            console.warn(e);
        }
    }

    public orderStatusIsOlder(message) {
        return (this.store.getState().get('myorders').filter(
            (order) => {
                return message.OrderId === order.externalOrderId
                &&
                message.CreationTime < order.time;
            }
        ).toList().toArray().length === 0);
    }

}
