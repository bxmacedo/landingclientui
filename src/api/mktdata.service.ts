import { Injectable, Inject } from '@angular/core';
import * as Redux from 'redux';
import { AppStore } from '../app/app.store';
import { BasicWebSocket } from './basic-web-socket';
import { MktDataActions, CandleChartActions, TickerActions, MessageActions } from '../app/app.actions';
import { v4 as uuid } from 'uuid';
import { environment } from '../environments/environment';
import { MessageType } from '../app/shared/models/message/message.type';
import { OrderSide } from '../app/shared/models/order/order.side';
import { setWsHeartbeat } from 'ws-heartbeat/client';

@Injectable()
export class MktDataService {

    public availablePairs;
    public ws: WebSocket;

    constructor(
        @Inject(AppStore) private store,
    ) {

        this.start(environment.ws_mktdata);

        const state = store.getState().toJS();
        this.availablePairs = Object.keys(state.tickers);

        if (this.availablePairs.length > 0) {
            this.store.dispatch(TickerActions.currentTicker(this.availablePairs[0]));
        }

        this.ws.onopen = () => (this.subscribe(this.store.getState().get('currentTicker').toJS().pair, true));
    }

    start(url) {
        this.ws = new WebSocket(url);

        setWsHeartbeat(this.ws, 'h3ping', {
            pingTimeout: 500000, // in 50 seconds, if no message accepted from server, close the connection.
            pingInterval: 30000, // every 30 seconds, send a ping message to the server.
        });

        this.ws.onclose = () => {
            console.log('closed!');
            // reconnect now
            this.check(url);
        };
        this.ws.onmessage = this.messageReceived.bind(this);
    }

    check(url) {
        if (!this.ws || this.ws.readyState === 3) {
            this.start(url);
        }
    }

    public sendMessage(message: string): boolean {
        if (this.ws.readyState === WebSocket.OPEN) {
            this.ws.send(message);
            return true;
        } else {
            console.warn('WS is not open to send message', message);
            return false;
        }
    }

    private dateTimeReviver(key, value) {
        if (typeof value === 'string') {
            const a = /\/Date\((\d*)[-+](\d*)\)\//.exec(value);
            if (a) {
                return (+a[1]);
            }
        }
        return value;
    }

    private messageReceived(event: any) {
        if (event.data !== 'h3pong') {
            const message = JSON.parse(event.data, this.dateTimeReviver);
            const storedMessageId = this.store.getState().get('processedMessageId')
                .toJS()
                .filter(
                    (value) => value === message.MessageId
                )[0];
            if (storedMessageId !== message.MessageId) {
                this.store.dispatch(MessageActions.newMessage(message.MessageId));
                switch (message.MessageType) {
                    case MessageType.Ticker:
                        this.store.dispatch(MktDataActions.updatePairInfo(
                            message.Symbol,
                            message.Ticker.LastPrice,
                            message.Ticker.TradeVolume, // message.Volume
                            message.Ticker.NetChange
                        ));
                        break;
                    case MessageType.NewOrder:
                        if (Number(message.Order.Amount) === 0) {
                            this.store.dispatch(MktDataActions.rcvdCancelOrder(
                                message.Order.OrderId,
                                message.Symbol,
                                message.Order.Side,
                                message.Order.Amount,
                                parseFloat(message.Order.Price)
                            ));
                        } else {
                            this.store.dispatch(MktDataActions.rcvdNewOrder(
                                message.Order.OrderId,
                                message.Symbol,
                                message.Order.Side,
                                message.Order.Amount,
                                parseFloat(message.Order.Price)
                            ));
                        }
                        break;
                    case MessageType.CancelOrder:
                        this.store.dispatch(MktDataActions.rcvdCancelOrder(
                            message.Order.OrderId,
                            message.Symbol,
                            message.Order.Side,
                            message.Order.Amount,
                            message.Order.Price
                        ));
                        break;
                    case MessageType.ExecutionReport:
                        this.store.dispatch(MktDataActions.executionReport(
                            message.Size,
                            message.Symbol,
                            message.Price,
                            message.TimeStamp
                        ));
                        break;
                    case MessageType.Trade:
                        this.store.dispatch(MktDataActions.executionReport(
                            message.Trade.Size,
                            message.Symbol,
                            message.Trade.Price,
                            message.Trade.TimeStamp
                        ));
                        this.createInitialCandle(message.Trade);
                        break;
                    case MessageType.Subscribed:
                        this.showTickers(message);
                        this.dispatchAllBids(message);
                        this.dispatchAllAsks(message);
                        this.dispatchAllTrades(message);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public dispatchAllBids(subscribedMessage) {
        const bids = subscribedMessage.Book.Bids;
        const pair = subscribedMessage.Symbol;
        for (const key in bids) {
            if (bids.hasOwnProperty(key)) {
                this.store.dispatch(MktDataActions.rcvdNewOrder(
                    bids[key].Id,
                    pair,
                    'B',
                    bids[key].Size,
                    parseFloat(bids[key].Price)
                ));
            }
        }
    }

    public dispatchAllAsks(subscribedMessage) {
        const asks = subscribedMessage.Book.Asks;
        const pair = subscribedMessage.Symbol;
        for (const key in asks) {
            if (asks.hasOwnProperty(key)) {
                this.store.dispatch(MktDataActions.rcvdNewOrder(
                    asks[key].Id,
                    pair,
                    'S',
                    asks[key].Size,
                    parseFloat(asks[key].Price)
                ));
            }
        }
    }

    public dispatchAllTrades(subscribedMessage) {
        const trades = subscribedMessage.Trades;
        const pair = subscribedMessage.Symbol;
        for (const key in trades) {
            if (trades.hasOwnProperty(key)) {
                this.store.dispatch(MktDataActions.executionReport(
                    trades[key].Size,
                    pair,
                    trades[key].Price,
                    trades[key].TimeStamp
                ));
                this.createInitialCandle(trades[key]);
            }
        }
    }

    public createInitialCandle(trade: any): void {
        if (this.store.getState().toJS().candlechart.length === 0) {
            this.store.dispatch(CandleChartActions.updateCandlechart(
                trade.TimeStamp,
                trade.Price,
                trade.Price,
                trade.Price,
                trade.Price,
                trade.Symbol
            ));
        } else {
            const candleChartData = this.store.getState().toJS().candlechart;
            const lastChartData = candleChartData[0];
            if (trade.TimeStamp <= lastChartData.date + 60000) {
                if (lastChartData.l > trade.Price) {
                    lastChartData.l = trade.Price;
                }

                if (lastChartData.h < trade.Price) {
                    lastChartData.h = trade.Price;
                }

                lastChartData.c = trade.Price;

                this.store.dispatch(CandleChartActions.updateFirstItemCandlechart(
                    lastChartData.date,
                    lastChartData.l,
                    lastChartData.h,
                    lastChartData.o,
                    lastChartData.c,
                    lastChartData.pair
                ));
            } else {
                this.store.dispatch(CandleChartActions.updateCandlechart(
                    trade.TimeStamp,
                    trade.Price,
                    trade.Price,
                    trade.Price,
                    trade.Price,
                    trade.Symbol
                ));
            }
        }
    }

    public showTickers(subscribedMessage) {
        this.store.dispatch(TickerActions.currentTicker(subscribedMessage.Symbol));
    }

    public subscribe(pair: string, subscribeFlag: boolean) {
        const uid = uuid();
        const subscribeMessage = {
            MessageId: uid,
            OperationId: uid,
            MessageType: subscribeFlag ? (MessageType.Subscribe) : MessageType.Unsubscribe,
            Symbol: pair,
            CreationTime: Date.now()
        };
        console.log(subscribeMessage);
        this.sendMessage(JSON.stringify(subscribeMessage));
    }
}
