export const BROKER_NAME = 'Abakate';

// BOOK
export const RENDER_INTERVAL = 1000; // ms
export const BOOK_BAR_MIN_WIDTH = 4;
export const BOOK_BAR_MAX_WIDTH = 40.0;
export const BOOK_PRECISION = 2;

// TRADES
export const TRADES_RENDER_INTERVAL = 2000; // ms

// TICKER
export const TICKER_RENDER_INTERVAL = 2000; // ms

// CANDLE CHART
export const CANDLECHART_RENDER_INTERVAL = 15000; // ms

// DEPTH CHART
export const DEPTHCHART_RENDER_INTERVAL = 10000; // ms

// MYORDERS
export const MYORDERS_RENDER_INTERVAL = 1000; // ms

// TRANSFER
export const TRANSFER_RENDER_INTERVAL = 1000; // ms
