import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'input-currency',
    templateUrl: './input-currency.component.html',
    styleUrls: ['./input-currency.component.scss']
})
export class InputCurrencyComponent implements OnInit {

    public inputBaseControl = new FormControl('', [Validators.required, Validators.min(0)]);

    @Input() strPrefix = '';
    @Input() placeholder;
    @Input() value = 0;
    @Output() selectChangePrice = new EventEmitter<number>();

    constructor() {

    }

    ngOnInit() {
    }

    changeValue(newValue) {
        this.selectChangePrice.emit(newValue);
    }

}
