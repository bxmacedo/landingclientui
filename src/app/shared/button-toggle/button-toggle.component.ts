import {
    EventEmitter,
    Component,
    OnInit,
    Input,
    Output
} from '@angular/core';

@Component({
    selector: 'buttontoggle',
    templateUrl: './button-toggle.component.html',
    styleUrls: ['./button-toggle.component.scss']
  })
  export class ButtonToggleComponent implements OnInit {
  
    @Input() options = [];
    @Input() value;
    @Output() selectChanges = new EventEmitter < number > ();


    constructor() {}

    ngOnInit() {

    }

    public select(change) {
        this.selectChanges.emit(change.value);
    }

}
