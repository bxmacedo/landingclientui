
import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'range-calendar',
    templateUrl: './range-calendar.component.html',
    styles: ['./range-calendar.component.scss'],
})
export class RangeCalendarComponent implements OnInit {

    public bsRangeValue: Date[];
    public now = new Date();
    public maxDate = new Date();
    public minDate = new Date();
    public startDate = new Date();
    @Output() rangeDates: EventEmitter<object> = new EventEmitter<object>();
    bsConfig: Partial<BsDatepickerConfig>;
    colorTheme = 'theme-default';


    constructor() {
      this.minDate.setDate(this.minDate.getDate() - 30);
      this.maxDate.setDate(this.maxDate.getDate());
      this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
      this.bsConfig.rangeInputFormat = 'DD/MM/YYYY';
      this.bsRangeValue = [this.minDate, this.maxDate];
    }

    ngOnInit() {
      this.rangeDates.emit({ fromDate: this.minDate, toDate: this.maxDate });

    }

    onChangeDates(event: any) {
      this.rangeDates.emit({ fromDate: event[0], toDate: event[1] });

    }
}
