import {
    Component,
    OnInit,
    EventEmitter,
    Input,
    Output
} from '@angular/core';
import {
    FormControl,
    Validators
} from '@angular/forms';

@Component({
  selector: 'inputbase',
  templateUrl: './input-base.component.html',
  styleUrls: ['./input-base.component.scss']
})
export class InputBaseComponent implements OnInit {
    public crypto: boolean = true;

    public inputBaseControl = new FormControl('', [

        Validators.required,
        Validators.min(0),

    ]);

    constructor() {

    }

    @Input() placeholder;
    @Input() cryptoType;
    @Input() value = 0;
    @Output() selectChangePrice = new EventEmitter < number > ();


    ngOnInit() {}

    changeValue(newValue) {
        this.selectChangePrice.emit(newValue);
    }

    onKey(event: any) {
        if (this.value == null || isNaN(this.value)) {
            this.crypto = true;
        } else {
            this.crypto = false;
        }
    }

}
