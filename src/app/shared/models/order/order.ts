import { OrderStatus } from './order.status';
import { OrderSide } from './order.side';
import { OrderType } from './order.type';

export class Order {
    orderId?: string;
    externalOrderId?: string;
    subtype: OrderType;
    side: OrderSide;
    size: number;
    price: number;
    pair: string;
    filled?: number;
    status?: OrderStatus;
    time?: number;
}
