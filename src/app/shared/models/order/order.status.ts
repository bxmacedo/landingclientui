export enum OrderStatus {
    Created = 'Created', // Created locally
    Sent = 'Sent',     // Sent to Web Socket
    Confirmed = 'Open' , // Received by Web Scoket
    New = 'New_0' , // Received by Web Scoket
    Filled = 'Filled_2',
    Rejected = 'Rejected_8',
    Cancelled = 'Canceled_4',
    PartiallyFilled = 'PartiallyFilled_1',
    Canceling = 'Canceling',
    AllStatus = 'AllStatus',
    // ERROR STATUS
    InsufficientClientFunds = 'Insufficient Client Funds',
    InsufficientBrokerFunds = 'Insufficient Broker Funds',
}
