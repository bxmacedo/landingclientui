export enum OrderType {
    Market = 'market',
    Limit = 'limit'
}
