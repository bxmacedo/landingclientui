import { MessageType } from './message.type';

export class Message {
    MessageId: string;
    OperationId: string;
    CreationTime: number;
    Broker: string;
    UserId: string;
    MessageType: MessageType;
    Symbol: string;
}
