import { Component, ViewChild, OnInit } from '@angular/core';
import { UploadFileService } from '../../shared/registeraccount-modal/upload-file-service';

import {DropzoneComponent, DropzoneDirective, DropzoneConfigInterface} from 'ngx-dropzone-wrapper';

@Component({
    selector: 'upload-file',
    templateUrl: './upload-file.component.html',
    styles: ['./upload-file.component.scss'],
    providers: [UploadFileService]
})
export class UploadFileComponent implements OnInit {
    public type =  'component';
    public disabled = false;
    public config: DropzoneConfigInterface = {
      clickable: true,
      maxFiles: 1,
      autoReset: null,
      errorReset: null,
      cancelReset: null
    };
    @ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;
    @ViewChild(DropzoneDirective) directiveRef?: DropzoneDirective;
    
    constructor() { }

    ngOnInit() {
        UploadFileService.clearSelfie.subscribe( inf =>  this.resetDropzoneUploads());
    }


    public toggleType(): void {
      this.type = (this.type === 'component') ? 'directive' : 'component';
    }
    
    public toggleDisabled(): void {
      this.disabled = !this.disabled;
    }
    
    public toggleAutoReset(): void {
      this.config.autoReset = this.config.autoReset ? null : 5000;
      this.config.errorReset = this.config.errorReset ? null : 5000;
      this.config.cancelReset = this.config.cancelReset ? null : 5000;
    }
    
    public toggleMultiUpload(): void {
      this.config.maxFiles = this.config.maxFiles ? 0 : 1;
    }
    
    public toggleClickAction(): void {
      this.config.clickable = !this.config.clickable;
    }
    
    public resetDropzoneUploads(): void {
      if (this.type === 'directive' && this.directiveRef) {
        this.directiveRef.reset();
      } else if (this.type === 'component' && this.componentRef && this.componentRef.directiveRef) {
        this.componentRef.directiveRef.reset();
      }
    }
    
    public onUploadError(args: any): void {
      //console.log('onUploadError:', args);
    }
    
    public onUploadSuccess(args: any): void {
      //console.log('onUploadSuccess:', args);
    }
  }

  