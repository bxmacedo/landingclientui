import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-privacy-modal',
    templateUrl: './privacy-modal.component.html',
    styleUrls: ['./privacy-modal.component.scss']
})
export class PrivacyModalComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<PrivacyModalComponent>,
    ) { }

    ngOnInit() {
    }

}
