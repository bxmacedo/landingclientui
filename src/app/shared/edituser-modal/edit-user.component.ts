import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { TradeService } from '../../../api/trade.service';
import { AppStore } from '../../app.store';
@Component({
    templateUrl: './edit-user.component.html',
    styleUrls: ['./edit-user.component.scss']
})

export class EditUserModalComponent implements OnInit {

    public selectedState: String = 'UF';
    public name: String;
    public email: String;
    public cpf: String;
    public birthdate: String;
    public cellPhone: String;
    public mothersName: String;
    public street: String;
    public number: Number;
    public complement: String;
    public city: String;
    public neigh: String;
    public zipCode: String;
    public profile;
    public profileForm: FormGroup;
    public cpfPattern = '[0-9]{11}';
    public validCPFFlag = false;
    public phonePattern = '^([1-9]{2})?\-?[1-9]{4,5}\-?[0-9]{4}$';
    public zipCodePattern = '[0-9]{5}\-?[0-9]{3}$';

    constructor(
        public dialogRef: MatDialogRef<EditUserModalComponent>,
        @Inject(AppStore) private store,
        @Inject(TradeService) private tradeService: TradeService,
    ) {
    }

    ngOnInit() {
        this.profile = this.store.getState().toJS().profile;
        // this.userTotalXP = this.profile.experience;
        // Profile Form
        this.profileForm = new FormGroup({
            'name': new FormControl(this.name, Validators.required),
            'email': new FormControl(this.email, [Validators.required, Validators.email]),
            'cpf': new FormControl(this.cpf, Validators.required),
            'birthdate': new FormControl(this.birthdate, Validators.required),
            'cellPhone': new FormControl(this.cellPhone, Validators.required),
            'mothersName': new FormControl(this.mothersName, Validators.required),
            'street': new FormControl(this.street, Validators.required),
            'number': new FormControl(this.number, Validators.required),
            'complement': new FormControl(this.complement, Validators.required),
            'neigh': new FormControl(this.neigh, Validators.required),
            'city': new FormControl(this.city, Validators.required),
            'zipCode': new FormControl(this.zipCode, Validators.required)
        });
    }

    public onNoClick(): void {
        this.dialogRef.close();
    }

    public onSubmit(): void {
        /*
        this.store.dispatch(EditProfileActions.updateFullProfile(
            this.selectedState,
            this.name,
            this.email,
            this.cpf,
            this.birthdate,
            this.cellPhone,
            this.mothersName,
            this.street,
            this.number,
            this.complement,
            this.city,
            this.neigh,
            this.zipCode
        )); */

        this.dialogRef.close();
    }

    public formatCPF(): void {
        if (!this.cpf) {
            return;
        }
        let formattedCPF = this.cpf;
        formattedCPF = formattedCPF.replace(/\D/g, '');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        this.cpf = formattedCPF;
    }

    public validCPF(): void {
        let Soma;
        let Resto;
        Soma = 0;

        if (!this.cpf) {
            return;
        }
        const strCPF = this.cpf.replace(/\./g, '').replace(/\-/g, '');

        if (strCPF === '00000000000') {
            this.validCPFFlag = false;
            return;
        }

        for (let i = 1; i <= 9; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (11 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(9, 10), 10)) {
            this.validCPFFlag = false;
            return;
        }

        Soma = 0;
        for (let i = 1; i <= 10; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (12 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(10, 11), 10)) {
            this.validCPFFlag = false;
            return;
        }

        this.validCPFFlag = true;
    }

    public formatCellPhone(): void {
        if (!this.cellPhone) {
            return;
        }
        let formattedCellPhone = this.cellPhone;
        formattedCellPhone = formattedCellPhone.replace(/\D/g, '');
        formattedCellPhone = formattedCellPhone.replace(/(\d+)(\d{4})/, '$1-$2');
        this.cellPhone = formattedCellPhone;

    }

    public formatZipCode(): void {
        if (!this.zipCode) {
            return;
        }
        let formattedZipCode = this.zipCode;
        formattedZipCode = formattedZipCode.replace(/\D/g, '');
        formattedZipCode = formattedZipCode.replace(/(\d+)(\d{3})/, '$1-$2');
        this.zipCode = formattedZipCode;
    }

    public formatBirthDate(): void {
        if (!this.birthdate) {
            return;
        }
        let formattedBirthDate = this.birthdate;
        formattedBirthDate = formattedBirthDate.replace(/\D/g, '');
        formattedBirthDate = formattedBirthDate.replace(/(\d{2})(\d)/, '$1/$2');
        formattedBirthDate = formattedBirthDate.replace(/(\d{2})(\d)/, '$1/$2');
        this.birthdate = formattedBirthDate;
    }
}