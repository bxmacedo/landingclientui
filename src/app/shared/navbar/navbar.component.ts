import { Component, OnInit } from '@angular/core';
import { Auth0Service } from '../../auth/auth0.service';
import { environment } from '../../../environments/environment';


@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    public production;

    constructor(
        public auth: Auth0Service,
    ) {
        this.production = environment.production;
    }

    ngOnInit() {
    }

    public logout(): void {
        this.auth.logout();
    }

}
