import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'input-quantity',
    templateUrl: './input-quantity.component.html',
    styleUrls: ['./input-quantity.component.scss']
})
export class InputQuantityComponent implements OnInit {

    public inputBaseControl = new FormControl('', [Validators.required, Validators.min(0)]);

    @Input() placeholder;
    @Input() cryptoType;
    @Input() value = 0;
    @Output() selectChangePrice = new EventEmitter<number>();

    constructor() {

    }
    ngOnInit() {
        
    }

    changeValue(newValue) {
        this.selectChangePrice.emit(newValue);
    }

}
