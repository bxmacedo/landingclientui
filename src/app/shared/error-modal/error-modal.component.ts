import {Component, Inject} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material';


@Component({
    selector: 'error-modal',
    templateUrl: './error-modal.component.html',
    styleUrls: ['./error-modal.component.scss']
  })

export class ErrorModalComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
