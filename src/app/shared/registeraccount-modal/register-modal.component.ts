import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { TradeService } from '../../../api/trade.service';
import { AppStore } from '../../app.store';
import { AccountActions } from '../../app.actions';
import { v4 as uuid } from 'uuid';
import { MessageType } from '../../shared/models/message/message.type';
import { UploadFileService } from './upload-file-service';

export interface State {
    name: string;
    code: string;
}

@Component({
    templateUrl: './register-modal.html',
    styleUrls: ['./register-modal.scss'],
    providers: [UploadFileService]
})

export class RegisterModalComponent implements OnInit {
    public bank: string;
    public agency: string;
    public account: string;
    public cpfOwnerName: string;
    public nameCoOwner: string;
    public accountForm: FormGroup;
    public bankPattern = '[0-9]{3}';
    public agencyPattern = '[a-zA-Z0-9]{4}';
    public isTypeAccount = false;
    public validCPFFlag = false;

    /**

        public mokListBanks = [{
                name: 'Arkansas',
                code: '111'
            },
            {
                name: 'California',
                code: '222'
            },
            {
                name: 'Florida',
                code: '333'
            },
            {
                name: 'Texas',
                code:  '444'
            }
        ];
        filteredStates: any;
        stateCtrl = new FormControl();
    **/

    constructor(
        public dialogRef: MatDialogRef<RegisterModalComponent>,
        private uploadFileService: UploadFileService,
        @Inject(AppStore) private store,
        @Inject(TradeService) private tradeService: TradeService
    ) {
        /** Future implementation
            this.filteredStates = this.stateCtrl.valueChanges
            .pipe(
                startWith(''),
                map(state => state ? this.filterName(state) : this.mokListBanks.slice())
            );
        **/
    }

    /** Future implementation
        private filterName(value: string) {
            const filterValue = value.toLowerCase();

            return this.mokListBanks.filter(state => state.name.toLowerCase().indexOf(filterValue) === 0);
        }
    **/
    ngOnInit() {
        this.accountForm = new FormGroup({
            'bank': new FormControl(this.bank, Validators.required),
            'agency': new FormControl(this.agency, [Validators.required]),
            'account': new FormControl(this.account, Validators.required),
        });
        /* Future implementation
            'cpfOwnerName': new FormControl(this.cpfOwnerName, [Validators.required, Validators.maxLength(14)]),
            'nameCoOwner': new FormControl(this.bank, Validators.required),
        **/
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        const accId = this.bank + this.agency + this.account;
        const address = this.bank + ' ' + this.agency + ' ' + this.account;
        const symbol = 'BRL';
        const visible = true;
        const transfer = {
            MessageType: MessageType.CreateNewAddress,
            CreationTime: Date.now(),
            MessageId: uuid(),
            Symbol: symbol,
            accId: accId,
            Address: address
        };

        this.store.dispatch(AccountActions.newAccount(accId, address, symbol, visible));

        /**
         * Function back-end*/
        this.tradeService.sendTransferAccounts(transfer);

        this.dialogRef.close();

    }

    public select() {
        this.isTypeAccount = !this.isTypeAccount;

    }

    public formatCPF(): void {
        if (!this.cpfOwnerName) {
            return;
        }
        let formattedCPF = this.cpfOwnerName;
        formattedCPF = formattedCPF.replace(/\D/g, '');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        this.cpfOwnerName = formattedCPF;
    }

    cleanSelfie() {
        this.uploadFileService.clear();
    }

    public validCPF(): void {
        let Soma;
        let Resto;
        Soma = 0;

        if (!this.cpfOwnerName) {
            return;
        }
        const strCPF = this.cpfOwnerName.replace(/\./g, '').replace(/\-/g, '');

        if (strCPF === '00000000000') {
            this.validCPFFlag = false;
            return;
        }

        for (let i = 1; i <= 9; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (11 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(9, 10), 10)) {
            this.validCPFFlag = false;
            return;
        }

        Soma = 0;
        for (let i = 1; i <= 10; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (12 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(10, 11), 10)) {
            this.validCPFFlag = false;
            return;
        }

        this.validCPFFlag = true;
    }
}
