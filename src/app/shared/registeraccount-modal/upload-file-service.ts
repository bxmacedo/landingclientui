import { Injectable, EventEmitter } from '@angular/core';



@Injectable()
export class UploadFileService {

   static clearSelfie: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }


    clear() {
        UploadFileService.clearSelfie.emit();
    }
}