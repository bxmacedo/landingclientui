import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Auth0Service } from '../../auth/auth0.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

    public forgot: FormGroup;
    public email: string;

    constructor(
        public auth: Auth0Service,
    ) {

    }

    ngOnInit() {
        this.forgot = new FormGroup({
            'email': new FormControl(this.email, Validators.required)
        });
    }
    public resetPassword(email): void {
        if (email === '') {
            alert('Favor preencher o campo de email.');
        } else {
            this.auth.resetPassword(email);
            alert('Um email foi enviado para ' + email + ' para resetar sua senha');
        }
    }

}
