import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Auth0Service } from '../../auth/auth0.service';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {


    public signin: FormGroup;
    public email: string;
    public password: string;

    constructor(
        public auth: Auth0Service,
    ) { }

    ngOnInit() {
        this.signin = new FormGroup({
            'email': new FormControl(this.email, Validators.required),
            'password': new FormControl(this.password, Validators.required),
        });
    }

    public createSignUp(): void {
        // this.store.dispatch(ProfileActions.createSignUp(
        //     this.userName,
        //     this.userLastName,
        //     this.store.getState().toJS().signUp.email,
        //     this.store.getState().toJS().signUp.cpf,
        //     this.store.getState().toJS().signUp.password,
        // ));
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'email') {
            document.getElementById('password').focus();
        }
        if (element === 'password') {
            this.auth.login(this.email, this.password);
        }
    }

}
