import { Component, OnInit, } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

declare let $: any;

@Component({
    selector: 'app-old-sign-up',
    templateUrl: './old-sign-up.component.html',
    styleUrls: ['./old-sign-up.component.scss']
})
export class OldSignUpComponent implements OnInit {

    animating: boolean;
    mayPassFirst: boolean;
    mayPassSecond: boolean;
    validCPFFlag = false;
    validPasswordsFlag = false;

    fullname = '';
    cpf = '';
    email = '';
    mothersname = '';
    addr_street = '';
    addr_number = '';
    addr_compl = '';
    addr_neigh = '';
    addr_city = '';
    addr_state = '';
    addr_zip = '';
    cell_areacode = '';
    cell_number = '';
    birthdate = '';
    password = '';
    password2 = '';

    passStrength = 0;

    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    ngOnInit() {

    }

    next(event): void {
        if (this.animating) {
            return;
        }
        this.animating = true;

        const current_fs = $(event.srcElement).parent();
        const next_fs = $(current_fs).next();

        $('#progressbar li').eq($('fieldset').index(next_fs)).addClass('active');

        next_fs.show();

        current_fs.animate({
            opacity: 0
        }, {
                step: function (now, mx) {
                    const scale = 1 - (1 - now) * 0.2;
                    const left = (now * 50) + '%';
                    const opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'position': 'absolute'
                    });
                    next_fs.css({
                        'left': left,
                        'opacity': opacity
                    });
                },
                duration: 400,
                complete: function () {
                    current_fs.hide();
                    this.animating = false;
                }.bind(this)
            });
    }

    previous(event): void {
        if (this.animating) {
            return;
        }
        this.animating = true;

        const current_fs = $(event.srcElement).parent();
        const previous_fs = $(current_fs).prev();

        $('#progressbar li').eq($('fieldset').index(current_fs)).removeClass('active');

        previous_fs.show();

        current_fs.animate({
            opacity: 0
        }, {
                step: function (now, mx) {
                    const scale = 0.8 + (1 - now) * 0.2;
                    const left = ((1 - now) * 50) + '%';
                    const opacity = 1 - now;
                    current_fs.css({
                        'left': left
                    });
                    previous_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'opacity': opacity
                    });
                },
                duration: 400,
                complete: function () {
                    current_fs.hide();
                    this.animating = false;
                }.bind(this)
            });
    }

    formatCPF() {
        if (!this.cpf) {
            return;
        }
        let formattedCPF = this.cpf;
        formattedCPF = formattedCPF.replace(/\D/g, '');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        this.cpf = formattedCPF;
    }

    formatBirthDate() {
        if (!this.birthdate) {
            return;
        }
        let formattedBirthDate = this.birthdate;
        formattedBirthDate = formattedBirthDate.replace(/\D/g, '');
        formattedBirthDate = formattedBirthDate.replace(/(\d{2})(\d)/, '$1/$2');
        formattedBirthDate = formattedBirthDate.replace(/(\d{2})(\d)/, '$1/$2');
        this.birthdate = formattedBirthDate;
    }

    formatCellPhone() {
        if (!this.cell_number) {
            return;
        }
        let formattedCellPhone = this.cell_number;
        formattedCellPhone = formattedCellPhone.replace(/\D/g, '');
        formattedCellPhone = formattedCellPhone.replace(/(\d+)(\d{4})/, '$1-$2');
        this.cell_number = formattedCellPhone;
    }

    formatZipCode() {
        if (!this.addr_zip) {
            return;
        }
        let formattedZipCode = this.addr_zip;
        formattedZipCode = formattedZipCode.replace(/\D/g, '');
        formattedZipCode = formattedZipCode.replace(/(\d+)(\d{3})/, '$1-$2');
        this.addr_zip = formattedZipCode;
    }

    checkStrength(strength: number): void {
        this.passStrength = strength;
        this.verifyPassFirst();
    }

    validCPF() {
        let Soma;
        let Resto;
        Soma = 0;

        if (!this.cpf) {
            return;
        }
        const strCPF = this.cpf.replace(/\./g, '').replace(/\-/g, '');

        if (strCPF === '00000000000') {
            this.validCPFFlag = false;
            return;
        }

        for (let i = 1; i <= 9; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (11 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(9, 10), 10)) {
            this.validCPFFlag = false;
            return;
        }

        Soma = 0;
        for (let i = 1; i <= 10; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (12 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(10, 11), 10)) {
            this.validCPFFlag = false;
            return;
        }

        this.validCPFFlag = true;
        this.verifyPassFirst();
    }

    verifyPassFirst() {
        this.mayPassFirst = this.password && (this.password.length > 0) && (this.password === this.password2) && this.passStrength >= 40 &&
            this.validCPFFlag &&
            (this.cpf && this.cpf.length > 0) &&
            (this.fullname && this.fullname.length > 0) &&
            (this.email && this.email.length > 3 && this.email.indexOf('@') > 0);
    }

    verifyPassSecond() {
        this.mayPassSecond = (this.mothersname && this.mothersname.length > 0) &&
            (this.addr_street && this.addr_street.length > 0) &&
            (this.addr_number && this.addr_number.length > 0) &&
            (this.addr_neigh && this.addr_neigh.length > 0) &&
            (this.addr_city && this.addr_city.length > 0) &&
            (this.addr_state && this.addr_state.length > 0) &&
            (this.addr_zip && this.addr_zip.length >= 9) &&
            (this.cell_areacode && this.cell_areacode.length > 0) &&
            (this.cell_number && this.cell_number.length >= 10) &&
            (this.birthdate && this.birthdate.length >= 10);
    }

    submitForm() {
        const postData = {
            client_id: environment.Auth0.clientId,
            email: this.email,
            password: this.password,
            connection: 'Username-Password-Authentication',
            user_metadata: {
                fullname: this.fullname,
                cpf: this.cpf,
                mothersname: this.mothersname,
                addr_street_number_compl: this.addr_street + '$' + this.addr_number + '$' + this.addr_compl,
                addr_neigh_city_state_zip: this.addr_neigh + '$' + this.addr_city + '$' + this.addr_state + '$' + this.addr_zip,
                cell_areacode: this.cell_areacode,
                cell_number: this.cell_number,
                birthdate: this.birthdate,
            }
        };

        const req = this.http.post('https://' + environment.Auth0.domain + '/dbconnections/signup', postData, {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        });
        req.subscribe(data => {
            this.router.navigate(['./poscadastro']);
        }, err => {
            this.router.navigate(['./poscadastro']);
        });
    }

}
