import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OldSignUpComponent } from './old-sign-up.component';

describe('OldSignUpComponent', () => {
    let component: OldSignUpComponent;
    let fixture: ComponentFixture<OldSignUpComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [OldSignUpComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(OldSignUpComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
