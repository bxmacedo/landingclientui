import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MktDataService } from '../../../api/mktdata.service';
import { AppStore } from '../../app.store';
import { TICKER_RENDER_INTERVAL, BOOK_BAR_MIN_WIDTH, BOOK_BAR_MAX_WIDTH, BOOK_PRECISION } from '../../app.constants';

@Component({
    selector: 'app-l-book',
    templateUrl: './l-book.component.html',
    styleUrls: ['./l-book.component.scss']
})
export class LBookComponent implements OnInit, OnDestroy {
    public lastPrice: number;
    public volume: number;
    public variation: number;
    public counter: any;

    public pair;
    public bids;
    public asks;

    constructor(
        @Inject(MktDataService) private mktDataService: MktDataService,
        @Inject(AppStore) private store,
    ) {
        this.startTicker();
    }

    ngOnInit() {
        this.mktDataService.subscribe('ETH:BRL', false);
    }

    ngOnDestroy() {
        clearTimeout(this.counter);
    }

    public startTicker(): void {
        this.timer(this.updateSelectCrypto.bind(this), TICKER_RENDER_INTERVAL);
    }

    public timer(callback, milliseconds): void {
        this.counter = setTimeout(() => {
            callback();
            this.timer(callback, milliseconds);
        }, milliseconds);
    }

    public updateSelectCrypto(): void {
        const state = this.store.getState().get('tickers').toJS();
        const data = state['ETH:BRL'];

        if (data !== undefined) {
            // TICKER
            this.lastPrice = data.last;
            this.variation = data.variation;
            this.volume = data.volume;

            // BOOK
            const bids = this.store.getState()
                .getIn(['orderbook', 'bid'])
                .sortBy((order) => order.orderId)
                .sortBy((order) => -order.price)
                .toList()
                .toArray()
                .filter((order) => order.symbol === 'ETH:BRL')
                .slice(0, 30);

            this.bids = [];
            let lastVolume = 0.0;
            let lastPrice = '#';

            const bidMinMaxAmount = bids.reduce((acc, val) => {
                acc[0] = (acc[0] === undefined || val.amount < acc[0]) ? val.amount : acc[0]; // MIN
                acc[1] = (acc[1] === undefined || val.amount > acc[1]) ? val.amount : acc[1]; // MAX
                return acc;
            }, []);

            this.mountBookSide(bids, this.bids, lastVolume, lastPrice, bidMinMaxAmount);

            const asks = this.store.getState()
                .getIn(['orderbook', 'ask'])
                .toList()
                .sortBy((order) => order.orderId)
                .sortBy((order) => order.price)
                .toArray()
                .filter((order) => order.symbol === 'ETH:BRL')
                .slice(0, 30);

            this.asks = [];
            lastVolume = 0.0;
            lastPrice = '#';

            const askMinMaxAmount = asks.reduce((acc, val) => {
                acc[0] = (acc[0] === undefined || val.amount < acc[0]) ? val.amount : acc[0]; // MIN
                acc[1] = (acc[1] === undefined || val.amount > acc[1]) ? val.amount : acc[1]; // MAX
                return acc;
            }, []);

            this.mountBookSide(asks, this.asks, lastVolume, lastPrice, askMinMaxAmount);
        }
    }

    public mountBookSide(side, thisSide, lastVolume, lastPrice, minMaxAmount): void {

        side.forEach(element => {

            lastVolume += parseFloat(element.amount);

            const stringPrice = String(Number(element.price).toFixed(BOOK_PRECISION));

            const priceDiffIdx = this.firstDifference(lastPrice, stringPrice);
            const floatAmount = parseFloat(element.amount);

            const amount = floatAmount.toFixed(8);

            lastPrice = stringPrice;

            const amountPeriodIdx = amount.indexOf('.');

            const totalAmountStr = lastVolume.toFixed(8);
            const totalAmountPeriodIdx = totalAmountStr.indexOf('.');

            // tslint:disable-next-line:max-line-length
            const barWidth = BOOK_BAR_MAX_WIDTH - ( ( (BOOK_BAR_MAX_WIDTH - BOOK_BAR_MIN_WIDTH) * (minMaxAmount[1] - floatAmount) ) / (minMaxAmount[1] - minMaxAmount[0]) );
            const normalizedAmount = barWidth < BOOK_BAR_MAX_WIDTH ? barWidth : BOOK_BAR_MAX_WIDTH;

            thisSide.push({
                amount: amount,
                amountOne: amount.substring(0, amountPeriodIdx),
                amountTwo: amount.substring(amountPeriodIdx),
                price: stringPrice,
                priceOne: stringPrice.substring(0, priceDiffIdx),
                priceTwo: stringPrice.substring(priceDiffIdx),
                totalAmount: totalAmountStr,
                totalAmountOne: totalAmountStr.substring(0, totalAmountPeriodIdx),
                totalAmountTwo: totalAmountStr.substring(totalAmountPeriodIdx),
                normalizedAmount
            });

        });
    }

    firstDifference(orig: string, match: string) {
        const shorter = Math.min(orig.length, match.length);

        for (let i = 0; i < shorter; i++) {
            if (orig[i] !== match[i]) {
                return i;
            }
        }
        return shorter;
    }

    trackByFnBid(index) {
        return index;
    }

    trackByFnAsk(index) {
        return index;
    }
}
