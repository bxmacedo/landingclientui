import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LBookComponent } from './l-book.component';

describe('LBookComponent', () => {
    let component: LBookComponent;
    let fixture: ComponentFixture<LBookComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LBookComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LBookComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
