import { Component, OnInit, OnChanges } from '@angular/core';
import { Auth0Service } from '../auth/auth0.service';

import { BROKER_NAME } from '../globals';

@Component({
    selector: 'app-landingpage',
    templateUrl: './landingpage.component.html',
    styleUrls: ['./landingpage.component.scss']
})
export class LandingpageComponent implements OnInit, OnChanges {

    public brokername: any;
    public profile: any;
    public btc: boolean;

    constructor(
        public auth: Auth0Service,
    ) {
        this.brokername = BROKER_NAME;
    }

    ngOnInit() {
        this.btc = false;
    }

    ngOnChanges() {
    }

}
