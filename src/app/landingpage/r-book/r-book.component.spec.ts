import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RBookComponent } from './r-book.component';

describe('RBookComponent', () => {
    let component: RBookComponent;
    let fixture: ComponentFixture<RBookComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RBookComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RBookComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
