
import {
    LOG_IN,
    LOG_OUT,
    SAVE_PROFILE,
    UPDATE_PROFILE,
    UPDATE_FULL_PROFILE,
    SAVE_AUTH,
    CREATE_SIGNUP,
    ALLOW_PAIR,
    UPDATE_PAIR_INFO,
    UPDATE_PAIR_LASTPX,
    SELECT_PAIR,
    UPDATE_BALANCE,
    RCVD_NEW_ORDER,
    RCVD_CANCEL_ORDER,
    MY_ORDER, // My open, cancelled and filled orders
    UPDATE_ORDER_STATUS,
    UPDATE_ORDER_EXECUTION,
    NEW_ORDER,
    SENT_ORDER,
    CONFIRMED_ORDER,
    EXECUTION_REPORT,
    MY_TRANSFER,
    NEW_CRYPTO_TRANSFER,
    NEW_FIAT_TRANSFER,
    UPDATE_TRANSFER,
    NEW_HISTORY,
    NEW_HISTORY_RESET,
    MY_HISTORY,
    MY_HISTORY_HAS_CHILD,
    WAITING_HISTORY,
    REMOVE_WAITING_HISTORY,
    MY_NOTIFICATION,
    MY_SETTING,
    MY_SAFETY,
    UPDATE_PAGE,
    LOAD_CANDLECHART,
    UPDATE_CANDLECHART,
    UPDATE_FIRST_ITEM_CANDLECHART,
    CREATE_FILTERED_CANDLECHART,
    RESET_FILTERED_CANDLECHART,
    RESET_DEPTHCHART,
    LOAD_DEPTHCHART,
    NEW_ACCOUNT,
    UPDATE_ACCOUNT,
    REMOVE_ACCOUNT,
    NEW_MESSAGE
} from './app.actions';
import { Map, List } from 'immutable';
import { combineReducers } from 'redux-immutable';

const INITIAL_STATE = Map({
    profile: Map({}),
    profileFull: Map({}),
    auth: Map({
        accessToken: '',
        idToken: '',
        expiresAt: ''
    }),
    signUp: Map({
        name: '',
        cpf: '',
        email: '',
        password: ''
    }),
    userStatus: Map({
        loggedIn: false,
    }),
    orders: List([]),
    myorders: Map({}),
    mytransfers: Map({}),
    historyNew: List([]),
    history: List([]),
    historyWaiting: List([]),
    orderbook: Map({
        bid: Map({}),
        ask: Map({})
    }),
    trades: List([]),
    tickers: Map({ // Available pairs should be informed by H3
        'ETH:BRL': Map({
            last: 0,
            variation: 0,
            volume: 0,
            allowed: false
        }),
        /*'BTC:BRL': Map({
            last: 0,
            variation: 0,
            volume: 0,
        })*/ // Available pairs should be informed by H3
    }),
    balances: Map({
        'BTC': Map({
            sequence: 0,
            value: 0
        }),
        'ETH': Map({
            sequence: 0,
            value: 0
        }),
        'BRL': Map({
            sequence: 0,
            value: 0
        }),
    }),
    accounts: Map([]),
    currentTicker: Map({
        pair: 'ETH:BRL'
    }),
    notifications: Map({
        buyCripto: false,
        sellCripto: false,
        contributionSubmission: false,
        contributionAproved: false,
        requestRescue: false,
        rescueAproved: false,
        monthlyHistory: false,
        btcQuotation: Map({
            decrease: Map({
                status: false,
                value: 0
            }),
            increase: Map({
                status: false,
                value: 0
            }),
        }),
        ethQuotation: Map({
            decrease: Map({
                status: false,
                value: 0
            }),
            increase: Map({
                status: false,
                value: 0
            }),
        }),
        newsAboutPlatform: false,
        newFeatures: false
    }),
    settings: Map({
        language: '',
        timezone: '',
        accountId: '',
        services: Map({
            gorila: false
        }),
        favoriteCoin: Map({
            btc: false,
            eth: false
        }),
        negotiationCoin: Map({
            btc: false,
            eth: false
        }),
    }),
    safety: Map({
        safeWord: ''
    }),
    page: Map({
        currentPage: 'Default Page',
        mode: 'full'
    }),
    candlechart: List([]),
    candlechartfiltered: List([]),
    depthchart: List([]),
    processedMessageId: List([])
});

const defaultReducer = (state = INITIAL_STATE, action) => {
    return List([]);
};

const profileReducer = (state = INITIAL_STATE.get('profile'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case SAVE_PROFILE:
            return Map(payload);
        case UPDATE_PROFILE:
            return Map({
                nickname: payload.nickname,
                picture: payload.picture,
                name: payload.name,
                password: payload.password,
                cpf: payload.cpf,
                birthdate: payload.birthdate,
                phone: payload.phone,
                experience: payload.experience
            });
        default:
            return state;
    }
};

const profileFullReducer = (state = INITIAL_STATE.get('profileFull'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case UPDATE_FULL_PROFILE:
            return Map({
                name: payload.name,
                email: payload.email,
                cpf: payload.cpf,
                cellPhone: payload.cellPhone,
                birthdate: payload.birthdate,
                mothersName: payload.mothersName,
                street: payload.street,
                selectedStade: payload.selectedState,
                number: payload.number,
                complement: payload.complement,
                city: payload.city,
                neigh: payload.neigh,
                zipCode: payload.zipCode
            });
        default:
            return state;
    }
};

const authReducer = (state = INITIAL_STATE.get('auth'), action) => {
    switch (action.type) {
        case SAVE_AUTH:
            return Map({
                accessToken: action.payload.accessToken,
                idToken: action.payload.idToken,
                expiresAt: action.payload.expiresAt
            });
        default:
            return state;
    }
};

const signUpReducer = (state = INITIAL_STATE.get('signUp'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case CREATE_SIGNUP:
            return Map({
                name: payload.name,
                cpf: payload.cpf,
                birthday: payload.birthday,
                email: payload.email,
                phone: payload.phone,
                cel: payload.cel,
                city: payload.city,
                state: payload.state,
                address: payload.address,
                number: payload.number,
                complement: payload.complement,
                zipCode: payload.zipCode,
                neightborhood: payload.neightborhood,
                mother: payload.mother,
                password: payload.password
            });
        default:
            return state;
    }
};

const userReducer = (state = INITIAL_STATE.get('userStatus'), action) => {
    switch (action.type) {
        case LOG_IN:
            return Map({
                loggedIn: true
            });
        case LOG_OUT:
            return Map({
                loggedIn: false
            });
        default:
            return state;
    }
};

const ordersReducer = (state = INITIAL_STATE.get('orders'), action) => {
    switch (action.type) {
        case NEW_ORDER:
            return Map(action.payload);
        case SENT_ORDER:
            return Map(action.payload);
        case CONFIRMED_ORDER:
            return Map(action.payload);
        default:
            return state;
    }
};

const tickersReducer = (state = INITIAL_STATE.get('tickers'), action) => {
    switch (action.type) {
        case ALLOW_PAIR:
            return state.setIn(
                [action.payload.pair],
                Map({
                    variation: state.toJS()[action.payload.pair].variation,
                    last: state.toJS()[action.payload.pair].last,
                    volume: state.toJS()[action.payload.pair].volume,
                    allowed: true
                }));
        case UPDATE_PAIR_INFO:
            return state.setIn(
                [action.payload.pair],
                Map({
                    variation: action.payload.variation,
                    last: action.payload.last,
                    volume: action.payload.volume
                }));
        case UPDATE_PAIR_LASTPX:
            return state.setIn(
                [action.payload.pair, 'last'],
                action.payload.last);
        default:
            return state;
    }
};

const currentTickerReducer = (state = INITIAL_STATE.get('currentTicker'), action) => {
    switch (action.type) {
        case SELECT_PAIR:
            return Map(action.payload);
        default:
            return state;
    }
};

const accountReducer = (state: any = INITIAL_STATE.get('accounts'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case NEW_ACCOUNT:
            return state.set(payload.accId, {
                accId: payload.accId,
                address: payload.address,
                symbol: payload.symbol,
                visible: payload.visible

            });
        case UPDATE_ACCOUNT:
            return state.set(payload.accId, {
                accId: payload.accId,
                address: payload.address,
                symbol: payload.symbol,
                visible: payload.visible
            });
        case REMOVE_ACCOUNT:
            return state.delete(payload.accId);

        default:
            return state;
    }
};

const walletReducer = (state: any = INITIAL_STATE.get('balances'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case UPDATE_BALANCE:
            return state.set(payload.symbol, {
                sequence: payload.sequence,
                value: payload.value
            });
        default:
            return state;
    }
};

const orderbookReducer = (state = INITIAL_STATE.get('orderbook'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case RCVD_NEW_ORDER:
            if (payload.side === 'B') {
                return state.updateIn(['bid'], orders => orders.set(payload.orderId, {
                    orderId: payload.orderId,
                    symbol: payload.symbol,
                    side: payload.side,
                    amount: payload.amount,
                    price: payload.price
                }));
            } else if (payload.side === 'S') {
                return state.updateIn(['ask'], orders => orders.set(payload.orderId, {
                    orderId: payload.orderId,
                    symbol: payload.symbol,
                    side: payload.side,
                    amount: payload.amount,
                    price: payload.price
                }));
            } else {
                return state;
            }
        case RCVD_CANCEL_ORDER:
            if (payload.side === 'B') {
                return state.removeIn(['bid', payload.orderId]);
            } else if (payload.side === 'S') {
                return state.removeIn(['ask', payload.orderId]);
            } else {
                return state;
            }
        default:
            return state;
    }
};

const tradesReducer = (state: any = INITIAL_STATE.get('trades'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case EXECUTION_REPORT:
            return state.slice(0, 199).unshift(payload);
        default:
            return state;
    }
};

const myordersReducer = (state: any = INITIAL_STATE.get('myorders'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case MY_ORDER:
            return state.set(payload.orderId, {
                orderId: payload.orderId,
                pair: payload.pair,
                side: payload.side,
                status: payload.status,
                size: payload.size,
                price: payload.price,
                filled: payload.filled,
                time: payload.time,
                externalOrderId: payload.externalOrderId
            });
        case UPDATE_ORDER_STATUS:
            return state.set(
                payload.orderId,
                {
                    orderId: payload.orderId,
                    pair: state.toJS()[payload.orderId].pair,
                    side: state.toJS()[payload.orderId].side,
                    status: payload.status,
                    size: state.toJS()[payload.orderId].size,
                    price: state.toJS()[payload.orderId].price,
                    filled: state.toJS()[payload.orderId].filled,
                    time: payload.time,
                    externalOrderId: state.toJS()[payload.orderId].externalOrderId
                });
        case UPDATE_ORDER_EXECUTION:
            return state.set(payload.orderId, {
                orderId: payload.orderId,
                pair: payload.pair,
                side: payload.side,
                status: payload.status,
                size: (state.toJS()[payload.orderId] === undefined) ? 0 :  state.toJS()[payload.orderId].size,
                price: payload.price,
                filled: payload.filled,
                time: payload.time,
                externalOrderId: payload.externalOrderId
            });
        default:
            return state;
    }
};

const transferReducer = (state: any = INITIAL_STATE.get('mytransfers'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case MY_TRANSFER:
            return state.set(payload.transferId, {
                transferId: payload.transferId,
                type: payload.type,
                symbol: payload.symbol,
                amount: payload.amount,
                address: payload.address,
                status: payload.status,
                time: payload.time
            });
        case NEW_CRYPTO_TRANSFER:
            return state.set(payload.transferId, {
                transferId: payload.transferId,
                type: payload.type,
                address: payload.address,
                symbol: payload.symbol,
                amount: payload.amount,
                status: payload.status,
                time: payload.time
            });
        case NEW_FIAT_TRANSFER:
            return state.set(payload.transferId, {
                transferId: payload.transferId,
                type: payload.type,
                symbol: payload.symbol,
                amount: payload.amount,
                status: payload.status,
                time: payload.time
            });
        case UPDATE_TRANSFER:
            return state.set(payload.transferId, {
                transferId: payload.transferId,
                type: state.toJS()[payload.transferId].type,
                address: state.toJS()[payload.transferId].address,
                symbol: payload.symbol,
                amount: payload.amount,
                status: payload.status,
                time: payload.time
            });
        default:
            return state;
    }
};

const historyNewReducer = (state: any = INITIAL_STATE.get('historyNew'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case NEW_HISTORY:
            return state.push(
                {
                    OperationId: payload.OperationId,
                    UserId: payload.UserId,
                    Symbol: payload.Symbol,
                    Currency: payload.Currency,
                    Notional: payload.Notional,
                    Balance: payload.Balance,
                    SeqId: payload.SeqId,
                    ParentOperationId: payload.ParentOperationId,
                    TimeStamp: payload.TimeStamp,
                    OperationType: payload.OperationType
                }
            );
        case NEW_HISTORY_RESET:
            return state = List([]);
        default:
            return state;
    }
};

const historyReducer = (state: any = INITIAL_STATE.get('history'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case MY_HISTORY:
            return state.push(
                {
                    id: payload.id,
                    parentId: payload.parentId,
                    type: payload.type,
                    date: payload.date,
                    value: payload.value,
                    fee: payload.fee,
                    symbol: payload.symbol,
                    balance: payload.balance,
                    exchange: payload.exchange,
                    childs: payload.childs,
                    total: payload.total
                }
            );
        case MY_HISTORY_HAS_CHILD:
            return state.forEach(history => {
                if (state.indexOf(history) === payload.position) {
                    history.childs.push(
                        {
                            id: payload.id,
                            parentId: payload.parentId,
                            type: payload.type,
                            date: payload.date,
                            value: payload.value,
                            fee: payload.fee,
                            symbol: payload.symbol,
                            balance: payload.balance,
                            exchange: payload.exchange,
                            childs: payload.childs,
                            total: payload.total
                        }
                    );
                }
            });
        default:
            return state;
    }
};

const historyWaitingReducer = (state: any = INITIAL_STATE.get('historyWaiting'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case WAITING_HISTORY:
            return state.push(
                {
                    id: payload.id,
                    parentId: payload.parentId,
                    type: payload.type,
                    date: payload.date,
                    value: payload.value,
                    fee: payload.fee,
                    symbol: payload.symbol,
                    balance: payload.balance,
                    exchange: payload.exchange,
                    childs: payload.childs,
                    total: payload.total
                }
            );
        case REMOVE_WAITING_HISTORY:
            return state.splice(payload.position, 1);
        default:
            return state;
    }
};

const notificationReducer = (state: any = INITIAL_STATE.get('notifications'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case MY_NOTIFICATION:
            return Map({
                buyCripto: payload.buyCripto,
                sellCripto: payload.sellCripto,
                contributionSubmission: payload.contributionSubmission,
                contributionAproved: payload.contributionAproved,
                requestRescue: payload.requestRescue,
                rescueAproved: payload.rescueAproved,
                monthlyHistory: payload.monthlyHistory,
                btcQuotation: payload.btcQuotation,
                ethQuotation: payload.ethQuotation,
                newsAboutPlatform: payload.newsAboutPlatform,
                newFeatures: payload.newFeatures
            });
        default:
            return state;
    }
};

const settingsReducer = (state: any = INITIAL_STATE.get('settings'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case MY_SETTING:
            return Map({
                language: payload.language,
                timezone: payload.timezone,
                accountId: payload.accountId,
                services: payload.services,
                favoriteCoin: payload.favoriteCoin,
                negotiationCoin: payload.negotiationCoin
            });
        default:
            return state;
    }
};

const safetyReducer = (state: any = INITIAL_STATE.get('safety'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case MY_SAFETY:
            return Map({
                safeWord: payload.safeWord,
            });
        default:
            return state;
    }
};


const pageReducer = (state = INITIAL_STATE.get('page'), action) => {
    switch (action.type) {
        case UPDATE_PAGE:
            return Map(action.payload);
        default:
            return state;
    }
};

const candlechartReducer = (state: any = INITIAL_STATE.get('candlechart'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case LOAD_CANDLECHART:
            return List([
                {
                    date: payload.date,
                    l: payload.l,
                    h: payload.h,
                    o: payload.o,
                    c: payload.c,
                    pair: payload.pair
                }
            ]);
        case UPDATE_CANDLECHART:
            return state
                .sortBy((candledata) => -candledata.date)
                .unshift(payload);
        case UPDATE_FIRST_ITEM_CANDLECHART:
            return state
                .sortBy((candledata) => -candledata.date)
                .shift()
                .unshift(payload);
        default:
            return state;
    }
};

const candlechartfilteredReducer = (state: any = INITIAL_STATE.get('candlechartfiltered'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case CREATE_FILTERED_CANDLECHART:
            return List(payload);
        case RESET_FILTERED_CANDLECHART:
            return List();
        default:
            return state;
    }
};

const depthchartReducer = (state: any = INITIAL_STATE.get('depthchart'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case RESET_DEPTHCHART:
            return List();
        case LOAD_DEPTHCHART:
            return List(payload);
        default:
            return state;
    }
};

const messageReducer = (state: any = INITIAL_STATE.get('processedMessageId'), action) => {
    const payload = action.payload;
    switch (action.type) {
        case NEW_MESSAGE:
            return state.push(payload);
        default:
            return state;
    }
};


export const rootReducer = combineReducers({
    profile: profileReducer,
    profileFull: profileFullReducer,
    auth: authReducer,
    signUp: signUpReducer,
    orders: ordersReducer,
    myorders: myordersReducer,
    orderbook: orderbookReducer,
    trades: tradesReducer,
    tickers: tickersReducer,
    transfers: transferReducer,
    balances: walletReducer,
    currentTicker: currentTickerReducer,
    historyNew: historyNewReducer,
    history: historyReducer,
    historyWaiting: historyWaitingReducer,
    notifications: notificationReducer,
    settings: settingsReducer,
    safety: safetyReducer,
    page: pageReducer,
    candlechart: candlechartReducer,
    candlechartfiltered: candlechartfilteredReducer,
    depthchart: depthchartReducer,
    accounts: accountReducer,
    userStatus: userReducer,
    processedMessageId: messageReducer
});
