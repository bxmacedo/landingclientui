import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreSignUpSuccessComponent } from './pre-sign-up-success.component';

describe('PreSignUpSuccessComponent', () => {
    let component: PreSignUpSuccessComponent;
    let fixture: ComponentFixture<PreSignUpSuccessComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PreSignUpSuccessComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PreSignUpSuccessComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
