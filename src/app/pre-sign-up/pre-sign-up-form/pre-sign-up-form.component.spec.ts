import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreSignUpFormComponent } from './pre-sign-up-form.component';

describe('PreSignUpFormComponent', () => {
  let component: PreSignUpFormComponent;
  let fixture: ComponentFixture<PreSignUpFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreSignUpFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreSignUpFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
