import { Component, OnInit, Inject } from '@angular/core';
import { AppStore } from '../../app.store';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { TermsModalComponent } from '../../shared/terms-modal/terms-modal.component';
import { PrivacyModalComponent } from '../../shared/privacy-modal/privacy-modal.component';


@Component({
    selector: 'app-pre-sign-up-form',
    templateUrl: './pre-sign-up-form.component.html',
    styleUrls: ['./pre-sign-up-form.component.scss']
})
export class PreSignUpFormComponent implements OnInit {

    public form: FormGroup;
    public userName: string;
    public email: string;
    public acceptTerms: boolean;

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
        private dialog: MatDialog,
    ) { }

    ngOnInit() {
        this.acceptTerms = false;
        const store = this.store.getState().toJS().signUp;
        this.userName = store.name;
        this.form = new FormGroup({
            'name': new FormControl(this.userName, Validators.required),
            'email': new FormControl(this.email, [Validators.required, Validators.email]),
        });
    }

    public verifyData(): void {
        if (!this.acceptTerms) {
            alert('Leia e aceite os termos de uso e política de privacidade!');
        } else if (this.form.controls.email.status === 'VALID'
            && this.form.value.name !== '' && this.form.value.lastName !== '') {
            this.router.navigate(['pre-cadastro/sucesso']);
            // this.store.dispatch(ProfileActions.createSignUp(
            //     this.userName,
            //     this.email,
            //     '',
            //     ''
            // ));
        }
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'name') {
            document.getElementById('email').focus();
        } else if (element === 'email') {
            this.verifyData();
        }
    }

    public openTermsDialog(): void {
        this.dialog.open(TermsModalComponent);
    }

    public openPrivacyDialog(): void {
        this.dialog.open(PrivacyModalComponent);
    }
}
