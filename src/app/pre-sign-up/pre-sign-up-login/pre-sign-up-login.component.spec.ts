import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreSignUpLoginComponent } from './pre-sign-up-login.component';

describe('PreSignUpLoginComponent', () => {
  let component: PreSignUpLoginComponent;
  let fixture: ComponentFixture<PreSignUpLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreSignUpLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreSignUpLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
