import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreSignUpAlreadyExistsComponent } from './pre-sign-up-already-exists.component';

describe('PreSignUpAlreadyExistsComponent', () => {
  let component: PreSignUpAlreadyExistsComponent;
  let fixture: ComponentFixture<PreSignUpAlreadyExistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreSignUpAlreadyExistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreSignUpAlreadyExistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
