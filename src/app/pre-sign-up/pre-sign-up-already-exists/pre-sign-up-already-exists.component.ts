import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pre-sign-up-already-exists',
  templateUrl: './pre-sign-up-already-exists.component.html',
  styleUrls: ['./pre-sign-up-already-exists.component.scss']
})
export class PreSignUpAlreadyExistsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
