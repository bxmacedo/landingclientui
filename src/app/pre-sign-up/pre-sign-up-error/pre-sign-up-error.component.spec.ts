import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreSignUpErrorComponent } from './pre-sign-up-error.component';

describe('PreSignUpErrorComponent', () => {
    let component: PreSignUpErrorComponent;
    let fixture: ComponentFixture<PreSignUpErrorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PreSignUpErrorComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PreSignUpErrorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
