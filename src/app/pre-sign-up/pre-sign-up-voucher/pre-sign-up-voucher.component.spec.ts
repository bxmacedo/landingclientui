import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreSignUpVoucherComponent } from './pre-sign-up-voucher.component';

describe('PreSignUpVoucherComponent', () => {
    let component: PreSignUpVoucherComponent;
    let fixture: ComponentFixture<PreSignUpVoucherComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PreSignUpVoucherComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PreSignUpVoucherComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
