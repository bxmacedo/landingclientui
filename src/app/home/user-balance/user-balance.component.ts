import { Inject, Component, OnInit } from '@angular/core';
import { AppStore } from '../../app.store';

@Component({
    selector: 'app-user-balance',
    templateUrl: './user-balance.component.html',
    styleUrls: ['./user-balance.component.scss']
})
export class UserBalanceComponent implements OnInit {

    public balanceBRL;
    public balanceETH;
    public balanceBTC;

    public currentCryptoSymbol;

    constructor(
        @Inject(AppStore) private store,

    ) {
        this.updateMyBalances();
        store.subscribe(() => this.updateMyBalances());
    }

    updateMyBalances() {
        const stateBalances = this.store.getState().get('balances').sortBy((balance) => balance.symbol).toJS();
        const pair = this.store.getState().get('currentTicker').toJS().pair;

        this.currentCryptoSymbol = pair.substring(0, pair.indexOf(':'));

        this.balanceBRL = stateBalances.BRL.value;
        this.balanceBTC = stateBalances.BTC.value;
        this.balanceETH = stateBalances.ETH.value;

    }

    ngOnInit() {
    }

}
