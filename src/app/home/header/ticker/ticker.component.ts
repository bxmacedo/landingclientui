import { Component, OnInit, Inject, Input } from '@angular/core';
import { Auth0Service } from '../../../auth/auth0.service';
import { MktDataService } from '../../../../api/mktdata.service';
import { AppStore } from '../../../app.store';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.scss']
})
export class TickerComponent implements OnInit {
    public availablePairs: string[];
    public currentPair: string;
    public currentCryptoSymbol: string;
    public currentPairBeautified: string;
    public lastPrice: number;
    public volume: number;
    public variation: number;
    public pairs = [{}];

    public message;

    constructor(
        private auth: Auth0Service,
        @Inject(MktDataService) private mktDataService: MktDataService,
        @Inject(AppStore) private store
    ) {
    }

    ngOnInit() { }

    public logout(): void {
        const state = this.store.getState().toJS();
        this.currentPair = this.store.getState().get('currentTicker').toJS().pair;
        // SEND UNSUBSCRIBE
        this.mktDataService.subscribe(this.currentPair, false);
        this.auth.logout();
    }
}
