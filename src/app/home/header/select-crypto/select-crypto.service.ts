import { Injectable, EventEmitter } from '@angular/core';



@Injectable()
export class SelectCryptoService {

    static changeValue: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }


    onChanged(matSelectChange) {
        SelectCryptoService.changeValue.emit(matSelectChange);
    }
}
