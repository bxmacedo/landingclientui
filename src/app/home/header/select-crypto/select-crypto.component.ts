import { Component, Inject, OnInit } from '@angular/core';
import { TICKER_RENDER_INTERVAL } from '../../../app.constants';
import { AppStore } from '../../../app.store';
import { TickerActions } from '../../../app.actions';
import { MktDataService } from '../../../../api/mktdata.service';
import { SelectCryptoService } from './select-crypto.service';
import {SelectCryptoOrderService} from '../../orderentry/select-crypto-order.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'select-crypto',
    templateUrl: './select-crypto.component.html',
    styleUrls: ['./select-crypto.component.scss'],
    providers: [SelectCryptoService, SelectCryptoOrderService],
})
export class SelectCryptoComponent implements OnInit {
    public currentPair: string;
    public availablePairs: string[];

    constructor(
        private selectCryptoService: SelectCryptoService,
        @Inject(MktDataService) private mktDataService: MktDataService,
        @Inject(AppStore) private store
    ) {
        this.startUpdateSelectCrypto();
    }

    startUpdateSelectCrypto() {
        this.timer(this.updateSelectCrypto.bind(this), TICKER_RENDER_INTERVAL);
    }

    timer(callback, milliseconds) {
        setTimeout(() => {
            callback();
            this.timer(callback, milliseconds);
        }, milliseconds);
    }

    ngOnInit() {
        SelectCryptoOrderService.changeValueOrder.subscribe(
            matRadioChange => {
                this.onChangedOrder(matRadioChange);
            }
        );
    }

    updateSelectCrypto() {
        const state = this.store.getState().toJS();
        this.availablePairs = Object.keys(state.tickers);
        this.currentPair = this.store.getState().get('currentTicker').toJS().pair;
    }

    onChangedPair(matSelectChange) {
        // SEND UNSUBSCRIBE
        this.mktDataService.subscribe(this.currentPair, false);
        // SEND SUBSCRIBE
        this.mktDataService.subscribe(matSelectChange.value, true);
        this.store.dispatch(TickerActions.currentTicker(matSelectChange.value));

       this.selectCryptoService.onChanged(matSelectChange);
    }

    onChangedOrder(matSelectChange) {
        // SEND UNSUBSCRIBE
        this.mktDataService.subscribe(this.currentPair, false);
        // SEND SUBSCRIBE
        this.mktDataService.subscribe(matSelectChange, true);
        this.store.dispatch(TickerActions.currentTicker(matSelectChange));

    }

}
