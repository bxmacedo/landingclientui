import { Component, OnInit, EventEmitter, Output, Input, Inject } from '@angular/core';
import { BROKER_NAME } from '../../app.constants';
import { AppStore } from '../../app.store';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    brokerName: string = BROKER_NAME;

    @Input() sidebarMode: string;
    @Input() profile: Object;
    @Output() changeToggle = new EventEmitter<any>();
    @Input() menuMobile: boolean;
    @Output() menuChange = new EventEmitter<any>();

    constructor(
        @Inject(AppStore) private store,
    ) { }

    ngOnInit() {
        this.profile = this.store.getState().toJS().profile;
    }

    toggleCollapseSidebar() {
        this.changeToggle.emit('toggle');
    }
    toggleMenuMobile(): void {
        this.menuMobile = !this.menuMobile;
        this.menuChange.emit({ v: this.menuMobile });
    }
}

