import { Component, OnInit, Inject, Input } from '@angular/core';
import { TICKER_RENDER_INTERVAL } from '../../../app.constants';
import { AppStore } from '../../../app.store';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'crypto-type',
    templateUrl: './crypto-type.component.html',
    styleUrls: ['./crypto-type.component.scss']
})


export class CryptoTypeComponent implements OnInit {

    public currentPair: string;
    public currentCryptoSymbol: string;
    public currentPairBeautified: string;
    public lastPrice: number;
    public volume: number;
    public variation: number;

    constructor(
        @Inject(AppStore) private store,
    ) {
        this.startUpdateCryptoType();
    }

    ngOnInit() { }

    startUpdateCryptoType() {
        this.timer(this.updateCryptoType.bind(this), TICKER_RENDER_INTERVAL);
    }

    timer(callback, milliseconds) {
        setTimeout(() => {
            callback();
            this.timer(callback, milliseconds);
        }, milliseconds);
    }

    updateCryptoType() {
        const state = this.store.getState().toJS();
        this.currentPair = this.store.getState().get('currentTicker').toJS().pair;

        this.currentCryptoSymbol = this.currentPair.substring(0, this.currentPair.indexOf(':'));

        this.currentPairBeautified = this.currentPair.replace(':', '/');
        this.currentPairBeautified = this.currentPairBeautified.replace('BRL', 'R$');

        const currentTicker = state.tickers[this.currentPair];

        if (currentTicker !== undefined) {
            this.lastPrice = currentTicker.last;
            this.variation = currentTicker.variation;
            this.volume = currentTicker.volume;
        }
    }

}
