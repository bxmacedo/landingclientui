import { Component, OnInit, Inject, Input,  ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { AppStore } from '../../../../app.store';
import { TradeService } from '../../../../../api/trade.service';
import { MatSnackBar, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { OrderActions } from '../../../../app.actions';
import { OrderStatus } from '../../../../shared/models/order/order.status';
import { Order } from '../../../../shared/models/order/order';
import { MessageType } from '../../../../shared/models/message/message.type';
import { OrderSide } from '../../../../shared/models/order/order.side';
import { MYORDERS_RENDER_INTERVAL } from '../../../../app.constants';
import { SnackBarMessage } from '../../../../shared/message/snackbar-message.component';
import * as moment from 'moment';

@Component({
    selector: 'app-myorders',
    templateUrl: './myorders.component.html',
    styleUrls: ['./myorders.component.scss']
})
export class MyordersComponent implements OnInit, AfterViewInit, OnDestroy {
    displayedColumns = ['time', 'size', 'filled', 'price', 'side', 'status', 'actions'];
    dataSource = new MatTableDataSource();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @Input() tabActive;
    @Input() ordersAllOpen;
    public counter;
    public orders: Order[] = []; 
    public valueFilterStatus = '';
    public valueFilterType = '';
    // these props will be used in the future
    //public valueFilterDate = 'Recent';
    //public oldOrdersDate = moment().subtract(30, 'days');

    constructor (
        @Inject(AppStore) private store,
        @Inject(TradeService) private tradeService: TradeService,
        public snackBar: MatSnackBar,
    ) {
        // store.subscribe(() => this.updateMyOrders(this.ordersAllOpen));
    }


    applyFilters(filtersValue: string) {
        filtersValue = filtersValue.trim().toLowerCase();
        this.dataSource.filter = filtersValue;

    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    updateMyOrders(ordersAllOpen) {
        this.orders = this.store.getState().get('myorders').sortBy((order) => -order.time).toList().toArray();
        if (ordersAllOpen == 1) {
            this.orders =  this.orderByStatus();
        }
        const pair = this.store.getState().get('currentTicker').toJS().pair;

        //filter Orders Date. Open Orders Tab show all orders
        /* --- This filter will be shown in other screen in the future
        if (ordersAllOpen == 0) {
            if (this.valueFilterDate === 'Recent') {
                this.orders = this.orders.filter(order => moment(order.time) >= this.oldOrdersDate);
            } else {
                this.orders = this.orders.filter(order => moment(order.time) <= this.oldOrdersDate);
            }
        }
        */

        this.dataSource.data = this.orders.filter((obj) => obj.pair === pair);
    }

    orderByStatus() {
        const ordersAux: Order[] = [];
        for (let i = 0; i < this.orders.length; i++) {
            if (this.orders[i].status !== undefined) {
                if (this.orders[i].status === OrderStatus.Created ||
                    this.orders[i].status === OrderStatus.New ||
                    this.orders[i].status === OrderStatus.PartiallyFilled ||
                    this.orders[i].status === OrderStatus.Confirmed ||
                    this.orders[i].status === OrderStatus.Sent )  {
                    ordersAux.push(this.orders[i]);
                }
            }
        }
        return ordersAux;
    }

    ngOnDestroy() {
        clearTimeout(this.counter);
    }

    cancelOrder(orderId) {
        const cancelOrder = this.orders.find(order => order.orderId === orderId);
        console.log('Cancel: ' + orderId);
        console.log(cancelOrder);
        this.tradeService.send(cancelOrder, this.store.getState().get('currentTicker').toJS().pair, MessageType.CancelOrder);
        this.store.dispatch(
            OrderActions.myOrder(
                orderId,
                cancelOrder.pair,
                cancelOrder.side,
                OrderStatus.Canceling,
                cancelOrder.size,
                cancelOrder.price,
                cancelOrder.filled,
                cancelOrder.time,
                cancelOrder.externalOrderId
            )
        );
        this.snackBar.openFromComponent(SnackBarMessage, {
            duration: 4500,
            data: 'Cancelamento enviado! Aguardando confirmação de cancelamento.',
            panelClass: ['snackbar-messsage']
        });

    }
    public timer(): void {
        this.counter = setTimeout(() => {
            this.updateMyOrders(this.ordersAllOpen);
            this.timer();
        }, MYORDERS_RENDER_INTERVAL);
    }

    ngOnInit() {
        this.dataSource.filterPredicate =
            (data: Order, filters: string) => {
            const matchFilter = [];
            const filterArray = filters.split('+');
            const columns = (<any>Object).values([data.side, data.status]);
            // define colunas para filtrar
            filterArray.forEach(filter => {
                const customFilter = [];
                columns.forEach(column => customFilter.push(column.toLowerCase().includes(filter)));
                matchFilter.push(customFilter.some(Boolean)); // OR
            });
            return matchFilter.every(Boolean); // AND
        };
        this.updateMyOrders(this.ordersAllOpen);
        this.timer();
    }

    canCancelOrder (status: OrderStatus) {
        return (
            status === OrderStatus.Created ||
            status === OrderStatus.Sent ||
            status === OrderStatus.Confirmed ||
            status === OrderStatus.New ||
            status === OrderStatus.PartiallyFilled
        );
    }

    filterStatus(statusOrder: string) {
        if (statusOrder.localeCompare(OrderStatus.AllStatus) != 0) {
            this.valueFilterStatus = statusOrder;
        } else {
            this.valueFilterStatus = '';
        }

        const filters = this.valueFilterType  + '+' + this.valueFilterStatus;
        this.applyFilters(filters);

    }

    filterType(typeStatus: string) {
        if (typeStatus.localeCompare(OrderSide.AllTO) != 0) {
            this.valueFilterType = typeStatus;
        } else {
            this.valueFilterType = '';
        }
        const filters = this.valueFilterType  + '+' + this.valueFilterStatus;
        this.applyFilters(filters);
    }


    setSymbolStatus(status: OrderStatus) {
        if (status === OrderStatus.Sent) {
            return 'status-sent';
        } else if (status === OrderStatus.Canceling) {
            return 'status-canceling';
        }
    }
    
    /* this filter will be shown in other screen in the future
    filterDate(dateType: string) {
        this.valueFilterDate = dateType;
        this.updateMyOrders(this.ordersAllOpen);

    }
    */

}
