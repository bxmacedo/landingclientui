import { Component, OnInit, Inject, Output, Input, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { OrderStatus } from '../../../../shared/models/order/order.status';
import { OrderSide } from '../../../../shared/models/order/order.side';
import { TransferType } from '../../../../shared/models/transfer/transfer.type';
import { TransferStatus } from '../../../../shared/models/transfer/transfer.status';

@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss']
})

export class FilterComponent implements OnInit {

    public initalStatus = 'AllStatus';
    public statusType: String[];
    public typeMovement: String[];
    public inicialType: String = 'AllTO';
    @Input() tabSelected;
    @Input() isOrdersAllOpen;
    @Output() statusChange: EventEmitter<string> = new EventEmitter<string>();
    @Output() typeChange: EventEmitter<string> = new EventEmitter<string>();
    //This commented props will be used in the future
    //public initialDate: String = 'Recent';
    //public dateType: String[];
    //@Output() dateChange: EventEmitter<string> = new EventEmitter<string>();


    constructor() { }

    ngOnInit() {
        if (this.tabSelected == 1) {
            this.setMovementType(OrderSide);
            this.setStatusOrders(OrderStatus);
        } else {
            this.setMovementType(TransferType);
            this.setStatusOrders(TransferStatus);
        }
        //to use in future updates
        //this.setDateInterval()
    }

    setStatusOrders(typeStatus) {
        this.statusType = [];
        Object.keys(typeStatus).forEach(key => {
            if (typeStatus[key] !== OrderStatus.InsufficientBrokerFunds
                &&
                typeStatus[key] !== OrderStatus.InsufficientClientFunds
            ) {
                this.statusType.push(typeStatus[key]);
            }
        });
    }

    setMovementType(typeOption) {
        this.typeMovement = [];
        Object.keys(typeOption).forEach(key => {
            this.typeMovement.push(typeOption[key]);
        });
    }

    /* to use in future updates
    setDateInterval() {
        this.dateType = ['Recent', 'Old'];
        //const keys = Object.keys({ recent: 'recent', older: 'older' });
    }
    */

    filterStatus(MatSelectChange: any) {
        this.statusChange.emit(MatSelectChange.value);
    }

    filterType(MatSelectChange: any) {
        this.typeChange.emit(MatSelectChange.value);
    }

    /* to use in future updates
    filterDate(MatSelectChange: any) {
        this.dateChange.emit(MatSelectChange.value);
    }
    */

}
