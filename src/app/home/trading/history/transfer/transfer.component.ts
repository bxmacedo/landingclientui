import { Inject, Component, OnInit, Input, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { AppStore } from '../../../../app.store';
import { Transfer } from '../../../../shared/models/transfer/transfer';
import { OrderStatus } from '../../../../shared/models/order/order.status';
import { TransferStatus } from '../../../../shared/models/transfer/transfer.status';
import { OrderSide } from '../../../../shared/models/order/order.side';
import { TRANSFER_RENDER_INTERVAL } from '../../../../app.constants';
import * as moment from 'moment';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit, AfterViewInit, OnDestroy {
    displayedColumns = ['time', 'amount', 'type', 'symbol', 'status'];
    dataSource = new MatTableDataSource();

    @ViewChild(MatSort) sort: MatSort;
    @Input() tabActive;
    @Input() transfersAllOpen;
    public transfers: Transfer[];
    public valueFilterStatus = '';
    public valueFilterType = '';
    public counter;
    public valueFilterDate = 'Recent';
    public oldTransfersDate = moment().subtract(30, 'days');

    constructor(
        @Inject(AppStore) private store,
    ) {
        // store.subscribe(() => this.updateMyTransfers());
    }

    updateMyTransfers(transfersAllOpen) {
        this.transfers = this.store.getState().get('transfers').sortBy((transfer) => -transfer.time).toList().toArray();
        if (this.transfersAllOpen == 1) {
            this.transfers =  this.transferByStatus();
        }

        //filter Transfers Date. Open Transfers Tab show all transfers
        if (transfersAllOpen == 0) {
            if (this.valueFilterDate === 'Recent') {
                this.transfers = this.transfers.filter(transfer => moment(transfer.time) >= this.oldTransfersDate);
            } else {
                this.transfers = this.transfers.filter(transfer => moment(transfer.time) <= this.oldTransfersDate);
            }
        }

        this.dataSource.data = this.transfers;
    }

    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
    }

    ngOnInit() {
        this.dataSource.filterPredicate =
            (data: Transfer, filters: string) => {
            const matchFilter = [];
            const filterArray = filters.split('+');
            const columns = (<any>Object).values([data.type, data.status]);
            // define colunas para filtrar

            filterArray.forEach(filter => {
                const customFilter = [];
                columns.forEach(column => customFilter.push(column.toLowerCase().includes(filter)));
                matchFilter.push(customFilter.some(Boolean)); // OR
            });
            return matchFilter.every(Boolean); // AND
        }
        this.timer();
        this.updateMyTransfers(this.transfersAllOpen);
    }
    ngOnDestroy() {
        clearTimeout(this.counter);
    }
    transferByStatus() {
        const transferAux: Transfer[] = [];
        for (let i = 0; i < this.transfers.length; i++) {
            if (this.transfers[i].status !== undefined) {
                if (this.transfers[i].status === TransferStatus.New ||
                    this.transfers[i].status === TransferStatus.Approved)  {
                        transferAux.push(this.transfers[i]);
                }
            }
        }
        return transferAux;
    }

    applyFilters(filtersValue: string) {
        filtersValue = filtersValue.trim().toLowerCase();
        this.dataSource.filter = filtersValue;

    }

    public timer(): void {
        this.counter = setTimeout(() => {
            this.updateMyTransfers(this.transfersAllOpen);
            this.timer();
        }, TRANSFER_RENDER_INTERVAL);
    }


    filterStatus(statusOrder: string) {
        if (statusOrder.localeCompare(OrderStatus.AllStatus) != 0) {
            this.valueFilterStatus = statusOrder;
        } else {
            this.valueFilterStatus = '';
        }

        const filters = this.valueFilterStatus + '+' + this.valueFilterType;
        this.applyFilters(filters);

    }

    filterType(typeStatus: string) {
        if (typeStatus.localeCompare(OrderSide.AllTO) != 0) {
            this.valueFilterType = typeStatus;
        } else {
            this.valueFilterType = '';
        }
        const filters = this.valueFilterStatus + '+' + this.valueFilterType;
        this.applyFilters(filters);
    }

    filterDate(dateType: string) {
        this.valueFilterDate = dateType;
        this.updateMyTransfers;
    }

}
