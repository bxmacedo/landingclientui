import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  public myOrdersOpen = true;
  public myOrdersAll = false;
  public transferAll = false;
  public transferOpen = false;
  constructor() { }

    ngOnInit() {
    }

    isMyOrdersOpen() {
      this.myOrdersOpen = true;
      this.myOrdersAll = false;
      this.transferAll = false;
      this.transferOpen = false;
    }
    isMyOrdersAll() {
      this.myOrdersOpen = false;
      this.myOrdersAll = true;
      this.transferAll = false;
      this.transferOpen = false;
    }
    isTranserAll() {
      this.myOrdersOpen = false;
      this.myOrdersAll = false;
      this.transferAll = true;
      this.transferOpen = false;
    }
    isTranserOpen() {
      this.myOrdersOpen = false;
      this.myOrdersAll = false;
      this.transferAll = false;
      this.transferOpen = true;
    }

}
