import { Component, OnInit, Inject, OnDestroy, } from '@angular/core';
import { Auth0Service } from '../../auth/auth0.service';

import { AppStore } from '../../app.store';

@Component({
    selector: 'app-trading',
    templateUrl: './trading.component.html',
    styleUrls: ['./trading.component.scss']
})
export class TradingComponent implements OnInit, OnDestroy {

    public pair: string;
    public cryptoSymbol: string;
    public fiatSymbol: string;
    public currentPage = 'Default Page';

    public currentBookTab = 0;
    public clickOutEnabled = false;
    public clickOutEnabledWithDep = false;
    public profile: Object = {};

    public collapseBtn = false;
    public bookConsolidated = false;

    public bookGraf = true;
    public mobWidth: any;

    public chartMobile = 'chartMobile';
    public chartDesktop = 'chartDesktop';

    public sidebarMode;
    public counter;

    constructor(
        @Inject(Auth0Service) public auth: Auth0Service,
        @Inject(AppStore) private store

    ) {
        store.subscribe(() => this.updateState());
        this.mobWidth = (window.screen.width);
    }

    ngOnInit() {
        this.timer();
        this.sidebarMode = 'full';
    }

    ngOnDestroy() {
        clearTimeout(this.counter);
    }

    public updateState(): void {
        this.profile = this.store.getState().toJS().profile;
        this.pair = this.store.getState().get('currentTicker').toJS().pair;

        this.cryptoSymbol = this.pair.substring(0, this.pair.indexOf(':'));
        this.fiatSymbol = this.pair.substring(this.pair.indexOf(':') + 1);

        this.currentPage = this.store.getState().get('page').toJS().currentPage;
    }

    public toggleCollapseBottom(): void {
        this.collapseBtn = !this.collapseBtn;
    }

    public toggleBookGraf(): void {
        this.bookGraf = !this.bookGraf;
    }

    public timer(): void {
        this.counter = setTimeout(() => {
            this.sidebarMode = localStorage.getItem('sidebarMode');
            this.timer();
        }, 1000);
    }
}
