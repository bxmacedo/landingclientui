export const subscribedMockMessage = {
    'Book': {
        'Bids': [
            {
                'Symbol': 'ETH',
                'Id': 8,
                'Size': 2,
                'Price': 500.00000000000006,
                'TimeStamp': '/Date(1523292979496-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 50,
                'Size': 2,
                'Price': 500.00000000000006,
                'TimeStamp': '/Date(1523292979540-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 5,
                'Size': 10,
                'Price': 333.33000000000004,
                'TimeStamp': '/Date(1523292979493-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 47,
                'Size': 10,
                'Price': 333.33000000000004,
                'TimeStamp': '/Date(1523292979538-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 6,
                'Size': 6,
                'Price': 66.66,
                'TimeStamp': '/Date(1523292979494-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 48,
                'Size': 6,
                'Price': 66.66,
                'TimeStamp': '/Date(1523292979539-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 7,
                'Size': 10,
                'Price': 3.33,
                'TimeStamp': '/Date(1523292979495-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 49,
                'Size': 10,
                'Price': 3.33,
                'TimeStamp': '/Date(1523292979540-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 16,
                'Size': 1,
                'Price': 0.7999999999999999,
                'TimeStamp': '/Date(1523292979507-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 23,
                'Size': 3,
                'Price': 0.7999999999999999,
                'TimeStamp': '/Date(1523292979513-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 30,
                'Size': 1,
                'Price': 0.7,
                'TimeStamp': '/Date(1523292979521-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 3,
                'Size': 4,
                'Price': 0.6599999999999999,
                'TimeStamp': '/Date(1523292979486-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 46,
                'Size': 4,
                'Price': 0.6599999999999999,
                'TimeStamp': '/Date(1523292979535-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 13,
                'Size': 4,
                'Price': 0.6,
                'TimeStamp': '/Date(1523292979505-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 14,
                'Size': 3,
                'Price': 0.6,
                'TimeStamp': '/Date(1523292979506-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 15,
                'Size': 1,
                'Price': 0.5,
                'TimeStamp': '/Date(1523292979506-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 24,
                'Size': 5,
                'Price': 0.5,
                'TimeStamp': '/Date(1523292979514-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 43,
                'Size': 4,
                'Price': 0.5,
                'TimeStamp': '/Date(1523292979532-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 22,
                'Size': 3,
                'Price': 0.39999999999999997,
                'TimeStamp': '/Date(1523292979513-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 25,
                'Size': 3,
                'Price': 0.39999999999999997,
                'TimeStamp': '/Date(1523292979515-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 27,
                'Size': 9,
                'Price': 0.39999999999999997,
                'TimeStamp': '/Date(1523292979518-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 39,
                'Size': 4,
                'Price': 0.39999999999999997,
                'TimeStamp': '/Date(1523292979529-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 20,
                'Size': 1,
                'Price': 0.32999999999999996,
                'TimeStamp': '/Date(1523292979511-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 4,
                'Size': 6,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979492-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 2,
                'Size': 6,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979499-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 12,
                'Size': 3,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979504-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 17,
                'Size': 3,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979508-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 18,
                'Size': 3,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979509-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 19,
                'Size': 3,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979510-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 26,
                'Size': 4,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979517-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 28,
                'Size': 1,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979519-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 33,
                'Size': 1,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979523-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 36,
                'Size': 1,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979526-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 41,
                'Size': 1,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979530-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 42,
                'Size': 1,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979531-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 44,
                'Size': 6,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979533-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 45,
                'Size': 6,
                'Price': 0.3,
                'TimeStamp': '/Date(1523292979534-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 21,
                'Size': 3,
                'Price': 0.19999999999999998,
                'TimeStamp': '/Date(1523292979512-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 9,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979497-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 10,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979498-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 29,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979520-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 31,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979522-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 32,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979523-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 34,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979524-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 35,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979525-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 37,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979527-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 38,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979528-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 40,
                'Size': 1,
                'Price': 0.1,
                'TimeStamp': '/Date(1523292979529-0300)/'
            },
            {
                'Symbol': 'ETH',
                'Id': 11,
                'Size': 1,
                'Price': 0.01,
                'TimeStamp': '/Date(1523292979498-0300)/'
            }
        ],
        'Asks': []
    },
    'Trades': [],
    'Amount': 0,
    'UserId': null,
    'Precision': 8,
    'Broker': null,
    'MessageType': 'Subscribed',
    'CreationTime': 1523293029395,
    'MessageId': '05f8e5ff-cd28-4123-9bf3-7ba544b2e66f',
    'OperationId': '05f8e5ff-cd28-4123-9bf3-7ba544b2e66f',
    'Symbol': 'ETH:BRL',
    'Error': false,
    'ErrorMessage': null
};
