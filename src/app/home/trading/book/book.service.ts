import { Injectable, EventEmitter } from '@angular/core';
import { OrderSide } from '../../../shared/models/order/order.side';


@Injectable()
export class BookService {

    static valueOffer: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    clickItem(side: OrderSide, quantity: number, value: number) {
        const inf = {
            'side': side,
            'quantity': quantity,
            'value': value,
        };

        BookService.valueOffer.emit(inf);
    }
}
