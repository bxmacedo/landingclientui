import { Component, ViewEncapsulation, Inject, Input, OnDestroy } from '@angular/core';
import { AppStore } from '../../../app.store';
import {
    RENDER_INTERVAL,
    BOOK_BAR_MIN_WIDTH,
    BOOK_BAR_MAX_WIDTH,
    BOOK_PRECISION
} from '../../../app.constants';
import { OrderSide } from '../../../shared/models/order/order.side';
import { BookService } from './book.service';

@Component({
    selector: 'app-book',
    templateUrl: './book.component.html',
    styleUrls: ['./book.component.scss'],
    providers: [BookService],
    encapsulation: ViewEncapsulation.None
})

export class BookComponent implements OnDestroy {
    public pair;
    public bids;
    public asks;
    public counter;

    @Input() sidebarMode = 'full';
    @Input() bookConsolidated = false;

    constructor(
        private bookService: BookService,
        @Inject(AppStore) private store,
    ) {

        this.pair = this.store.getState().get('currentTicker').toJS().pair;
        this.updateOrderBook();
        store.subscribe(() => {
            this.pair = this.store.getState().get('currentTicker').toJS().pair;
        });

        this.startOrderBook();
    }

    ngOnDestroy() {
        clearTimeout(this.counter);
    }

    startOrderBook() {
        this.timer(this.updateOrderBook.bind(this), RENDER_INTERVAL);
    }

    timer(callback, milliseconds) {
        this.counter = setTimeout(() => {
            callback();
            this.timer(callback, milliseconds);
        }, milliseconds);
    }

    updateOrderBook() {
        const bids = this.store.getState()
            .getIn(['orderbook', 'bid'])
            .toList()
            .sortBy((order) => order.orderId)
            .sortBy((order) => -order.price)
            .toArray()
            .filter((order) => order.symbol === this.pair)
            .slice(0, 200);

        this.bids = [];
        let lastVolume = 0.0;
        let lastPrice = '#';
        let lastAmount = 0;
        let totalAmount = bids.reduce((acc, current) => acc + parseFloat(current.amount), 0.0);
        const bidMinMaxAmount = bids.reduce((acc, val) => {
            acc[0] = ( acc[0] === undefined || val.amount < acc[0] ) ? val.amount : acc[0]; // MIN
            acc[1] = ( acc[1] === undefined || val.amount > acc[1] ) ? val.amount : acc[1]; // MAX
            return acc;
        }, []);

        this.mountBookSide(bids, this.bids, lastVolume, lastPrice, totalAmount, lastAmount, bidMinMaxAmount);

        const asks = this.store.getState()
            .getIn(['orderbook', 'ask'])
            .toList()
            .sortBy((order) => order.orderId)
            .sortBy((order) => order.price)
            .toArray()
            .filter((order) => order.symbol === this.pair)
            .slice(0, 200);

        this.asks = [];
        lastVolume = 0.0;
        lastPrice = '#';
        lastAmount = 0;
        totalAmount = asks.reduce((acc, current) => acc + parseFloat(current.amount), 0.0);
        const askMinMaxAmount = asks.reduce((acc, val) => {
            acc[0] = ( acc[0] === undefined || val.amount < acc[0] ) ? val.amount : acc[0]; // MIN
            acc[1] = ( acc[1] === undefined || val.amount > acc[1] ) ? val.amount : acc[1]; // MAX
            return acc;
        }, []);

        this.mountBookSide(asks, this.asks, lastVolume, lastPrice, totalAmount, lastAmount, askMinMaxAmount);
    }

    mountBookSide(side, thisSide, lastVolume, lastPrice, totalAmount, lastAmount, minMaxAmount) {

        let maxValue = minMaxAmount[1];
        const minValue = minMaxAmount[0];

        side.forEach(element => {

            let isMyOrder = false;

            lastVolume += parseFloat(element.amount);

            const stringPrice = String(Number(element.price).toFixed(BOOK_PRECISION));

            const priceDiffIdx = this.firstDifference(lastPrice, stringPrice);
            let floatAmount = parseFloat(element.amount);
            let prices;
            let mountConsolidated = false;

            if (this.bookConsolidated === true && lastPrice === stringPrice) {
                mountConsolidated = true;
                floatAmount += lastAmount;
                prices = thisSide.pop();
            }

            if (
                this.store.getState().get('myorders').filter(
                    (order) => parseFloat(order.externalOrderId) === element.orderId
                ).toList().toArray().length !== 0
            ) {
                isMyOrder = true;
            }

            if (floatAmount > maxValue) {
                maxValue = floatAmount;
            }
            lastAmount = floatAmount;

            const amount = floatAmount.toFixed(8);

            lastPrice = stringPrice;

            const amountPeriodIdx = amount.indexOf('.');

            const totalAmountStr = lastVolume.toFixed(8);
            const totalAmountPeriodIdx = totalAmountStr.indexOf('.');

            // tslint:disable-next-line:max-line-length
            const barWidth = BOOK_BAR_MAX_WIDTH - (((BOOK_BAR_MAX_WIDTH - BOOK_BAR_MIN_WIDTH) * (maxValue - floatAmount)) / (maxValue - minValue));
            const normalizedAmount = barWidth < BOOK_BAR_MAX_WIDTH ? barWidth : BOOK_BAR_MAX_WIDTH;

            if (mountConsolidated) {
                thisSide.push({
                    amount: amount,
                    amountOne: amount.substring(0, amountPeriodIdx),
                    amountTwo: amount.substring(amountPeriodIdx),
                    price: stringPrice,
                    priceOne: prices.priceOne,
                    priceTwo: prices.priceTwo,
                    totalAmount: totalAmountStr,
                    totalAmountOne: totalAmountStr.substring(0, totalAmountPeriodIdx),
                    totalAmountTwo: totalAmountStr.substring(totalAmountPeriodIdx),
                    normalizedAmount,
                    lastVolume,
                    isMyOrder
                });
            } else {
                thisSide.push({
                    amount: amount,
                    amountOne: amount.substring(0, amountPeriodIdx),
                    amountTwo: amount.substring(amountPeriodIdx),
                    price: stringPrice,
                    priceOne: stringPrice.substring(0, priceDiffIdx),
                    priceTwo: stringPrice.substring(priceDiffIdx),
                    totalAmount: totalAmountStr,
                    totalAmountOne: totalAmountStr.substring(0, totalAmountPeriodIdx),
                    totalAmountTwo: totalAmountStr.substring(totalAmountPeriodIdx),
                    normalizedAmount,
                    lastVolume,
                    isMyOrder
                });
            }
        });
    }

    firstDifference(orig: string, match: string) {
        const shorter = Math.min(orig.length, match.length);

        for (let i = 0; i < shorter; i++) {
            if (orig[i] !== match[i]) {
                return i;
            }
        }
        return shorter;
    }

    onRowClick(side: OrderSide, type: string, quantity: number, value: number) {
        this.bookService.clickItem(side, quantity, value);

    }

    trackByFnBid(index, bid) {
        return index;
    }

    trackByFnAsk(index, ask) {
        return index;
    }

}
