import { Component, OnInit, Inject, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { AmChart, AmChartsService } from '@amcharts/amcharts3-angular';
import { AppStore } from '../../../app.store';
import { CANDLECHART_RENDER_INTERVAL } from '../../../app.constants';
import { FilteredCandlechart } from '../../../app.actions';

@Component({
    selector: 'app-candlechart',
    templateUrl: './candlechart.component.html',
    styleUrls: ['./candlechart.component.scss']
})
export class CandlechartComponent implements OnInit, OnDestroy {
    public chart: AmChart;
    public chartData = [];
    public backupData = [];
    public originalChartData = [];
    public lastDate;
    public minDate;
    public sortedDescChartData = [];
    public consolidateSelected;
    public timeSelected;

    public currentCryptoSymbol;
    public counter;
    public pair;

    public consolidated;

    public consolidateOptions = ['Padrão', '1.5m', '3m', '5m', '15m', '30m', '1h', '2h', '4h', '6h', '12h', '1d', '3d', '7d'];

    public consolidateOptionsMobile = ['Padrão', '1m', '5m', '15m', '30m', '60m', '1D', '1S'];
    @Input()
    public chartType: string;
    @Input()
    public bookGraf: boolean;
    @Output()
    public bookGrafChange = new EventEmitter<any>();

    constructor(
        private AmCharts: AmChartsService,
        @Inject(AppStore) private store,
    ) { }

    ngOnDestroy() {
        if (this.chart) {
            this.AmCharts.destroyChart(this.chart);
        }
        clearTimeout(this.counter);
    }

    ngOnInit() {
        const state = this.store.getState().toJS();
        this.chartData = state.candlechart;
        this.consolidateSelected = 'Padrão';
        this.timeSelected = 'allPeriod';
        this.consolidated = false;
        this.createChartOptions();
        this.timer();
    }

    public createChartOptions(): void {
        this.chart = this.AmCharts.makeChart('candlechartdiv', {
            'type': 'serial',
            'theme': 'dark',
            'hideCredits': true,
            'dataProvider': this.chartData.reverse(),
            'categoryField': 'date',
            'columnWidth': 0.8,
            'fontFamily': 'Montserrat',
            'decimalSeparator': ',',
            'thousandsSeparator': '.',
            'graphs': [
                {
                    'id': 'g1',
                    'type': 'candlestick',
                    'closeField': 'c',
                    'highField': 'h',
                    'lowField': 'l',
                    'openField': 'o',
                    'fillAlphas': 1,
                    'lineAlpha': 1,
                    'lineThickness': 2,
                    'fillColors': '#4dde90',
                    'lineColor': '#4dde90',
                    'negativeFillColors': '#ff4823',
                    'negativeLineColor': '#ff4823',
                    'balloonText':
                        `   Abertura: <b>[[o]]</b>
                        Baixa: <b>[[l]]</b>
                        Alta: <b>[[h]]</b>
                        Fechamento: <b>[[c]]</b>
                    `
                }
                // , {
                //     'id': 'g2',
                //     'valueAxis': 'v2',
                //     'bullet': 'round',
                //     'bulletBorderAlpha': 1,
                //     'bulletColor': '#FFFFFF',
                //     'bulletSize': 5,
                //     'hideBulletsCount': 50,
                //     'lineThickness': 2,
                //     'lineColor': '#20acd4',
                //     'type': 'smoothedLine',
                //     'title': 'Market Days',
                //     'useLineColorForBulletBorder': true,
                //     'valueField': 'h'
                // }
            ],
            'chartCursor': {
                'valueLineEnabled': true,
                'valueLineBalloonEnabled': true,
                'categoryBalloonDateFormat': 'DD MMM YYYY JJ:NNh',
            },
            'balloon': {
                'adjustBorderColor': true,
                'color': '#000000',
                'cornerRadius': 5,
                'fillColor': '#FFFFFF',
                'fontSize': 12,
            },
            'categoryAxis': {
                'parseDates': true,
                'equalSpacing': false,
                'dashLength': 1,
                'minPeriod': 'mm',
                'gridAlpha': 0,
                'tickLength': 0,
                'axisAlpha': 0,
            },
            'valueAxes': [
                {
                    'position': 'right',
                    'title': '',
                    'axisAlpha': 0,
                    'dashLength': 1,
                    'gridColor': '#415761',
                    'gridAlpha': 0.9,
                    'color': '#95989a'
                },
                // {
                //     'id': 'v2',
                //     'title': 'Market Days',
                //     'gridAlpha': 0,
                //     'position': 'right',
                //     'autoGridCount': false
                // }
            ]
        });
    }

    public timer(): void {
        const state = this.store.getState().toJS();
        if (state.candlechart.length > 0) {
            this.lastDate = state.candlechart[0].date;
        }
        if (!this.consolidated) {
            this.counter = setTimeout(() => {
                this.chartData = state.candlechart;
                if (this.chart) {
                    this.chart.dataProvider = this.chartData.reverse();
                    this.chart.validateData();
                }
                this.timer();
            }, CANDLECHART_RENDER_INTERVAL);
        }
    }

    public agregate(MatSelectChange: any): void {
        this.consolidateSelected = MatSelectChange.value;
        this.consolidated = true;
        switch (MatSelectChange.value) {
            case '7d':
                // 7 days = 604800000 milliseconds
                this.agregateValues(604800000);
                break;
            case '3d':
                // 3 days = 259200000 milliseconds
                this.agregateValues(259200000);
                break;
            case '1d':
                // 1 day = 86400000 milliseconds
                this.agregateValues(86400000);
                break;
            case '12h':
                // 12 hours = 43200000 milliseconds
                this.agregateValues(43200000);
                break;
            case '6h':
                // 6 hours = 21600000 milliseconds
                this.agregateValues(21600000);
                break;
            case '4h':
                // 4 hours = 14400000 milliseconds
                this.agregateValues(14400000);
                break;
            case '2h':
                // 2 hours = 7200000 milliseconds;
                this.agregateValues(7200000);
                break;
            case '1h':
                // 1 hour = 3600000 milliseconds
                this.agregateValues(3600000);
                break;
            case '30m':
                // 30 minutes = 1800000 milliseconds
                this.agregateValues(1800000);
                break;
            case '15m':
                // 15 minutes = 900000 milliseconds
                this.agregateValues(900000);
                break;
            case '5m':
                // 5 minutes = 300000 milliseconds
                this.agregateValues(300000);
                break;
            case '3m':
                // 3 minutes = 180000 milliseconds
                this.agregateValues(180000);
                break;
            case '1.5m':
                // 1.5 minute = 90000 milliseconds
                this.agregateValues(90000);
                break;
            case 'Padrão':
                this.consolidated = false;
                const state = this.store.getState().toJS();
                this.chartData = state.candlechart;
                this.chart.dataProvider = this.chartData.reverse();
                this.chart.validateData();
                this.timer();
                break;
            default:
                break;
        }
    }

    public agregateMobile(event): void {
        this.consolidateSelected = event.value;
        this.consolidated = true;
        switch (event) {
            case '1S':
                // 7 days = 604800000 milliseconds
                this.agregateValues(604800000);
                break;
            case '1D':
                // 1 day = 86400000 milliseconds
                this.agregateValues(86400000);
                break;
            case '60m':
                // 1 hour = 3600000 milliseconds
                this.agregateValues(3600000);
                break;
            case '30m':
                // 30 minutes = 1800000 milliseconds
                this.agregateValues(1800000);
                break;
            case '15m':
                // 15 minutes = 900000 milliseconds
                this.agregateValues(900000);
                break;
            case '5m':
                // 5 minutes = 300000 milliseconds
                this.agregateValues(300000);
                break;
            case '1m':
                // 1 minute = 60000 milliseconds
                this.agregateValues(60000);
                break;
            case 'Padrão':
                this.consolidated = false;
                const state = this.store.getState().toJS();
                this.chartData = state.candlechart;
                this.chart.dataProvider = this.chartData.reverse();
                this.chart.validateData();
                this.timer();
                break;
            default:
                break;
        }
    }

    public agregateValues(accumulator: number): void {
        clearTimeout(this.counter);
        // get current candlechart data
        const candlechartData = this.store.getState().get('candlechart').toList().toArray().reverse();
        // get first full date of first inserted data
        const iArrayFullDate = new Date(candlechartData[0].date);
        // get month day year of the first inserted data
        const iArrayMDY = iArrayFullDate.getFullYear() + '.' + iArrayFullDate.getMonth() + '.' + iArrayFullDate.getDay();
        // set this date as first date with 00h 00m and 00s
        let initialMs = new Date(...iArrayMDY.split('.').map(Number)).getTime();

        // get current time in milliseconds
        const now = new Date().getTime();
        // create a multidimensional array
        const splitedData = [[]];
        // position of array
        let index = 0;
        // initial comparision value
        let initialAccumulator = initialMs;
        // 1514772000000 = 01/01/2018 00:00:00
        let consolidateData = [];
        initialMs += accumulator;
        // do until reaches todays time
        while (initialMs < now) {
            candlechartData.forEach(data => {
                // if date is inside range initial - initial + accumulator
                if (data.date < initialMs && data.date >= initialAccumulator) {
                    splitedData[index].push(data);
                }
            });
            // update range
            initialAccumulator += accumulator;
            initialMs += accumulator;
            if (splitedData[index].length > 0) {
                splitedData.push([]);
                index++;
            }
        }
        splitedData.splice(splitedData.length - 1, 1);
        index = 0;
        consolidateData = [];
        // consolidate separated data
        splitedData.forEach(data => {
            consolidateData.push(data[0]);
            for (let i = 0; i < data.length; i++) {
                if (consolidateData[index].l > data[i].l) {
                    consolidateData[index].l = data[i].l;
                }
                if (consolidateData[index].h < data[i].h) {
                    consolidateData[index].h = data[i].h;
                }
                consolidateData[index].c = data[i].c;
            }
            index++;
        });
        // reset filtered chart
        this.store.dispatch(FilteredCandlechart.resetFilteredCandlechart());
        // dispatch new data to filtered chart
        this.store.dispatch(FilteredCandlechart.createFilteredCandlechart(consolidateData));
        // show filtered chart
        this.chartData = this.store.getState().get('candlechartfiltered').toList().toArray().reverse();
        this.chart.dataProvider = this.chartData.reverse();
        this.chart.validateData();
    }

    public selectTime(MatSelectChange: any): void {
        switch (MatSelectChange.value) {
            case 'last5min':
                this.minDate = this.lastDate - (5 * 60 * 1000); // 5 min
                break;
            case 'last15min':
                this.minDate = this.lastDate - (15 * 60 * 1000); // 15 min
                break;
            case 'last1day':
                this.minDate = this.lastDate - (24 * 60 * 60 * 1000); // 1 day
                break;
            case 'last1month':
                this.minDate = this.lastDate - (30 * 24 * 60 * 60 * 1000); // 1 month
                break;
            case 'allPeriod':
                this.minDate = 0;
                break;
            default:
                break;
        }
        this.buildRange();
    }

    public buildRange(): void {
        const secData = this.store.getState().get('candlechartfiltered').toJS();
        this.chartData = [];
        secData.forEach(data => {
            if (data.date >= this.minDate) {
                this.chartData.push(data);
            }
        });
        if (this.chartData.length === 0) {
            this.chartData = this.backupData;
        }
        this.chart.dataProvider = this.chartData;
        this.chart.validateData();
    }

    public toggleBookGraf(): void {
        this.bookGraf = false;
        this.bookGrafChange.emit({ v: this.bookGraf });
    }
}
