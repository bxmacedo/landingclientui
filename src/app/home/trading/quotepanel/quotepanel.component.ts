import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-quotepanel',
    templateUrl: './quotepanel.component.html',
    styleUrls: ['./quotepanel.component.scss']
})
export class QuotepanelComponent implements OnInit {
    public typeofbox = 0;
    public coinType = 'Bitcoin';
    public openOrders = 3;
    public coinInitials = 'mBTC';
    public coinSellValue = 23849.81;
    public coinBuyValue = 22911.63;
    public balanceAvailable = 189.99;

    @Input() sidebarMode = 'full';

    constructor() { }

    ngOnInit() {
    }

    public changeBox(box: number): void {
        this.typeofbox = box;
    }

}
