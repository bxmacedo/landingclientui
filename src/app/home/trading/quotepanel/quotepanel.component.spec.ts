import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotepanelComponent } from './quotepanel.component';

describe('QuotepanelComponent', () => {
  let component: QuotepanelComponent;
  let fixture: ComponentFixture<QuotepanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotepanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotepanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
