import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { TradeService } from '../../../../api/trade.service';
import { AppStore } from '../../../app.store';
import { MessageType } from '../../../shared/models/message/message.type';
import { AccountActions } from '../../../app.actions';
import { v4 as uuid } from 'uuid';

export interface State {
    name: string;
    code: string;
}

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.scss']
})
export class EditPasswordComponent implements OnInit {
    public bank: string;
    public agency: string;
    public account: string;
    public cpfOwnerName: string;
    public nameCoOwner: string;
    public accountForm: FormGroup;
    public form: FormGroup;
    public bankPattern = '[0-9]{3}';
    public agencyPattern = '[a-zA-Z0-9]{4}';
    public isTypeAccount = false;
    public validCPFFlag = false;

    constructor(
        public dialogRef: MatDialogRef<EditPasswordComponent>,
        @Inject(AppStore) private store,
        @Inject(TradeService) private tradeService: TradeService
    ) { }


    ngOnInit() {
        this.accountForm = new FormGroup({
            'bank': new FormControl(this.bank, Validators.required),
            'agency': new FormControl(this.agency, [Validators.required]),
            'account': new FormControl(this.account, Validators.required),
        });

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        const accId = this.bank + this.agency + this.account;
        const address = this.bank + ' ' + this.agency + ' ' + this.account;
        const symbol = 'BRL';
        const visible = true;
        const transfer = {
            MessageType: MessageType.CreateNewAddress,
            CreationTime: Date.now(),
            MessageId: uuid(),
            Symbol: symbol,
            accId: accId,
            Address: address
        };

        this.store.dispatch(AccountActions.newAccount(accId, address, symbol, visible));

        /**
         * Function back-end*/
        this.tradeService.sendTransferAccounts(transfer);

        this.dialogRef.close();

    }

    public select() {
        this.isTypeAccount = !this.isTypeAccount;

    }
}
