import { Component, OnInit, Inject } from '@angular/core';
import { AppStore } from '../../app.store';
import { AccountActions } from '../../app.actions';
import { SettingActions, ProfileActions } from '../../app.actions';
import { Observable } from 'rxjs/Observable';
import { Auth0Service } from '../../auth/auth0.service';
import { v4 as uuid } from 'uuid';
import { TradeService } from '../../../api/trade.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { RegisterModalComponent } from '../../shared/registeraccount-modal/register-modal.component';
import { MatSnackBar } from '@angular/material';
import { SnackBarMessage } from '../../shared/message/snackbar-message.component';
import { EditPasswordComponent } from '../profile/edit-password/edit-password.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

   // PROFILE
   public profile;
   public userTotalXP = 0;
   private _total = 0;
   public status = 'Pendente';
   public userXP: Array<{ name: string, count: number, color: string }> = [
       { name: 'totalXP', count: (100 - this.userTotalXP), color: '#171f23' },
       { name: 'userXP', count: this.userTotalXP, color: '#fff600' },
   ];

   // SETTINGS
   public settings;
   public language: string;
   public timezone: string;
   public accountId: string;

   // Controls
   public editMode = false;
   public disabled = true;
   public changesSaved = true;

   public accounts: {
       'bank': string;
       'agency': string;
       'account': string;
       'symbol': string;
       'visible': boolean;
   }[];

   public tableData = [
    {date: new Date(1537671600000), document: 'nomedodocumento.jpg', actions: ''},
    {date: new Date(1537671600000), document: 'nomedodocumento.jpg', actions: ''},
    {date: new Date(1537671600000), document: 'nomedodocumento.jpg', actions: ''},
    {date: new Date(1537671600000), document: 'nomedodocumento.jpg', actions: ''},
  ];

  public dataSource = new MatTableDataSource();
  public displayedColumns = ['date', 'document', 'actions'];

   constructor(
       private auth: Auth0Service,
       @Inject(AppStore) private store,
       private tradeService: TradeService,
       private dialog: MatDialog,
       public snackBar: MatSnackBar
   ) {
       if (this.userXP.length > 0) {
           this._total = this.userXP.map(a => a.count).reduce((x, y) => x + y);
       }
       store.subscribe(() => {
           store.subscribe(() => this.updateState());
       });
   }

   updateState() {
       this.accounts = [];
       const accountsRegister = this.store.getState().get('accounts').toArray();

       for (const acc of accountsRegister) {
           const account: {
               'bank': string;
               'agency': string;
               'account': string;
               'visible': boolean;
               'symbol': string;
           } = { bank: '', agency: '', account: '', symbol: '', visible: true };
           if (acc.symbol.localeCompare('BRL') === 0) {
               const infBank = acc.address.split(' ');
               account.bank = infBank[0];
               account.agency = infBank[1];
               account.account = infBank[2];
               account.symbol = 'BRL';
               account.visible = acc.visible;
           } else {
               account.bank = acc.accId;
               account.agency = '';
               account.account = '';
               account.symbol = acc.symbol;
               account.visible = acc.visible;
           }
           this.accounts.push(account);
       }
   }

   ngOnInit() {
       this.profile = this.store.getState().toJS().profile;
       this.settings = this.store.getState().toJS().settings;
       this.language = this.settings.language;
       this.timezone = this.settings.timezone;
       this.accountId = this.settings.accountId;
       this.dataSource.data = this.tableData;
   }

   public changePhoto(): void {
   }

   public save(): void {
       this.store.dispatch(ProfileActions.updateProfile(
           this.profile.nickname,
           this.profile.picture,
           this.profile.name,
           this.profile.password,
           this.profile.cpf,
           this.profile.birthdate,
           this.profile.phone,
           this.profile.experience
       ));
       this.editMode = false;
       this.disabled = true;
       this.changesSaved = true;
   }

   public onEdit(): void {
       this.dialog.open(EditPasswordComponent);
   }

   public canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
       this.store.dispatch(SettingActions.mySetting(
           this.settings.language,
           this.settings.timezone,
           this.settings.accountId,
           this.settings.services,
           this.settings.favoriteCoin,
           this.settings.negotiationCoin
       ));
       if (!this.changesSaved) {
           return confirm('Do you want to leave before saving?');
       } else {
           this.sendSettings(this.settings);
           return true;
       }
   }

   public sendSettings(settings) {

       let coins_preferred = '';
       let coins_trading = '';

       if (settings.favoriteCoin.btc && settings.favoriteCoin.eth) {
           coins_preferred = 'BTC,ETH';
       } else if (settings.favoriteCoin.btc) {
           coins_preferred = 'BTC';
       } else if (settings.favoriteCoin.eth) {
           coins_preferred = 'ETH';
       }

       if (settings.negotiationCoin.btc && settings.negotiationCoin.eth) {
           coins_trading = 'BTC,ETH';
       } else if (settings.negotiationCoin.btc) {
           coins_trading = 'BTC';
       } else if (settings.negotiationCoin.eth) {
           coins_trading = 'ETH';
       }

       const messageId = uuid();
       const settingsMessage: any = {
           MessageType: 'UserAttributesUpdate',
           CreationTime: Date.now(),
           MessageId: messageId,
           OperationId: messageId,
           Attributes: {
               language: settings.language,
               timezone: settings.timezone,
               coins_preferred: coins_preferred,
               coins_trading: coins_trading
           }
       };

       this.tradeService.sendMessage(JSON.stringify(settingsMessage));
   }

   public deleteAccount(): void {
       const id = uuid();
       if (confirm('Deseja mesmo encerrar a sua conta?')) {
           const deleteMsg = {
               MessageId: id,
               OperationId: id,
               MessageType: 'TerminateAccount',
               CreationTime: new Date()
           };
           this.tradeService.sendMessage(JSON.stringify(deleteMsg));
           this.snackBar.openFromComponent(SnackBarMessage, {
               duration: 4500,
               data: 'Sua conta será encerrada. Entraremos em contato para maiores detalhes.',
               panelClass: ['snackbar-messsage']
           });
           this.auth.logout();
       }
   }

   public getPerimeter(radius: number): number {
       return Math.PI * 2 * radius;
   }

   public getColor(index: number): string {
       return this.userXP[index].color;
   }

   public getOffset(radius: number, index: number): number {
       let percent = 0;
       for (let i = 0; i < index; i++) {
           percent += ((this.userXP[i].count) / this._total);
       }
       const perimeter = Math.PI * 2 * radius;
       return perimeter * percent;
   }

   public select(MatSlideToggle, account) {

       let accId, visible, adress, symbol;
       if (account.symbol.localeCompare('BRL') === 0) {
           accId = account.bank + account.agency + account.account;
           visible = MatSlideToggle.checked;
           adress = account.bank + ' ' + account.agency + ' ' + account.account;
           symbol = 'BRL';
       } else {
           accId = account.bank;
           symbol = account.symbol;
           visible = MatSlideToggle.checked;
           adress = account.bank;
       }

       this.store.dispatch(AccountActions.updateAccount(accId, adress, symbol, visible));

   }

   public deleteAccountBank(account): void {
       const id = uuid();
       const address = account.bank + ' ' + account.agency + ' ' + account.account;
       const accId = account.bank + account.agency + account.account;

       if (confirm('Deseja mesmo excluir a conta?')) {
           const deleteMsg = {
               CreationTime: new Date(),
               MessageId: id,
               OperationId: id,
               MessageType: 'RemoveAccount',
               AccId: address,
               Symbol: 'BRL'
           };

           this.store.dispatch(AccountActions.removeAccount(accId));
           this.tradeService.sendTransferAccounts(JSON.stringify(deleteMsg));
       }
   }

   openDialog() {
       this.dialog.open(RegisterModalComponent);
   }
}
