import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationsSideComponent } from './operations-side.component';

describe('OperationsSideComponent', () => {
    let component: OperationsSideComponent;
    let fixture: ComponentFixture<OperationsSideComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [OperationsSideComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(OperationsSideComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
