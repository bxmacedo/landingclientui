import { Component, OnInit, Inject, ElementRef } from '@angular/core';
import { AppStore } from '../app.store';
import { environment } from '../../environments/environment';
import { Auth0Service } from '../auth/auth0.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public clickOutEnabled = false;
    public clickOutEnabledWithDep = false;
    public clickOutEnabledProfile = false;
    public orderDialogVisible = 'hidden';
    public withdepDialogVisible = 'hidden';
    public profileDialogVisible = 'hidden';
    public menuMobile;
    public sidebarMode = 'full';

    public loggedIn = false;

    constructor(
        @Inject(Auth0Service) public auth: Auth0Service,
        private elementRef: ElementRef,
        @Inject(AppStore) private store,

    ) {
        auth.bootstrap();
        store.subscribe(() => this.updateState());
    }

    ngOnInit() {
        localStorage.setItem('sidebarMode', 'full');
        this.elementRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
    }

    public updateState(): void {
        if (environment.production) {
            this.loggedIn = this.store.getState().get('userStatus').toJS().loggedIn;
        } else {
            this.loggedIn = true;
        }
    }

    public toggleSidebar(): void {
        if (this.sidebarMode === 'full') {
            this.sidebarMode = 'collapse';
            localStorage.setItem('sidebarMode', 'collapse');
        } else {
            this.sidebarMode = 'full';
            localStorage.setItem('sidebarMode', 'full');
        }
    }

    public popOrder(orderVisibility): void {
        this.orderDialogVisible = orderVisibility;
        setTimeout(() => {
            this.clickOutEnabled = (orderVisibility === 'visible');
        }, 80);
    }

    public popWithdep(withdepVisibility): void {
        this.withdepDialogVisible = withdepVisibility;
        setTimeout(() => {
            this.clickOutEnabledWithDep = (withdepVisibility === 'visible');
        }, 80);
    }

    public popProfile(profileVisibility): void {
        this.profileDialogVisible = profileVisibility;
        setTimeout(() => {
            this.clickOutEnabledProfile = (profileVisibility === 'visible');
        }, 80);
    }

    public onClickedOutside(e): void {
        if (this.clickOutEnabled) {
            this.orderDialogVisible = 'hidden';
            this.clickOutEnabled = false;
        }
    }

    public onClickedOutsideWithDep(e): void {
        if (this.clickOutEnabledWithDep) {
            this.withdepDialogVisible = 'hidden';
            this.clickOutEnabledWithDep = false;
        }
    }

    public onClickedOutsideProfile(e): void {
        if (this.clickOutEnabledProfile) {
            this.profileDialogVisible = 'hidden';
            this.clickOutEnabledProfile = false;
        }
    }

    public onMenuMobileEvent(evento): void {
        this.menuMobile = evento.v;
    }
}
