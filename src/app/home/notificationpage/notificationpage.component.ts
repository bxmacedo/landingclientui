import { Component, OnInit, Inject } from '@angular/core';
import { AppStore } from '../../app.store';
import { NotificationActions } from '../../app.actions';
import { CanComponentDeactivate } from '../../auth/can-deactivate-guard.service';
import { Observable } from 'rxjs/Observable';
import { TradeService } from '../../../api/trade.service';
import { v4 as uuid } from 'uuid';
import { MessageType } from '../../shared/models/message/message.type';


@Component({
    selector: 'app-notificationpage',
    templateUrl: './notificationpage.component.html',
    styleUrls: ['./notificationpage.component.scss']
})

export class NotificationpageComponent implements OnInit, CanComponentDeactivate {
    public userEmail: string;
    public state;
    public notification;
    public changesSaved = true;

    constructor(
        @Inject(AppStore) private store,
        @Inject(TradeService) private tradeService: TradeService,
    ) { }

    ngOnInit() {
        this.state = this.store.getState().toJS();
        this.userEmail = this.state.profile.email;
        this.notification = this.state.notifications;
    }

    public canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        if (!this.changesSaved) {
            return confirm('Deseja sair antes de salvar?');
        } else {
            this.store.dispatch(NotificationActions.myNotification(
                this.notification.buyCripto,
                this.notification.sellCripto,
                this.notification.contributionSubmission,
                this.notification.contributionAproved,
                this.notification.requestRescue,
                this.notification.rescueAproved,
                this.notification.monthlyHistory,
                this.notification.btcQuotation,
                this.notification.ethQuotation,
                this.notification.newsAboutPlatform,
                this.notification.newFeatures
            ));
            const messageId = uuid();
            const notificationMessage: any = {
                MessageType: MessageType.UserAttributesUpdate,
                CreationTime: Date.now(),
                MessageId: messageId,
                OperationId: messageId,
                Notifications: [
                    {
                        NotificationType: 'Hit',
                        SecurityType: 'Crypto',
                        Side: 'B',
                        MessageType: [
                            'ExecutionTaker',
                            'ExecutionMaker'
                        ],
                        Active: this.notification.buyCripto,
                        EndPointType: 'Email'

                    },
                    {
                        NotificationType: 'Hit',
                        SecurityType: 'Crypto',
                        Side: 'S',
                        MessageType: [
                            'ExecutionTaker',
                            'ExecutionMaker'
                        ],
                        Active: this.notification.sellCripto,
                        EndPointType: 'Email'
                    },
                    {
                        NotificationType: 'Hit',
                        SecurityType: 'Fiat',
                        MessageType: 'DepositConfirmation',
                        Active: this.notification.contributionAproved,
                        EndPointType: 'Email'
                    },
                    {
                        NotificationType: 'Hit',
                        SecurityType: 'Fiat',
                        MessageType: 'ClientWithdrawal',
                        Active: this.notification.requestRescue,
                        EndPointType: 'Email'
                    },
                    {
                        NotificationType: 'Hit',
                        SecurityType: 'Fiat',
                        MessageType: 'WithdrawalConfirmation',
                        Active: this.notification.rescueAproved,
                        EndPointType: 'Email'
                    },
                    {
                        NotificationType: 'ThresholdChange',
                        Security: 'BTC',
                        MessageType: 'Ticker',
                        Field: 'LastPrice',
                        Threshold: this.notification.btcQuotation.decrease.value,
                        Active: this.notification.btcQuotation.decrease.status,
                        EndPointType: 'Email'
                    },
                    {
                        NotificationType: 'ThresholdChange',
                        Securuty: 'BTC',
                        MessageType: 'Ticker',
                        Field: 'LastPrice',
                        Threshold: this.notification.btcQuotation.increase.value,
                        Active: this.notification.btcQuotation.increase.status,
                        EndPointType: 'Email'
                    },
                    {
                        NotificationType: 'ThresholdChange',
                        Security: 'ETH',
                        MessageType: 'Ticker',
                        Field: 'LastPrice',
                        Threshold: this.notification.ethQuotation.decrease.value,
                        Active: this.notification.ethQuotation.decrease.status,
                        EndPointType: 'Email'
                    },
                    {
                        NotificationType: 'ThresholdChange',
                        Securuty: 'ETH',
                        MessageType: 'Ticker',
                        Field: 'LastPrice',
                        Threshold: this.notification.ethQuotation.increase.value,
                        Active: this.notification.ethQuotation.increase.status,
                        EndPointType: 'Email'
                    },

                ]
            };
            this.tradeService.sendMessage(JSON.stringify(notificationMessage));
            return true;
        }
    }
}
