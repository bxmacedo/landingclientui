import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';

import { TradeService } from '../../../api/trade.service';

import * as Redux from 'redux';
import { AppStore } from '../../app.store';
import { OrderActions, WalletActions } from '../../app.actions';

import { MatSnackBar } from '@angular/material';

import { DecimalPipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';

import { v4 as uuid } from 'uuid';
import { Order } from '../../shared/models/order/order';
import { OrderSide } from '../../shared/models/order/order.side';
import { OrderStatus } from '../../shared/models/order/order.status';
import { OrderType } from '../../shared/models/order/order.type';
import { MessageType } from '../../shared/models/message/message.type';
import { environment } from '../../../environments/environment';
import { BookService } from '../trading/book/book.service';
import { SnackBarMessage } from '../../shared/message/snackbar-message.component';
import { MktDataService } from '../../../api/mktdata.service';
import { TickerActions } from '../../app.actions';
import { SelectCryptoService } from '../../home/header/select-crypto/select-crypto.service';
import { SelectCryptoOrderService } from './select-crypto-order.service';



@Component({
    selector: 'app-orderentry',
    templateUrl: './orderentry.component.html',
    styleUrls: ['./orderentry.component.scss'],
    providers: [BookService, SelectCryptoService, SelectCryptoOrderService],
    encapsulation: ViewEncapsulation.None
})
export class OrderentryComponent implements OnInit {
    public cryptoSymbol;
    public fiatSymbol;
    public symbolSelected;
    public order: Order = {
        subtype: OrderType.Limit,
        side: OrderSide.Buy,
        size: 0,
        price: 0,
        orderId: '',
        pair: 'ETH:BRL'
    };
    public availableSymbols: string[];
    public submitted = false;
    public lastPrice: number;
    public total = 0;
    public marketPrice: number;
    public quantityOffer: number;
    public priceOffer: number;

    constructor(
        private selectCryptoOrderService: SelectCryptoOrderService,
        @Inject(TradeService) private tradeService: TradeService,
        @Inject(AppStore) private store,
        @Inject(MktDataService) private mktDataService: MktDataService,
        public snackBar: MatSnackBar,
    ) {
        store.subscribe(() => this.updateState());
    }

    ngOnInit() {
        this.order.price = this.lastPrice;
        this.order.orderId = uuid();
        this.store.dispatch(OrderActions.newOrder(this.order));
        this.total = 0;
        BookService.valueOffer.subscribe(
            inf => {
                this.select(inf.side);
                if (this.order.subtype.localeCompare(OrderType.Limit) === 0) {
                    this.order.size = inf.quantity;
                    this.order.price = inf.value;
                }
                if (this.order.subtype.localeCompare(OrderType.Market) === 0) {
                        this.order.size = inf.quantity;
                }
                this.store.dispatch(OrderActions.newOrder(this.order));
            }
        );

        SelectCryptoService.changeValue.subscribe(
            onChangedSymbol => {
                this.onChangedSelectCrypto(onChangedSymbol);
            }
        );
    }

    public updateState(): void {
        const state = this.store.getState().toJS();

        this.order.pair = this.store.getState().get('currentTicker').toJS().pair; // current Pair

        this.cryptoSymbol = this.order.pair.substring(0, this.order.pair.indexOf(':'));
        this.fiatSymbol = this.order.pair.substring(this.order.pair.indexOf(':') + 1); // USAGE OF GENERIC SYMBOLS
        const currentTicker = state.tickers[this.order.pair];

        this.availableSymbols = Object.keys(state.tickers);
        this.symbolSelected = this.store.getState().get('currentTicker').toJS().pair;

        if (currentTicker !== undefined) {
            this.lastPrice = currentTicker.last;
        }

        this.marketPrice = this.getMarketPrice(this.order.side);

        if (state.orders.subtype === OrderType.Market) {
            if (currentTicker !== undefined) {
                this.order.price = this.marketPrice;
            } else {
                this.order.price = 0;
            }
        }

        this.order.subtype = state.orders.subtype;
        this.order.size = parseFloat(state.orders.size);
        this.order.side = state.orders.side;
        this.order.orderId = state.orders.orderId;

    }

    /**
     * getMarketPrice
     */
    public getMarketPrice(orderSide) {

        let bookSide;
        (orderSide === OrderSide.Buy) ? bookSide = 'ask' : bookSide = 'bid';

        const bidAsk = this.store.getState()
            .getIn(['orderbook', bookSide])
            .sortBy((order) => (bookSide === 'ask' ? order.price : -order.price))
            .toList()
            .toArray()
            .filter((order) => order.symbol === this.order.pair)
            .slice(0);

        if (bidAsk[0]) {
            return bidAsk[0].price;
        } else {
            return 0; // bidAsk[0].price;
        }

    }

    public onSubmit(parameter): void {

        const state = this.store.getState().toJS();
        if (this.order.side === OrderSide.Buy) { // BUY
            if (state.balances[this.fiatSymbol].value < (this.order.price * this.order.size)) {
                this.snackBar.openFromComponent(SnackBarMessage, {
                    duration: 4500,
                    data: 'Saldo insuficiente para compra.',
                    panelClass: ['snackbar-messsage']
                });
                this.submitted = false;
            } else {
                const newValue = state.balances[this.fiatSymbol].value - (this.order.price * this.order.size);
                this.store.dispatch(WalletActions.updateBalance(state.balances[this.fiatSymbol].sequence + 0.1, this.fiatSymbol, newValue));
                this.submitted = true;
            }
        } else { // SELL
            if (state.balances[this.cryptoSymbol].value < state.orders.size) {
                this.snackBar.openFromComponent(SnackBarMessage, {
                    duration: 4500,
                    data: 'Saldo de ' + this.cryptoSymbol + ' insuficiente para venda.',
                    panelClass: ['snackbar-messsage']
                });
                this.submitted = false;
            } else {
                const newValue = state.balances[this.cryptoSymbol].value - this.order.size;
                this.store.dispatch(
                    WalletActions.updateBalance(
                        state.balances[this.cryptoSymbol].sequence + 0.1,
                        this.cryptoSymbol,
                        newValue
                    )
                );
                this.submitted = true;
            }
        }
        if (this.submitted && this.order.size > 0) {
            this.store.dispatch(OrderActions.sentOrder(this.store.getState().toJS().orders));
            this.store.dispatch(OrderActions.myOrder(
                this.order.orderId,
                this.order.pair,
                this.order.side,
                OrderStatus.Created,
                this.order.size,
                this.order.price,
                0,
                Date.now(),
                this.order.orderId
            ));
            this.tradeService.send(this.order, this.order.pair, MessageType.OpenOrder);
            this.resetOrder();
            this.submitted = false;
            this.snackBar.openFromComponent(SnackBarMessage, {
                duration: 4500,
                data: 'Ordem enviada! Aguardando confirmação de abertura.',
                panelClass: ['snackbar-messsage']
            });
        }
    }

    public resetOrder(): void {
        this.order.orderId = uuid();
        this.store.dispatch(OrderActions.newOrder(this.order));
    }

    public select(orderType: OrderSide): void {
        this.order.side = orderType;
        this.order.orderId = uuid();
        this.store.dispatch(OrderActions.newOrder(this.order));
    }

    public newSizeValue(newSize: number): void {
        this.order.size = newSize;
        this.total = this.order.price * this.order.size;
        this.order.orderId = uuid();
        this.store.dispatch(OrderActions.newOrder(this.order));
    }

    public marketLimitValue(subtype: OrderType): void {
        if (subtype === OrderType.Market) {
            this.priceValue(this.lastPrice);
        }
        this.order.subtype = subtype;
        this.order.orderId = uuid();
        this.store.dispatch(OrderActions.newOrder(this.order));
    }

    public priceValue(price: number): void {
        this.order.price = price;
        this.total = this.order.price * this.order.size;
        this.store.dispatch(OrderActions.newOrder(this.order));
    }

    public onChangedSymbol(matRadioChange: any) {
        // SEND UNSUBSCRIBE
        this.mktDataService.subscribe(this.symbolSelected, false);
        // SEND SUBSCRIBE
        this.mktDataService.subscribe(matRadioChange.value, true);
        this.store.dispatch(TickerActions.currentTicker(matRadioChange.value));
        this.selectCryptoOrderService.onChangedOrder(matRadioChange.value);

    }

    public onChangedSelectCrypto(matRadioChange: any) {
        // SEND UNSUBSCRIBE
        this.mktDataService.subscribe(this.symbolSelected, false);
        // SEND SUBSCRIBE
        this.mktDataService.subscribe(matRadioChange.value, true);
        this.store.dispatch(TickerActions.currentTicker(matRadioChange.value));
    }

    public changeTotal(total: number): void {
        if (total !== 0 && this.order.price !== 0) {
            const sizeString = (this.order.price !== 0) ? (Math.round((total / this.order.price) * 100000000) / 100000000).toFixed(8) : '0';
            this.order.size = parseFloat(sizeString);
        }
        if (total === 0 && this.order.price !== 0) {
            this.order.size = 0;
        }
        this.store.dispatch(OrderActions.newOrder(this.order));
    }

}
