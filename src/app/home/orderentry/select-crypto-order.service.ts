import { Injectable, EventEmitter } from '@angular/core';



@Injectable()
export class SelectCryptoOrderService {
    static changeValueOrder: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    onChangedOrder(matSelectChange) {
        SelectCryptoOrderService.changeValueOrder.emit(matSelectChange);
    }
}
