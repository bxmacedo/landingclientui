import {EventEmitter, Component, OnInit, Input, Output} from '@angular/core';
import * as Redux from 'redux';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-buysell',
  templateUrl: './buysell.component.html',
  styleUrls: ['./buysell.component.scss']
})
export class BuysellComponent implements OnInit {

    @Input() value = 'Buy';
    @Input() buySelected = true;
    @Input() isBuyOrder: boolean;

    @Output() selectChanges = new EventEmitter < number > ();

    constructor() {

    }

    ngOnInit() {}

    public select(RadioObject) {
        this.selectChanges.emit(RadioObject.value);
    }

}
