import { Component, OnInit, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { AppStore } from '../../app.store';
import { MatTableDataSource, MatSort } from '@angular/material';
import { HistoryActions } from '../../app.actions';
import * as moment from 'moment';
import { RangeCalendarComponent } from '../../shared/range-calendar/range-calendar.component';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'historypage',
    templateUrl: './historypage.component.html',
    styleUrls: ['./historypage.component.scss']
})
export class HistorypageComponent implements OnInit, AfterViewInit {
    public history = [];
    public tableData = [];
    public dateBegin: Date = null;
    public dateEnd: Date = null;

    public startDate = 0;
    public endDate = 0;
    public displayedColumns = ['data', 'description', 'operationValue', 'credit', 'debit', 'balance', 'balanceBTC', 'balanceETH'];
    public dataSource = new MatTableDataSource();

    public historyKinds = [
        { value: 'all', viewValue: 'Todos' },
        { value: 'deposit', viewValue: 'Depósito' },
        { value: 'withdraw', viewValue: 'Saque' },
        { value: 'buy', viewValue: 'Compra' },
        { value: 'sell', viewValue: 'Venda' }
    ];

    public historyCoins = [
        { value: 'all', viewValue: 'Todas' },
        { value: 'currency', viewValue: 'R$' },
        { value: 'btc', viewValue: 'Bitcoin' },
        { value: 'eth', viewValue: 'Ethereum' }
    ];

    public totalBTC = 0;
    public totalETH = 0;
    public totalCRY = 0;

    // FILTERS
    public now = moment();
    public kind: string;
    public coin: string;
    public period: number;
    public disabled = false;
    public disabledPeriodSeven = false;
    public disabledPeriodFifteen = false;
    public disabledPeriodThirty = false;


    public isFiltered = false;

    @ViewChild(MatSort) sort: MatSort;

    constructor(
        @Inject(AppStore) private store,
    ) {
        this.startUpdateHistory();
    }

    ngOnInit() {
        const state = this.store.getState().toJS();

        // se o historyWaiting estiver vazio, significa que é a primeira vez que chega a informação
        // portanto chegará uma lista de objetos
        if (state.historyWaiting.length === 0) {
            // pega cada dado que chegou e insere no array local
            state.historyNew.forEach(data => {
                this.insertArray(data);
            });
            // insere os objetos filhos no array childs do objeto pai, do array local
            for (let i = 0; i < this.history.length; i++) {
                for (let j = 0; j < this.history.length; j++) {
                    if (this.history[i].id === this.history[j].parentId && i !== j) {
                        this.history[i].childs.push(this.history[j]);
                        this.history.splice(j, 1);
                    }
                }
            }
            // se um filho chega sem pai, ou antes do pai, ou é de outro tipo, insere o objeto do array local no historyWaiting
            for (let i = 0; i < this.history.length; i++) {
                if ((this.history[i].type === 'OpenOrder' && this.history[i].childs.length === 0)
                    || this.history[i].type === 'ExecutionMaker' || this.history[i].type === 'UserSignUp'
                    || this.history[i].type === 'Fee') {
                    this.insertWaitingHistory(this.history[i]);
                    this.history.splice(i, 1);
                    i--;
                }
            }
            // nos objetos do array local soma o valor dos filhos e joga no valor total do pai
            for (let i = 0; i < this.history.length; i++) {
                if (this.history[i].childs.length > 0) {
                    for (let j = 0; j < this.history[i].childs.length; j++) {
                        this.history[i].total = this.history[i].total + this.history[i].childs[j].value;
                    }
                }
            }
            // depois de formatar o bloco inicial de informações, copia o array local para o myHistory
            this.history.forEach(history => {
                this.insertMyHistory(history);
            });
            // se o historyWaiting não estiver vazio significa que virá apenas 1 objeto por vez
        } else {
            // transforma o historyNew em um objeto formatado
            state.historyNew.forEach(data => {
                this.insertArray(data);
            });
            let isParent = false;
            for (let i = 0; i < state.historyWaiting.length; i++) {
                // se o objeto que estiver vindo for pai de algum objeto do historyWaiting
                if (state.historyWaiting[i].parentId === this.history[0].id && state.historyWaiting[i].id !== this.history[0].id) {
                    isParent = true;
                    // insere no child do pai
                    this.history[0].childs.push(state.historyWaiting[i]);
                    // insere o pai no myHistory
                    this.insertMyHistory(this.history[0]);
                    // remove o filho do waiting
                    this.store.dispatch(HistoryActions.removeWaitingHistory(i));
                    state.historyWaiting.splice(i, 1);
                    // volta uma posição
                    i--;
                }
            }
            let isChild = false;
            // se o objeto que estiver vindo for não for pai de algum objeto do historyWaiting, pode ser filho de algum do history
            if (!isParent) {
                for (let i = 0; i < state.history.length; i++) {
                    // se o objeto que estiver vindo for filho de algum objeto do history
                    if (state.history[i].id === this.history[0].parentId && state.history[i].id !== this.history[0].id) {
                        isChild = true;
                        // insere o filho no myHistory pai
                        this.insertMyHistoryChild(this.history[0], i);
                        state.history[i].childs.push(this.history[0]);
                    }
                }
            }
            // se não for pai nem filho, insere no myHistory
            if (!isParent && !isChild) {
                // se for um filho antes do pai
                if ((this.history[0].type === 'OpenOrder' && this.history[0].childs.length === 0)
                    || this.history[0].type === 'ExecutionMaker' || this.history[0].type === 'UserSignUp'
                    || this.history[0].type === 'Fee') {
                    this.insertWaitingHistory(this.history[0]);
                } else {
                    this.insertMyHistory(this.history[0]);
                }
            }
        }
        this.updateHistory();
        this.getPeriod();
        // remove o objeto que veio sozinho
        this.store.dispatch(HistoryActions.newHistoryReset());
        // zera o array local inicial
        this.history = [];
        // fazer um sort da data
        // fazer um sort do balance do child p saber qual é o maior
    }

    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
    }

    public startUpdateHistory(): void {
        this.timer(this.updateHistory.bind(this), 1000);
    }

    public updateHistory(): void {
        if (!this.isFiltered) {
            this.tableData = this.store.getState().get('history').toList().toArray();
            this.dataSource.data = this.tableData;
        }
    }

    public timer(callback, milliseconds): void {
        setTimeout(() => {
            callback();
            this.timer(callback, milliseconds);
        }, milliseconds);
    }

    public insertArray(data: any): void {
        this.history.push(
            {
                id: data.OperationId,
                parentId: data.ParentOperationId,
                type: data.OperationType,
                date: data.TimeStamp,
                value: data.Notional,
                fee: 0,
                symbol: data.Currency,
                balance: data.Balance,
                exchange: data.Symbol,
                childs: [],
                total: 0
            }
        );
    }

    public insertMyHistory(history: any): void {
        this.store.dispatch(HistoryActions.myHistory(
            history.id,
            history.parentId,
            history.type,
            history.date,
            history.value,
            history.fee,
            history.symbol,
            history.balance,
            history.exchange,
            history.childs,
            history.total
        ));
    }

    public insertMyHistoryChild(history: any, position: number): void {
        this.store.dispatch(HistoryActions.myHistoryHasChild(
            position,
            history.id,
            history.parentId,
            history.type,
            history.date,
            history.value,
            history.fee,
            history.symbol,
            history.balance,
            history.exchange,
            history.childs,
            history.total
        ));
    }

    public insertWaitingHistory(history: any): void {
        this.store.dispatch(HistoryActions.waitingHistory(
            history.id,
            history.parentId,
            history.type,
            history.date,
            history.value,
            history.fee,
            history.symbol,
            history.balance,
            history.exchange,
            history.childs,
            history.total
        ));
    }

    public getPeriod(): void {
        if (this.tableData.length > 0) {
            this.startDate = new Date(this.tableData[0].date).getTime();
            this.endDate = new Date(this.tableData[0].date).getTime();
            this.tableData.forEach(history => {
                const currentDate = new Date(history.date).getTime();
                if (currentDate < this.startDate) {
                    this.startDate = currentDate;
                }
                if (currentDate > this.endDate) {
                    this.endDate = currentDate;
                }
            });
        }
    }

    public filterType(index: number): void {
        const filterArray = [];
        this.isFiltered = true;
        this.tableData.forEach(history => {
            if (index === 1 && history.type === 'ClientDeposit') {
                filterArray.push(history);
            } else if (index === 2 && history.type === 'ClientWithdraw') {
                filterArray.push(history);
            } else if (index === 3 && history.type === 'OpenOrder' && (history.symbol !== 'ETH' || history.symbol !== 'BTC')) {
                filterArray.push(history);
            } else if (index === 4 && history.type === 'OpenOrder' && (history.symbol === 'ETH' || history.symbol === 'BTC')) {
                filterArray.push(history);
            }
        });
        this.dataSource.data = filterArray;

        if (index === 0) {
            this.isFiltered = false;
            this.dataSource.data = this.tableData;
            this.disabled = true;
        }
        this.dataSource.sort = this.sort;
        this.disabled = !this.disabled;
    }

    public filterCoin(index: number): void {
        const filterArray = [];
        this.isFiltered = true;
        if (index === 1) {
            this.tableData.forEach(history => {
                if (history.type !== 'OpenOrder') {
                    filterArray.push(history);
                }
            });
        } else if (index === 2) {
            this.tableData.forEach(history => {
                if (history.type === 'OpenOrder') {
                    history.childs.forEach(child => {
                        if (child.symbol === 'mBTC') {
                            filterArray.push(history);
                        }
                    });
                }
            });
        } else if (index === 3) {
            this.tableData.forEach(history => {
                if (history.type === 'OpenOrder') {
                    history.childs.forEach(child => {
                        if (child.symbol === 'ETH') {
                            filterArray.push(history);
                        }
                    });
                }
            });
        }

        this.dataSource.data = filterArray;
        if (index === 0) {
            this.isFiltered = false;
            this.dataSource.data = this.tableData;
            this.disabled = true;
        }
        this.dataSource.sort = this.sort;
        this.disabled = !this.disabled;
    }

    private setTypeFilters (periodSeven: boolean, periodFifteen: boolean, periodThirty: boolean, allPeriod: boolean ) {
        this.disabledPeriodSeven = periodSeven;
        this.disabledPeriodFifteen = periodFifteen;
        this.disabledPeriodThirty = periodThirty;
        this.disabled = allPeriod;
    }

    private defaultDates() {
        this.dateBegin = null;
        this.dateEnd = null;
    }

    private setPeriod(dateBegin: number, dateEnd: number) {
        this.startDate = dateBegin;
        this.endDate = dateEnd;
    }

    public filterTime(time: number): void {
        const filterArray = [];
        this.isFiltered = true;
        const nowDate = new Date ();

        if (time === 7) {
            this.setPeriod((moment(nowDate).subtract('7', 'days')).valueOf(), this.now.valueOf());
            this.tableData.forEach(history => {
                const date = moment(history.date);
                if (date.isSameOrAfter(moment(nowDate).subtract('7', 'days'))) {
                    filterArray.push(history);
                }
            });
            this.setTypeFilters(true, false, false, false);
            this.defaultDates();

        } else if (time === 15) {
            this.setPeriod((moment(nowDate).subtract('15', 'days')).valueOf(), this.now.valueOf());
            this.tableData.forEach(history => {
                const date = moment(history.date);
                if (date.isSameOrAfter(moment(nowDate).subtract('15', 'days'))) {
                    filterArray.push(history);
                }
            });
            this.setTypeFilters(false, true, false, false);
            this.defaultDates();
        } else if (time === 30) {
            this.setPeriod((moment(nowDate).subtract('30', 'days')).valueOf(), this.now.valueOf());
            this.tableData.forEach(history => {
                const date = moment(history.date);
                if (date.isSameOrAfter(moment(nowDate).subtract('30', 'days'))) {
                    filterArray.push(history);
                }
            });
            this.setTypeFilters(false, false, true, false);
            this.defaultDates();
        }
        this.dataSource.data = filterArray;
        if (time === 0) {
            this.isFiltered = false;
            this.dataSource.data = this.tableData;
            this.setTypeFilters(false, false, false, true);
            this.defaultDates();

        }
        this.dataSource.sort = this.sort;
    }

    public filterDates(dates: object): void {
        this.dateBegin = dates['fromDate'];
        this.dateEnd = dates['toDate'];
        this.startDate = this.dateBegin.getTime();
        this.endDate = this.dateEnd.getTime();

        const filterArray = [];
        this.isFiltered = true;

        if (this.dateBegin !== null && this.dateEnd !== null) {
            this.dateBegin.setHours(0, 0, 0);
            this.dateEnd.setHours(23, 59, 59);
            this.tableData.forEach(history => {
                const date = moment(history.date);
                if (date.isBetween(this.dateBegin, this.dateEnd)) {
                    filterArray.push(history);
                }

            });
            this.setTypeFilters(false, false, false, false);
            this.dataSource.data = filterArray;
            this.dataSource.sort = this.sort;
        }
    }

}
