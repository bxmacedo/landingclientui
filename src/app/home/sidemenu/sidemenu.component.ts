import { Component, OnInit, Inject, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ProfileActions, UserActions, } from '../../app.actions';
import { Auth0Service } from '../../auth/auth0.service';
import { AppStore } from '../../app.store';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-sidemenu',
    templateUrl: './sidemenu.component.html',
    styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {
    public userTotalXP = 0;
    public userXP: Array<{ name: string, count: number, color: string }> = [
        { name: 'totalXP', count: (100 - this.userTotalXP), color: '#171f23' },
        { name: 'userXP', count: this.userTotalXP, color: '#fff600' }
    ];
    public defaultUserAvatar = 'https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest?cb=20110302033947';
    private _total = 0;
    public userNickname: string;
    public userAvatar: string;
    public userName: string;

    public loggedIn = false;

    @Input() orderDialogVisibility = 'visible';
    @Input() withdepDialogVisibility = 'visible';
    @Input() profileDialogVisibility = 'visible';

    @Input() sidebarMode: string;
    @Output() changePage = new EventEmitter<string>();
    @Output() orderDialog = new EventEmitter<string>();
    @Output() withdepDialog = new EventEmitter<string>();
    @Output() profileDialog = new EventEmitter<string>();

    @Input() menuMobile: boolean;
    @Output() menuChange = new EventEmitter<any>();

    constructor(
        private auth: Auth0Service,
        @Inject(AppStore) private store
    ) {
        store.subscribe(() => this.updateState());
        if (this.userXP.length > 0) {
            this._total = this.userXP.map(a => a.count).reduce((x, y) => x + y);
        }
    }

    updateState() {
        const profile = this.store.getState().toJS().profile;
        this.userNickname = profile.nickname;
        this.userAvatar = profile.picture;
        this.userName = profile.name;

        if (environment.production) {
            this.loggedIn = this.store.getState().get('userStatus').toJS().loggedIn;
        } else {
            this.loggedIn = true;
        }

    }

    ngOnInit() {
        if (!this.auth.userProfile) {
            this.auth.getProfile();
        }
    }

    newPage(page) {
        this.changePage.emit(page);
    }

    popOrder() {
        this.orderDialogVisibility === 'hidden' ? this.orderDialogVisibility = 'visible' : this.orderDialogVisibility = 'hidden';
        this.orderDialog.emit(this.orderDialogVisibility);
    }

    popWithDep() {
        this.withdepDialogVisibility === 'hidden' ? this.withdepDialogVisibility = 'visible' : this.withdepDialogVisibility = 'hidden';
        this.withdepDialog.emit(this.withdepDialogVisibility);
    }

    public popProfile(): void {
        this.profileDialogVisibility === 'hidden' ? this.profileDialogVisibility = 'visible' : this.profileDialogVisibility = 'hidden';
        this.profileDialog.emit(this.profileDialogVisibility);
    }

    public getPerimeter(radius: number): number {
        return Math.PI * 2 * radius;
    }

    public getColor(index: number): string {
        return this.userXP[index].color;
    }

    public getOffset(radius: number, index: number): number {
        let percent = 0;
        for (let i = 0; i < index; i++) {
            percent += ((this.userXP[i].count) / this._total);
        }
        const perimeter = Math.PI * 2 * radius;
        return perimeter * percent;
    }

    public logout(): void {
        this.auth.logout();
    }
    toggleMenuMobile(): void {
        this.menuMobile = false;
        this.menuChange.emit({v: this.menuMobile});
    }
}
