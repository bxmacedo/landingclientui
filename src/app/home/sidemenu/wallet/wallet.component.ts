import { Inject, Component, OnInit } from '@angular/core';
import { AppStore } from '../../../app.store';

@Component({
    selector: 'app-wallet',
    templateUrl: './wallet.component.html',
    styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {

    public balanceBRL = 0;
    public balanceETH = 0;
    public balanceBTC = 0;

    public currentCryptoSymbol = 'ETH';

    constructor(
        @Inject(AppStore) private store,

    ) {
        store.subscribe(() => this.updateMyBalances());
        this.updateMyBalances();
    }

    updateMyBalances() {
        const stateBalances = this.store.getState().get('balances').sortBy((balance) => balance.symbol).toJS();
        const pair = this.store.getState().get('currentTicker').toJS().pair;

        this.currentCryptoSymbol = pair.substring(0, pair.indexOf(':'));

        this.balanceBRL = stateBalances.BRL.value;
        this.balanceBTC = stateBalances.BTC.value;
        this.balanceETH = stateBalances.ETH.value;

    }

    ngOnInit() {
    }

}
