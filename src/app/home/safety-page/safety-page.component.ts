import { Component, OnInit, Inject } from '@angular/core';
import { RegisterModalWordComponent } from './register-modal/register-modal.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AppStore } from '../../app.store';
import { TradeService } from '../../../api/trade.service';


@Component({
    selector: 'app-safety-page',
    templateUrl: './safety-page.component.html',
    styleUrls: ['./safety-page.component.scss']
})
export class SafetyPageComponent implements OnInit {

    public haveSafeWord = false;

    constructor(
        @Inject(TradeService) private tradeService: TradeService,
        @Inject(AppStore) private store,
        private dialog: MatDialog

    ) {
        store.subscribe(() => this.updateSafeWord());
    }

    ngOnInit() {
    }

    updateSafeWord() {
        if (this.store.getState().get('safety').toJS().safeWord !== '') {
            this.haveSafeWord = true;
        }
    }

    enableSafeWord() {
        this.dialog.open(RegisterModalWordComponent);
    }

}
