import { Component, OnInit, Inject, NO_ERRORS_SCHEMA } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { TradeService } from '../../../../api/trade.service';
import * as Redux from 'redux';
import { AppStore } from '../../../app.store';
import { SafetyActions } from '../../../app.actions';
import { v4 as uuid } from 'uuid';
import { MessageType } from '../../../shared/models/message/message.type';


@Component({
    templateUrl: './register-modal.html',
    styleUrls: ['./register-modal.scss']
})

export class RegisterModalWordComponent implements OnInit {
    public word: string;
    public wordForm: FormGroup;
    public form: FormGroup;

    constructor(
            public dialogRef: MatDialogRef<RegisterModalWordComponent>,
            @Inject(AppStore) private store,
            @Inject(TradeService) private tradeService: TradeService
    ) {
    }

    ngOnInit() {
        this.wordForm = new FormGroup({
            'word': new FormControl(this.word, Validators.required),
        });

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        const word = this.word;

        const messageId = uuid();
        const safeWordMessage = {
            MessageType: MessageType.UserAttributesUpdate,
            CreationTime: Date.now(),
            MessageId: messageId,
            OperationId: messageId,
            Attributes: {
                security_text: word
            }
        };

        this.store.dispatch(SafetyActions.mySafety(word));

        this.tradeService.sendMessage(JSON.stringify(safeWordMessage));

        this.dialogRef.close();

      }

}
