import { Component, OnInit, Inject, NO_ERRORS_SCHEMA } from '@angular/core';
import { MatIconRegistry, MatSnackBar } from '@angular/material';
import { TradeService } from '../../../api/trade.service';
import * as Redux from 'redux';
import { AppStore } from '../../app.store';
import { TransferActions, AccountActions } from '../../app.actions';
import {RegisterModalComponent} from '../../shared/registeraccount-modal/register-modal.component';

import { v4 as uuid } from 'uuid';
import { TransferType } from '../../shared/models/transfer/transfer.type';
import { Transfer } from '../../shared/models/transfer/transfer';
import { TransferStatus } from '../../shared/models/transfer/transfer.status';
import {MatDialog, MatDialogConfig} from '@angular/material';
import { MessageType } from '../../shared/models/message/message.type';
import { SnackBarMessage } from '../../shared/message/snackbar-message.component';



@Component({
    selector: 'app-withdraw-deposit',
    templateUrl: './withdraw-deposit.component.html',
    styleUrls: ['./withdraw-deposit.component.scss']
})
export class WithdrawDepositComponent implements OnInit {

    public cryptoQty = 0;
    public address: string;
    public fiatValue = 0;
    public fiatSymbol: string;
    public cryptoSymbol: string;

    public availableSymbols: string[];
    public isCurrencySelected: boolean;
    public isWithdrawal = false;
    public operation = TransferType.ClientDeposit;

    public submitted = false;
    public copy = false;

    public cryptoAddress = '';

    public accountSelected: {
        'address': string;
        'symbol': string;
    }[];

    public account: any;

    public accountsLength = 0;

    public symbolSelected;

    public adressSelected;

    constructor(
        @Inject(TradeService) private tradeService: TradeService,
        @Inject(AppStore) private store,
        public snackBar: MatSnackBar,
        private dialog: MatDialog
    ) {
        store.subscribe(() => this.updateState());
    }

    ngOnInit() {

    }

    updateState() {
        const pair = this.store.getState().get('currentTicker').toJS().pair;

        this.cryptoSymbol = pair.substring(0, pair.indexOf(':'));
        this.fiatSymbol = pair.substring(pair.indexOf(':') + 1);

        this.availableSymbols = [];

        this.availableSymbols.push(this.cryptoSymbol);
        this.availableSymbols.push(this.fiatSymbol);

        if (this.symbolSelected !== this.cryptoSymbol && this.symbolSelected !== this.fiatSymbol) {
            this.symbolSelected = this.cryptoSymbol;
        }

        this.listAccounts();
        this.updateQRCode(this.cryptoSymbol);
    }

    updateQRCode(symbol) {
        this.cryptoAddress = '';
        const accounts = this.store.getState().get('accounts').toArray();
        accounts.forEach(
            element => {
                if (element.symbol === symbol) {
                    this.cryptoAddress = element.accId;
                    this.adressSelected = element.accId;
                }
            }
        );
    }

    public onChangedSymbol(MatRadioChange: any) {
        this.isCurrencySelected = (MatRadioChange.value === 'BRL');
        this.symbolSelected = MatRadioChange.value;
        this.adressSelected = '';

        if (!this.isCurrencySelected) {
            this.updateQRCode(MatRadioChange.value);
        }

        this.listAccounts();
    }

    selectDepositWithdrawal(e: TransferType) {
        this.operation = e;
        (this.operation === TransferType.ClientWithdrawal) ? this.isWithdrawal = true : this.isWithdrawal = false;
    }

    priceValue(e) {
        this.isCurrencySelected ? this.fiatValue = e : this.cryptoQty = e;
        !this.isCurrencySelected ? this.fiatValue = 0 : this.cryptoQty = 0;
    }

    addressValue(e) {
        this.address = e;
    }

    onSubmit() {
        const state = this.store.getState().toJS();
        if (this.isCurrencySelected) {
            if (this.isWithdrawal && state.balances[this.fiatSymbol].value < this.fiatValue) {
                this.snackBar.openFromComponent(SnackBarMessage, {
                    duration: 4500,
                    data: 'Saldo insuficiente para saque.',
                    panelClass: ['snackbar-messsage']
                });
            } else {
                const transfer: Transfer = {
                    transferId: uuid(),
                    symbol: this.fiatSymbol,
                    amount: this.fiatValue,
                    status: TransferStatus.New,
                    time: Date.now(),
                    type: this.operation
                };
                this.store.dispatch(TransferActions.newFiatTransfer(transfer));
                this.tradeService.sendTransferNotification(transfer);
                this.address = '';
                this.cryptoQty = 0;
                this.fiatValue = 0;
            }
        } else {
            if (this.isWithdrawal && state.balances[this.cryptoSymbol].value < this.cryptoQty) {
                this.snackBar.openFromComponent(SnackBarMessage, {
                    duration: 4500,
                    data: 'Saldo insuficiente para saque.',
                    panelClass: ['snackbar-messsage']
                });
            } else {
                const transfer = {
                    transferId: uuid(),
                    symbol: this.cryptoSymbol,
                    amount: this.cryptoQty,
                    address: this.address,
                    status: TransferStatus.New,
                    time: Date.now(),
                    type: this.operation
                };
                this.store.dispatch(TransferActions.newCryptoTransfer(transfer));
                this.tradeService.sendTransferNotification(transfer);
                this.address = '';
                this.cryptoQty = 0;
                this.fiatValue = 0;
            }
        }
    }

    listAccounts () {
        this.accountSelected = [];
        const accounts = this.store.getState().get('accounts').toArray();
        for (const account of accounts) {
            if (account.symbol === this.symbolSelected && account.visible) {
                if (account.address === undefined) {
                    account.address = account.accId;
                }
                this.accountSelected.push(account);
            }
        }

        this.accountsLength = this.accountSelected.length;
    }
    openDialog() {
        if (this.symbolSelected === 'BRL') {
            this.dialog.open(RegisterModalComponent);
        } else {
            const symbol = this.symbolSelected;
            const accId = '';
            const address = '';
            const transfer = {
                MessageType: MessageType.CreateNewAddress,
                CreationTime: Date.now(),
                MessageId: uuid(),
                Symbol: symbol,
                accId: accId
            };

            // this.store.dispatch(AccountActions.newAccount(accId, address, symbol,true));

            this.tradeService.sendTransferAccounts(transfer);
        }
    }

    isCopied() {
        if (this.copy) {
            this.snackBar.openFromComponent(SnackBarMessage, {
                duration: 450,
                data: 'Texto copiado.',
                panelClass: ['snackbar-messsage']
            });
        }
    }
}
