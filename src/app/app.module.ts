import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import 'hammerjs';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { AppRoutingModule } from './app-routing.module';
import { NgReduxModule } from '@angular-redux/store';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { ClickOutsideModule } from 'ng4-click-outside';

import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { appStoreProviders } from './app.store';

import { AppComponent } from './app.component';
import { OrderentryComponent } from './home/orderentry/orderentry.component';
import { BookComponent } from './home/trading/book/book.component';

import { MktDataService } from '../api/mktdata.service';
import { TradeService } from '../api/trade.service';

import { HomeComponent } from './home/home.component';
import { TickerComponent } from './home/header/ticker/ticker.component';
import { SelectCryptoComponent } from './home/header/select-crypto/select-crypto.component';
import { CryptoTypeComponent } from './home/header/crypto-type/crypto-type.component';
import { SidemenuComponent } from './home/sidemenu/sidemenu.component';
import { FooterComponent } from './home/footer/footer.component';
import { TradingComponent } from './home/trading/trading.component';
import { LandingpageComponent } from './landingpage/landingpage.component';

import { Auth0Service } from './auth/auth0.service';
import { PasswordStrengthBarComponent } from './landingpage/old-sign-up/password-strength-bar/password-strength-bar.component';
import { OldSignUpComponent } from './landingpage/old-sign-up/old-sign-up.component';
import { PostSignUpComponent } from './landingpage/post-sign-up/post-sign-up.component';
import { AuthGuard } from './auth/auth-guard.service';
import { CanDeactivateGuard } from './auth/can-deactivate-guard.service';
import { BuysellComponent } from './home/orderentry/buysell/buysell.component';
import { MyordersComponent } from './home/trading/history/myorders/myorders.component';
import { FilterComponent } from './home/trading/history/filter/filter.component';

import { WithdrawDepositComponent } from './home/withdraw-deposit/withdraw-deposit.component';
import { RegisterModalComponent } from './shared/registeraccount-modal/register-modal.component';

import { MatIconModule, MatSelectModule, MatCheckboxModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { HistoryComponent } from './home/trading/history/history.component';
import { TransferComponent } from './home/trading/history/transfer/transfer.component';
import { HeaderComponent } from './home/header/header.component';
import { WalletComponent } from './home/sidemenu/wallet/wallet.component';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { HistorypageComponent } from './home/historypage/historypage.component';
import { NotificationpageComponent } from './home/notificationpage/notificationpage.component';
import { CandlechartComponent } from './home/trading/candlechart/candlechart.component';
import { SettingspageComponent } from './home/settingspage/settingspage.component';
import { EditUserModalComponent } from './shared/edituser-modal/edit-user.component';
import { QuotepanelComponent } from './home/trading/quotepanel/quotepanel.component';
import { ButtonToggleComponent } from './shared/button-toggle/button-toggle.component';
import { InputBaseComponent } from './shared/input-base/input-base.component';
import { SnackBarMessage } from './shared/message/snackbar-message.component';
import { UploadFileComponent } from './shared/upload-file/upload-file.component';
import { InputCurrencyComponent } from './shared/input-currency/input-currency.component';
import { InputQuantityComponent } from './shared/input-quantity/input-quantity.component';
import { OperationsSideComponent } from './home/operations-side/operations-side.component';
import { ErrorModalComponent } from './shared/error-modal/error-modal.component';
import { UserBalanceComponent } from './home/user-balance/user-balance.component';
import { SafetyPageComponent } from './home/safety-page/safety-page.component';
import { DepthchartComponent } from './home/trading/depthchart/depthchart.component';
import { CdkDetailRowDirective } from './home/historypage/cdk-detail-row.directive';
import { QRCodeModule } from 'angularx-qrcode';
import { ReplaceTwoDots } from './shared/pipes/replace-two-dots';
import { RBookComponent } from './landingpage/r-book/r-book.component';
import { LBookComponent } from './landingpage/l-book/l-book.component';
import { RegisterModalWordComponent } from './home/safety-page/register-modal/register-modal.component';
import { LFooterComponent } from './shared/l-footer/l-footer.component';
import { RangeCalendarComponent } from './shared/range-calendar/range-calendar.component';
import { HowItWorksComponent } from './landingpage/how-it-works/how-it-works.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignUpStep1Component } from './sign-up/sign-up-step-1/sign-up-step-1.component';
import { SignUpStep2Component } from './sign-up/sign-up-step-2/sign-up-step-2.component';
import { SignUpStep3Component } from './sign-up/sign-up-step-3/sign-up-step-3.component';
import { SignUpStep4Component } from './sign-up/sign-up-step-4/sign-up-step-4.component';
import { PreSignUpComponent } from './pre-sign-up/pre-sign-up.component';
import { PreSignUpSuccessComponent } from './pre-sign-up/pre-sign-up-success/pre-sign-up-success.component';
import { PreSignUpErrorComponent } from './pre-sign-up/pre-sign-up-error/pre-sign-up-error.component';
import { PreSignUpVoucherComponent } from './pre-sign-up/pre-sign-up-voucher/pre-sign-up-voucher.component';
import { SignUpStep5Component } from './sign-up/sign-up-step-5/sign-up-step-5.component';
import { PreSignUpLoginComponent } from './pre-sign-up/pre-sign-up-login/pre-sign-up-login.component';
import { PreSignUpAlreadyExistsComponent } from './pre-sign-up/pre-sign-up-already-exists/pre-sign-up-already-exists.component';
import { PreSignUpFormComponent } from './pre-sign-up/pre-sign-up-form/pre-sign-up-form.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { MatAutocompleteModule, MatFormFieldModule } from '@angular/material';
import { LoginComponent } from './login/login.component';
import { SigninComponent } from './login/signin/signin.component';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { AboutUsComponent } from './landingpage/about-us/about-us.component';
import { JobsComponent } from './jobs/jobs.component';
import { VacancyComponent } from './jobs/vacancy/vacancy.component';
import { SignUpStep6Component } from './sign-up/sign-up-step-6/sign-up-step-6.component';
import { SignUpStep7Component } from './sign-up/sign-up-step-7/sign-up-step-7.component';
import { SignUpStep8Component } from './sign-up/sign-up-step-8/sign-up-step-8.component';
import { SignUpStep9Component } from './sign-up/sign-up-step-9/sign-up-step-9.component';
import { ProfileComponent } from './home/profile/profile.component';
import { ClipboardModule } from 'ngx-clipboard';
import { TermsModalComponent } from './shared/terms-modal/terms-modal.component';
import { PrivacyModalComponent } from './shared/privacy-modal/privacy-modal.component';
import { EditPasswordComponent } from './home/profile/edit-password/edit-password.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DropzoneModule, DropzoneConfigInterface, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';



registerLocaleData(localePt);
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.lang.json');
}
const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
    url: 'https://httpbin.org/post',
    acceptedFiles: 'image/*',
    createImageThumbnails: true
};

@NgModule({
    declarations: [
        AppComponent,
        OldSignUpComponent,
        PostSignUpComponent,
        PasswordStrengthBarComponent,
        OrderentryComponent,
        BookComponent,
        TickerComponent,
        SelectCryptoComponent,
        CryptoTypeComponent,
        SidemenuComponent,
        FooterComponent,
        TradingComponent,
        LandingpageComponent,
        BuysellComponent,
        ButtonToggleComponent,
        MyordersComponent,
        FilterComponent,
        WithdrawDepositComponent,
        RegisterModalComponent,
        RegisterModalWordComponent,
        HistoryComponent,
        TransferComponent,
        HeaderComponent,
        WalletComponent,
        HistorypageComponent,
        InputBaseComponent,
        SnackBarMessage,
        InputCurrencyComponent,
        InputQuantityComponent,
        NotificationpageComponent,
        SettingspageComponent,
        EditUserModalComponent,
        QuotepanelComponent,
        HomeComponent,
        OperationsSideComponent,
        UserBalanceComponent,
        CdkDetailRowDirective,
        SafetyPageComponent,
        DepthchartComponent,
        CandlechartComponent,
        ReplaceTwoDots,
        LBookComponent,
        RBookComponent,
        LFooterComponent,
        HowItWorksComponent,
        NavbarComponent,
        SignUpComponent,
        SignUpStep1Component,
        SignUpStep2Component,
        SignUpStep3Component,
        SignUpStep4Component,
        PreSignUpComponent,
        PreSignUpSuccessComponent,
        PreSignUpErrorComponent,
        PreSignUpVoucherComponent,
        SignUpStep5Component,
        PreSignUpLoginComponent,
        PreSignUpAlreadyExistsComponent,
        PreSignUpFormComponent,
        LoginComponent,
        SigninComponent,
        ForgotPasswordComponent,
        AboutUsComponent,
        JobsComponent,
        VacancyComponent,
        TermsModalComponent,
        SignUpStep6Component,
        SignUpStep7Component,
        SignUpStep8Component,
        SignUpStep9Component,
        ProfileComponent,
        PrivacyModalComponent,
        RangeCalendarComponent,
        UploadFileComponent,
        ErrorModalComponent,
        EditPasswordComponent,
    ],
    imports: [
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        HttpClientModule,
        NgReduxModule,
        CurrencyMaskModule,
        ClickOutsideModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        MatExpansionModule,
        MatRadioModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatSnackBarModule,
        MatDialogModule,
        MatTabsModule,
        MatPaginatorModule,
        MatTableModule,
        MatSortModule,
        MatListModule,
        MatIconModule,
        MatSelectModule,
        MatCheckboxModule,
        BrowserAnimationsModule,
        MatSlideToggleModule,
        NgIdleKeepaliveModule.forRoot(),
        AppRoutingModule,
        ReactiveFormsModule,
        AmChartsModule,
        QRCodeModule,
        ScrollToModule.forRoot(),
        MatAutocompleteModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ClipboardModule,
        BsDatepickerModule.forRoot(),
        DatepickerModule.forRoot(),
        DropzoneModule,
    ],
    providers: [
        MktDataService,
        Auth0Service,
        appStoreProviders,
        TradeService,
        { provide: LOCALE_ID, useValue: 'pt-BR' },
        CanDeactivateGuard,
        AuthGuard,
        {
            provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {
                autoFocus: true,
                panelClass: 'custom-dialog',
                hasBackdrop: true,
                backdropClass: 'dark-backdrop'
            }
        },
        {
            provide: DROPZONE_CONFIG,
            useValue: DEFAULT_DROPZONE_CONFIG
        }

    ],
    bootstrap: [AppComponent],
    schemas: [
        NO_ERRORS_SCHEMA,
    ],
    entryComponents: [
        RegisterModalComponent,
        RegisterModalWordComponent,
        EditUserModalComponent,
        SnackBarMessage,
        TermsModalComponent,
        PrivacyModalComponent,
        ErrorModalComponent,
        EditPasswordComponent,
    ]
})
export class AppModule { }
