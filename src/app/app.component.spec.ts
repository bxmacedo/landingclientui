import { TestBed, async } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppRoutingModuleStub } from './test-lib/app-routing.stub';
import { TranslateServiceStub } from './test-lib/ngx-translate.stub';
import { MktDataService } from './mktdata.service';
import { BookComponent } from './book/book.component';
import { Auth0Service } from './auth0.service';
import { AppStore } from './app.store';
import * as Redux from 'redux';
import { TradeService } from './trade.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        BookComponent
      ],
      imports: [
        AppRoutingModuleStub,
        TranslateServiceStub
      ],
      providers: [
        Auth0Service,
        AppStore,
        TradeService,
        MktDataService,
        { provide: TranslateService, useClass: TranslateServiceStub }
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  }));
});
