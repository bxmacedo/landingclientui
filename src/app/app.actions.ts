import { Transfer } from './shared/models/transfer/transfer';

export interface Action {
    type: string;
    payload?: any;
}

export const SAVE_PROFILE = 'saveProfile';
export const UPDATE_PROFILE = 'updateProfile';
export const UPDATE_FULL_PROFILE = 'updateFullProfile';

export const SAVE_AUTH = 'saveAuth';
export const CREATE_SIGNUP = 'createSignUp';

export const NEW_ORDER = 'newOrder';
export const SENT_ORDER = 'sentOrder';
export const CONFIRMED_ORDER = 'confirmedOrder';

export const MY_ORDER = 'myOrder';
export const UPDATE_ORDER_STATUS = 'updateOrderStatus';
export const UPDATE_ORDER_EXECUTION = 'updateOrderExecution';

export const UPDATE_BALANCE = 'updateBalance';

export const NEW_PAIR = 'newPair';
export const ALLOW_PAIR = 'allowPair';
export const UPDATE_PAIR_INFO = 'updatePairInfo';
export const UPDATE_PAIR_LASTPX = 'updatePairLastPx';
export const RCVD_NEW_ORDER = 'rcvdNewOrder';
export const RCVD_CANCEL_ORDER = 'rcvdCancelOrder';
export const EXECUTION_REPORT = 'executionReport';

export const SELECT_PAIR = 'currentTicker';

export const MY_TRANSFER = 'myTransfer';
export const NEW_CRYPTO_TRANSFER = 'newCryptoTransfer';
export const NEW_FIAT_TRANSFER = 'newFiatTransfer';
export const UPDATE_TRANSFER = 'updateTransfer';

export const NEW_HISTORY = 'newHistory';
export const NEW_HISTORY_RESET = 'newHistoryReset';
export const MY_HISTORY = 'myHistory';
export const MY_HISTORY_HAS_CHILD = 'myHistoryHasChild';
export const WAITING_HISTORY = 'waitingHistory';
export const REMOVE_WAITING_HISTORY = 'removeWaitingHistory';

export const MY_NOTIFICATION = 'myNotification';
export const MY_SETTING = 'mySetting';
export const UPDATE_PAGE = 'updatePage';

export const MY_SAFETY = 'mySafety';

export const LOAD_CANDLECHART = 'loadCandlechart';
export const UPDATE_CANDLECHART = 'updateCandlechart';
export const UPDATE_FIRST_ITEM_CANDLECHART = 'updateFirstItemCandlechart';

export const CREATE_FILTERED_CANDLECHART = 'createFilteredCandlechart';
export const RESET_FILTERED_CANDLECHART = 'resetFilteredCandlechart';

export const RESET_DEPTHCHART = 'resetDepthchart';
export const LOAD_DEPTHCHART = 'loadDepthchart';

export const NEW_ACCOUNT = 'newAccount';
export const UPDATE_ACCOUNT = 'updateAccount';
export const REMOVE_ACCOUNT = 'removeAccount';


export const LOG_IN = 'logIn';
export const LOG_OUT = 'logOut';

export const NEW_MESSAGE = 'newMessage';

export class ProfileActions {
    static saveProfile(profile: any): Action {
        return {
            type: SAVE_PROFILE,
            payload: profile
        };
    }

    static updateProfile(
        nickname: string,
        picture: string,
        name: string,
        password: string,
        cpf: string,
        birthdate: string,
        phone: string,
        experience: number
    ): Action {
        return {
            type: UPDATE_PROFILE,
            payload: {
                nickname,
                picture,
                name,
                password,
                cpf,
                birthdate,
                phone,
                experience
            }
        };
    }

    static saveAuth(accessToken: string, idToken: string, expiresAt: string): Action {
        return {
            type: SAVE_AUTH,
            payload: {
                accessToken: accessToken,
                idToken: idToken,
                expiresAt: expiresAt
            }
        };
    }
    static createSignUp(
        name: string,
        cpf: string,
        birthday: string,
        email: string,
        phone: string,
        cel: string,
        city: string,
        state: string,
        address: string,
        number: number,
        complement: string,
        zipCode: string,
        neightborhood: string,
        mother: string,
        password: string
    ): Action {
        return {
            type: CREATE_SIGNUP,
            payload: {
                name,
                cpf,
                birthday,
                email,
                phone,
                cel,
                city,
                state,
                address,
                number,
                complement,
                zipCode,
                neightborhood,
                mother,
                password
            }
        };
    }
}

export class UserActions {
    static logIn(): Action {
        return {
            type: LOG_IN
        };
    }

    static logOut(): Action {
        return {
            type: LOG_OUT
        };
    }
}

export class OrderActions {
    static newOrder(order: any): Action {
        return {
            type: NEW_ORDER,
            payload: order
        };
    }

    static sentOrder(order: any): Action {
        return {
            type: SENT_ORDER,
            payload: order
        };
    }

    static confirmedOrder(order: any): Action {
        return {
            type: CONFIRMED_ORDER,
            payload: order
        };
    }

    static myOrder(orderId, pair, side, status, size, price, filled, time, externalOrderId): Action {
        return {
            type: MY_ORDER,
            payload: {
                orderId,
                pair,
                side,
                status,
                size,
                price,
                filled,
                time,
                externalOrderId
            }
        };
    }

    static updateOrderStatus(orderId, status, time): Action {
        return {
            type: UPDATE_ORDER_STATUS,
            payload: {
                orderId: orderId,
                status: status,
                time: time
            }
        };
    }

    static updateOrderExecution(orderId, pair, side, status, size, price, filled, time, externalOrderId): Action {
        return {
            type: UPDATE_ORDER_EXECUTION,
            payload: {
                orderId: orderId,
                pair: pair,
                side: side,
                status: status,
                size: size,
                price: price,
                filled: filled,
                time: time,
                externalOrderId: externalOrderId
            }
        };
    }
}

export class TickerActions {
    static currentTicker(pair: string): Action {
        return {
            type: SELECT_PAIR,
            payload: {
                pair
            }
        };
    }

    static newPair(pair: string): Action {
        return {
            type: NEW_PAIR,
            payload: {
                pair
            }
        };
    }

    static allowPair(pair: string): Action {
        return {
            type: ALLOW_PAIR,
            payload: {
                pair
            }
        };
    }
}

export class MktDataActions {
    static updatePairInfo(pair: string, last: number, volume: number, variation: number): Action {
        return {
            type: UPDATE_PAIR_INFO,
            payload: {
                pair,
                last,
                variation,
                volume
            }
        };
    }

    static updatePairLastPx(pair: string, last: number): Action {
        return {
            type: UPDATE_PAIR_LASTPX,
            payload: {
                pair,
                last
            }
        };
    }

    static rcvdNewOrder(orderId, symbol, side, amount, price) {
        return {
            type: RCVD_NEW_ORDER,
            payload: {
                orderId: orderId,
                symbol: symbol,
                side: side,
                amount: amount,
                price: price
            }
        };
    }

    static rcvdCancelOrder(orderId, symbol, side, amount, price) {
        return {
            type: RCVD_CANCEL_ORDER,
            payload: {
                orderId: orderId,
                symbol: symbol,
                side: side,
                amount: amount,
                price: price
            }
        };
    }

    static executionReport(size, symbol, price, time) {
        return {
            type: EXECUTION_REPORT,
            payload: {
                size: size,
                symbol: symbol,
                price: price,
                time: time
            }
        };
    }
}

export class TransferActions {
    static newCryptoTransfer(transfer: Transfer): Action {
        return {
            type: NEW_CRYPTO_TRANSFER,
            payload: transfer
        };
    }

    static newFiatTransfer(transfer: Transfer): Action {
        return {
            type: NEW_FIAT_TRANSFER,
            payload: transfer
        };
    }

    static myTransfer(transferId, symbol, address, amount, status, time): Action {
        return {
            type: MY_TRANSFER,
            payload: {
                transferId,
                symbol,
                address,
                amount,
                status,
                time
            }
        };
    }

    static updateTransfer(transferId, time, status, errorMessage): Action {
        return {
            type: UPDATE_TRANSFER,
            payload: {
                transferId,
                time,
                status,
                errorMessage
            }
        };
    }
}

export class AccountActions {
    static newAccount(accId: string, address: string, symbol: string, visible: boolean): Action {
        return {
            type: NEW_ACCOUNT,
            payload: {
                accId,
                address,
                symbol,
                visible
            }
        };
    }
    static updateAccount(accId: string, address: string, symbol: string, visible: boolean): Action {
        return {
            type: UPDATE_ACCOUNT,
            payload: {
                accId,
                address,
                symbol,
                visible
            }
        };
    }

    static removeAccount(accId: number): Action {
        return {
            type: REMOVE_ACCOUNT,
            payload: {
                accId
            }
        };
    }
}


export class EditProfileActions {
    public selectedState: String = 'UF';
    public name: String;
    public email: String;
    public cpf: String;
    public birthdate: String;
    public cellPhone: String;
    public mothersName: String;
    public street: String;
    public number: Number;
    public complement: String;
    public city: String;
    public neigh: String;
    public zipCode: String;

    static updateFullProfile(selectedState: String, name: String, email: String, cpf: String, birthdate: String,
        cellPhone: String, mothersName: String, street: String, number: Number, complement: String, city: String,
        neigh: String, zipCode: String): Action {
        return {
            type: UPDATE_FULL_PROFILE,
            payload: {
                selectedState,
                name,
                email,
                cpf,
                cellPhone,
                birthdate,
                mothersName,
                street,
                number,
                complement,
                city,
                neigh,
                zipCode
            }
        };
    }
}

export class WalletActions {
    static updateBalance(sequence: number, symbol: string, value: number): Action {
        return {
            type: UPDATE_BALANCE,
            payload: {
                sequence,
                symbol,
                value
            }
        };
    }
}

export class HistoryActions {
    static newHistory(
        OperationId: string,
        UserId: string,
        Symbol: string,
        Currency: string,
        Notional: number,
        Balance: number,
        SeqId: number,
        ParentOperationId: string,
        TimeStamp: string,
        OperationType: string
    ): Action {
        return {
            type: NEW_HISTORY,
            payload: {
                OperationId,
                UserId,
                Symbol,
                Currency,
                Notional,
                Balance,
                SeqId,
                ParentOperationId,
                TimeStamp,
                OperationType
            }
        };
    }
    static newHistoryReset(): Action {
        return {
            type: NEW_HISTORY_RESET,
            payload: {}
        };
    }
    static myHistory(
        id: string,
        parentId: string,
        type: string,
        date: Date,
        value: number,
        fee: number,
        symbol: string,
        balance: number,
        exchange: string,
        childs: any,
        total: number
    ): Action {
        return {
            type: MY_HISTORY,
            payload: {
                id,
                parentId,
                type,
                date,
                value,
                fee,
                symbol,
                balance,
                exchange,
                childs,
                total
            }
        };
    }
    static myHistoryHasChild(
        position: number,
        id: string,
        parentId: string,
        type: string,
        date: Date,
        value: number,
        fee: number,
        symbol: string,
        balance: number,
        exchange: string,
        childs: any,
        total: number
    ): Action {
        return {
            type: MY_HISTORY,
            payload: {
                position,
                id,
                parentId,
                type,
                date,
                value,
                fee,
                symbol,
                balance,
                exchange,
                childs,
                total
            }
        };
    }
    static waitingHistory(
        id: string,
        parentId: string,
        type: string,
        date: Date,
        value: number,
        fee: number,
        symbol: string,
        balance: number,
        exchange: string,
        childs: any,
        total: number
    ): Action {
        return {
            type: WAITING_HISTORY,
            payload: {
                id,
                parentId,
                type,
                date,
                value,
                fee,
                symbol,
                balance,
                exchange,
                childs,
                total
            }
        };
    }
    static removeWaitingHistory(position: number): Action {
        return {
            type: REMOVE_WAITING_HISTORY,
            payload: {
                position
            }
        };
    }
}

export class NotificationActions {
    static myNotification(
        buyCripto: boolean,
        sellCripto: boolean,
        contributionSubmission: boolean,
        contributionAproved: boolean,
        requestRescue: boolean,
        rescueAproved: boolean,
        monthlyHistory: boolean,
        btcQuotation: {
            decrease: {
                status: boolean,
                value: number
            },
            increase: {
                status: boolean,
                value: number
            },
        },
        ethQuotation: {
            decrease: {
                status: boolean,
                value: number
            },
            increase: {
                status: boolean,
                value: number
            },
        },
        newsAboutPlatform: boolean,
        newFeatures: boolean
    ): Action {
        return {
            type: MY_NOTIFICATION,
            payload: {
                buyCripto,
                sellCripto,
                contributionSubmission,
                contributionAproved,
                requestRescue,
                rescueAproved,
                monthlyHistory,
                btcQuotation,
                ethQuotation,
                newsAboutPlatform,
                newFeatures
            }
        };
    }
}
export class PageActions {
    static updatePage(
        currentPage: string, mode: string
    ): Action {
        return {
            type: UPDATE_PAGE,
            payload: {
                currentPage,
                mode
            }
        };
    }
}

export class SettingActions {
    static mySetting(
        language: string,
        timezone: Date,
        accountId: string,
        services: {
            gorila: boolean
        },
        favoriteCoin: {
            btc: boolean,
            eth: boolean
        },
        negotiationCoin: {
            btc: boolean,
            eth: boolean
        }
    ): Action {
        return {
            type: MY_SETTING,
            payload: {
                language,
                timezone,
                accountId,
                services,
                favoriteCoin,
                negotiationCoin
            }
        };
    }
}

export class SafetyActions {
    static mySafety(
        safeWord: string,
    ): Action {
        return {
            type: MY_SAFETY,
            payload: {
                safeWord,
            }
        };
    }
}

export class CandleChartActions {
    static loadCandlechart(
        date: Date,
        l: number,
        h: number,
        o: number,
        c: number,
        pair: string
    ): Action {
        return {
            type: LOAD_CANDLECHART,
            payload: [
                {
                    date,
                    l,
                    h,
                    o,
                    c,
                    pair
                }
            ]
        };
    }
    static updateCandlechart(
        date: Date,
        l: number,
        h: number,
        o: number,
        c: number,
        pair: string
    ): Action {
        return {
            type: UPDATE_CANDLECHART,
            payload: {
                date,
                l,
                h,
                o,
                c,
                pair
            }
        };
    }
    static updateFirstItemCandlechart(
        date: Date,
        l: number,
        h: number,
        o: number,
        c: number,
        pair: string
    ): Action {
        return {
            type: UPDATE_FIRST_ITEM_CANDLECHART,
            payload: {
                date,
                l,
                h,
                o,
                c,
                pair
            }
        };
    }
}


export class FilteredCandlechart {
    static createFilteredCandlechart(filteredCandlecChartData: any): Action {
        return {
            type: CREATE_FILTERED_CANDLECHART,
            payload: filteredCandlecChartData
        };
    }
    static resetFilteredCandlechart(): Action {
        return {
            type: RESET_FILTERED_CANDLECHART,
            payload: []
        };
    }
}

export class DepthChartActions {
    static resetDepthChart(): Action {
        return {
            type: RESET_DEPTHCHART,
            payload: []
        };
    }
    static loadDepthchart(depthChartData: any): Action {
        return {
            type: LOAD_DEPTHCHART,
            payload: depthChartData
        };
    }
}

export class MessageActions {
    static newMessage(payload): Action {
        return {
            type: NEW_MESSAGE,
            payload
        };
    }
}
