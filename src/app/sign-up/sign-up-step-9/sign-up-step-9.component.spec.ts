import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpStep9Component } from './sign-up-step-9.component';

describe('SignUpStep9Component', () => {
    let component: SignUpStep9Component;
    let fixture: ComponentFixture<SignUpStep9Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SignUpStep9Component]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SignUpStep9Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
