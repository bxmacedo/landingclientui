import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpStep6Component } from './sign-up-step-6.component';

describe('SignUpStep6Component', () => {
    let component: SignUpStep6Component;
    let fixture: ComponentFixture<SignUpStep6Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SignUpStep6Component]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SignUpStep6Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
