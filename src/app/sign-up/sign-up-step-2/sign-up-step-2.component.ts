import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { ErrorModalComponent } from '../../shared/error-modal/error-modal.component';

@Component({
    selector: 'app-sign-up-step-2',
    templateUrl: './sign-up-step-2.component.html',
    styleUrls: ['./sign-up-step-2.component.scss']
})
export class SignUpStep2Component implements OnInit, AfterViewInit {
    public step2: FormGroup;
    public cpf: string;
    public cpfFormatted: string;
    public cpfPattern = '[0-9]{11}';
    public validCPFFlag = false;
    public birth;
    public older: boolean;

    public fills = ['', ''];
    public blanks = ['', '', '', '', '', ''];

    constructor(
        @Inject(AppStore) private store,
        public dialog: MatDialog,
        private router: Router
    ) { }

    ngAfterViewInit() {
        document.getElementById('cpf').focus();
    }

    ngOnInit() {
        this.older = false;
        const store = this.store.getState().toJS().signUp;
        this.cpf = store.cpf;
        this.birth = store.birthday;
        this.step2 = new FormGroup({
            'cpf': new FormControl(this.cpf, [Validators.required, Validators.maxLength(14)]),
            'birth': new FormControl(this.birth, Validators.required),
        });
    }

    openModalError(msg: string) {
        this.dialog.open(ErrorModalComponent, {
            data: {
                msg: msg
            }
        });
    }

    public createSignUp(): void {
        const now = moment();
        const date = moment(this.birth);
        if (!this.older) {
           // alert('Declare ser maior de 18 anos');
           this.openModalError('Declare ser maior de 18 anos');
        } else if (date.isAfter(now.subtract('18', 'years'))) {
            alert('Data de nascimento inválida');
        } else if (this.step2.valid) {
            this.store.dispatch(ProfileActions.createSignUp(
                this.store.getState().toJS().signUp.name,
                this.cpf,
                this.birth,
                this.store.getState().toJS().signUp.email,
                this.store.getState().toJS().signUp.phone,
                this.store.getState().toJS().signUp.cel,
                this.store.getState().toJS().signUp.city,
                this.store.getState().toJS().signUp.state,
                this.store.getState().toJS().signUp.address,
                this.store.getState().toJS().signUp.number,
                this.store.getState().toJS().signUp.complement,
                this.store.getState().toJS().signUp.zipCode,
                this.store.getState().toJS().signUp.neightborhood,
                this.store.getState().toJS().signUp.mother,
                this.store.getState().toJS().signUp.password
            ));
            this.router.navigate(['/novo-cadastro/passo-3']);
        }
    }

    public formatCPF(): void {
        if (!this.cpf) {
            return;
        }
        let formattedCPF = this.cpf;
        formattedCPF = formattedCPF.replace(/\D/g, '');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d)/, '$1.$2');
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        this.cpf = formattedCPF;
    }

    public validCPF(): void {
        let Soma;
        let Resto;
        Soma = 0;

        if (!this.cpf) {
            return;
        }
        const strCPF = this.cpf.replace(/\./g, '').replace(/\-/g, '');

        if (strCPF === '00000000000') {
            this.validCPFFlag = false;
            return;
        }

        for (let i = 1; i <= 9; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (11 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(9, 10), 10)) {
            this.validCPFFlag = false;
            return;
        }

        Soma = 0;
        for (let i = 1; i <= 10; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i), 10) * (12 - i);
        }
        Resto = (Soma * 10) % 11;

        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(10, 11), 10)) {
            this.validCPFFlag = false;
            return;
        }

        this.validCPFFlag = true;
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'cpf') {
            document.getElementById('birth').focus();
            const now = moment();
            const date = moment(this.birth);
        }
    }

}
