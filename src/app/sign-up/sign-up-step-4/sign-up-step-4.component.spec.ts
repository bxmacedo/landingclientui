import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpStep4Component } from './sign-up-step-4.component';

describe('SignUpStep4Component', () => {
    let component: SignUpStep4Component;
    let fixture: ComponentFixture<SignUpStep4Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SignUpStep4Component]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SignUpStep4Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
