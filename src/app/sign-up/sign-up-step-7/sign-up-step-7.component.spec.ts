import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpStep7Component } from './sign-up-step-7.component';

describe('SignUpStep7Component', () => {
  let component: SignUpStep7Component;
  let fixture: ComponentFixture<SignUpStep7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpStep7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpStep7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
