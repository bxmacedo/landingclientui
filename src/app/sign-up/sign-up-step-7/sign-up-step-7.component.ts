import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';


@Component({
    selector: 'app-sign-up-step-7',
    templateUrl: './sign-up-step-7.component.html',
    styleUrls: ['./sign-up-step-7.component.scss']
})
export class SignUpStep7Component implements OnInit, AfterViewInit {

    public step7: FormGroup;
    public mother: string;

    public fills = ['', '', '', '', '', '', ''];
    public blanks = [''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
    ) { }

    ngAfterViewInit() {
        document.getElementById('mother').focus();
    }

    ngOnInit() {
        const store = this.store.getState().toJS().signUp;
        this.mother = store.mother;
        this.step7 = new FormGroup({
            'mother': new FormControl(this.mother, [Validators.required, Validators.minLength(2)]),
        });
    }

    public createSignUp(): void {
        this.store.dispatch(ProfileActions.createSignUp(
            this.store.getState().toJS().signUp.name,
            this.store.getState().toJS().signUp.cpf,
            this.store.getState().toJS().signUp.birthday,
            this.store.getState().toJS().signUp.email,
            this.store.getState().toJS().signUp.phone,
            this.store.getState().toJS().signUp.cel,
            this.store.getState().toJS().signUp.city,
            this.store.getState().toJS().signUp.state,
            this.store.getState().toJS().signUp.address,
            this.store.getState().toJS().signUp.number,
            this.store.getState().toJS().signUp.complement,
            this.store.getState().toJS().signUp.zipCode,
            this.store.getState().toJS().signUp.neightborhood,
            this.mother,
            this.store.getState().toJS().signUp.password
        ));
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'mother' && this.step7.value.mother !== '') {
            this.createSignUp();
            this.router.navigate(['/novo-cadastro/passo-8']);
        }
    }

}
