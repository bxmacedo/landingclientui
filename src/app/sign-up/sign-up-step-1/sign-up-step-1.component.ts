import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sign-up-step-1',
    templateUrl: './sign-up-step-1.component.html',
    styleUrls: ['./sign-up-step-1.component.scss']
})
export class SignUpStep1Component implements OnInit, AfterViewInit {

    public step1: FormGroup;
    public userName: string;

    public fills = [''];
    public blanks = ['', '', '', '', '', '', ''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
    ) { }

    ngAfterViewInit() {
        document.getElementById('name').focus();
    }

    ngOnInit() {
        const store = this.store.getState().toJS().signUp;
        this.userName = store.name;
        this.step1 = new FormGroup({
            'name': new FormControl(this.userName, Validators.required),
        });
    }

    public createSignUp(): void {
        this.store.dispatch(ProfileActions.createSignUp(
            this.userName,
            this.store.getState().toJS().signUp.cpf,
            this.store.getState().toJS().signUp.birthday,
            this.store.getState().toJS().signUp.email,
            this.store.getState().toJS().signUp.phone,
            this.store.getState().toJS().signUp.cel,
            this.store.getState().toJS().signUp.city,
            this.store.getState().toJS().signUp.state,
            this.store.getState().toJS().signUp.address,
            this.store.getState().toJS().signUp.number,
            this.store.getState().toJS().signUp.complement,
            this.store.getState().toJS().signUp.zipCode,
            this.store.getState().toJS().signUp.neightborhood,
            this.store.getState().toJS().signUp.mother,
            this.store.getState().toJS().signUp.password
        ));
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'name' && this.step1.value.name !== '') {
            this.createSignUp();
            this.router.navigate(['/novo-cadastro/passo-2']);
        }
    }
}
