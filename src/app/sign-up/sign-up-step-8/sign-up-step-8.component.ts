import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppStore } from '../../app.store';
import { ProfileActions } from '../../app.actions';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TermsModalComponent } from '../../shared/terms-modal/terms-modal.component';
import { PrivacyModalComponent } from '../../shared/privacy-modal/privacy-modal.component';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'app-sign-up-step-8',
    templateUrl: './sign-up-step-8.component.html',
    styleUrls: ['./sign-up-step-8.component.scss']
})
export class SignUpStep8Component implements OnInit, AfterViewInit {

    public step8: FormGroup;
    public password: string;
    public confirmPassword: string;
    public passStrength;
    public br: boolean;
    public privateP: boolean;
    public acceptTerms: boolean;

    public fills = ['', '', '', '', '', '', '', ''];

    constructor(
        @Inject(AppStore) private store,
        private router: Router,
        private dialog: MatDialog,
        private http: HttpClient,
    ) { }

    ngAfterViewInit() {
        document.getElementById('password').focus();
    }

    ngOnInit() {
        this.br = false;
        this.privateP = false;
        this.acceptTerms = false;
        const store = this.store.getState().toJS().signUp;
        this.password = store.password;
        this.confirmPassword = store.password;
        this.step8 = new FormGroup({
            'password': new FormControl(this.password, Validators.required),
            'confirmPassword': new FormControl(this.confirmPassword, Validators.required),
        });
    }

    public createSignUp(): void {
        if (this.confirmPassword !== this.password
            || this.step8.controls.password.status === 'INVALID'
            || this.step8.controls.confirmPassword.status === 'INVALID') {
            alert('Confira sua senha!');
        } else if (!this.br || !this.privateP) {
            alert('Declaração inválida!');
        } else if (!this.acceptTerms) {
            alert('Leia e aceite os termos de uso e política de privacidade!');
        } else if (this.passStrength < 40) {
            alert('Força da senha baixa!');
        } else {
            const store = this.store.getState().toJS().signUp;
            if (store.name === '' || store.cpf === '' || store.birthday === '' || store.email === ''
                || store.phone === '' || store.cel === '' || store.city === '' || store.state === ''
                || store.address === '' || store.number === '' || store.complement === ''
                || store.zipCode === '' || store.neightborhood === '' || store.mother === '') {
                alert('Preencha todos os campos!');
            } else {
                const postData = {
                    client_id: environment.Auth0.clientId,
                    email: this.store.getState().toJS().signUp.email,
                    password: this.password,
                    connection: 'Username-Password-Authentication',
                    user_metadata: {
                        fullname: this.store.getState().toJS().signUp.name,
                        cpf: this.store.getState().toJS().signUp.cpf,
                        mothersname: this.store.getState().toJS().signUp.mother,
                        // tslint:disable-next-line:max-line-length
                        addr_street_number_compl: this.store.getState().toJS().signUp.address + '$' + this.store.getState().toJS().signUp.number + '$' + this.store.getState().toJS().signUp.complement,
                        addr_neigh_city_state_zip: this.store.getState().toJS().signUp.neighborhood + '$' + this.store.getState().toJS().signUp.city + '$' + this.store.getState().toJS().signUp.state + '$' + this.store.getState().toJS().signUp.zipCode,
                        cell_areacode: this.store.getState().toJS().signUp.phone,
                        cell_number: this.store.getState().toJS().signUp.cel,
                        birthdate: this.store.getState().toJS().signUp.birthday,
                    }
                };

                const req = this.http.post('https://' + environment.Auth0.domain + '/dbconnections/signup', postData, {
                    headers: new HttpHeaders().set('Content-Type', 'application/json')
                });
                req.subscribe(data => {
                    this.router.navigate(['./novo-cadastro/sucesso']);
                }, err => {
                    this.router.navigate(['./novo-cadastro/sucesso']);
                });
            }
        }
    }

    public checkStrength(strength: number): void {
        this.passStrength = strength;
    }

    public keytab(event): void {
        const element = event.srcElement.id;
        if (element === 'password') {
            document.getElementById('confirmPassword').focus();
        }
        if (element === 'confirmPassword' && this.confirmPassword === this.password
            && this.step8.controls.password.status === 'VALID'
            && this.step8.controls.confirmPassword.status === 'VALID'
            && this.br && this.privateP && this.acceptTerms) {
            this.createSignUp();
        } else {
            alert('Confira os campos!');
        }
    }

    public openTermsDialog(): void {
        this.dialog.open(TermsModalComponent);
    }

    public openPrivacyDialog(): void {
        this.dialog.open(PrivacyModalComponent);
    }

}
