import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpStep8Component } from './sign-up-step-8.component';

describe('SignUpStep8Component', () => {
    let component: SignUpStep8Component;
    let fixture: ComponentFixture<SignUpStep8Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SignUpStep8Component]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SignUpStep8Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
