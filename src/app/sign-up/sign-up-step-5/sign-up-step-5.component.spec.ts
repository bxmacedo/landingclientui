import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpStep5Component } from './sign-up-step-5.component';

describe('SignUpStep5Component', () => {
    let component: SignUpStep5Component;
    let fixture: ComponentFixture<SignUpStep5Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SignUpStep5Component]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SignUpStep5Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
