import { InjectionToken } from '@angular/core';
import {
    createStore,
    Store,
    compose,
    StoreEnhancer
} from 'redux';
import { rootReducer } from './app.reducer';

export const AppStore = new InjectionToken('app.store');

export function createAppStore() {
    return createStore(
        rootReducer
    );
}

export const appStoreProviders = [
    { provide: AppStore, useFactory: createAppStore }
];
