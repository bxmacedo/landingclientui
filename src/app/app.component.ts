import { Component, Inject } from '@angular/core';
import { MktDataService } from '../api/mktdata.service';
import { BookComponent } from '../app/home/trading/book/book.component';
import { Auth0Service } from './auth/auth0.service';
import { AppStore } from './app.store';
import * as Redux from 'redux';

import { TranslateService } from '@ngx-translate/core';
import { TradeService } from '../api/trade.service';
import {Keepalive} from '@ng-idle/keepalive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public translationsLoaded = false;

    public constructor(
      private translate: TranslateService,
      @Inject(Auth0Service) public auth: Auth0Service,
      @Inject(AppStore) private store,
       private keepAlive: Keepalive
    ) {
      this.language();
      keepAlive.interval(15);
      keepAlive.onPing.subscribe(() => {

      });
      auth.handleAuthentication();
    }

    private language() {
      try {

        this.translate.addLangs(['pt-br']);
        this.translate.setDefaultLang('pt-br');

        const updateTranslationsLoaded = _ => this.translationsLoaded = true;
        this.translate.use('pt-br')
          .subscribe(updateTranslationsLoaded, updateTranslationsLoaded);
      } catch (e) { console.warn(e); }
    }
}
