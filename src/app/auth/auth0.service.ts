import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AppStore } from '../app.store';
import { ProfileActions } from '../app.actions';
import * as auth0 from 'auth0-js';
import { environment } from '../../environments/environment';


@Injectable()
export class Auth0Service {
    // Configure Auth0
    auth0 = new auth0.WebAuth({
        domain: environment.Auth0.domain,
        clientID: environment.Auth0.clientId,
        redirectUri: environment.Auth0.redirectUri,
        audience: environment.Auth0.audience,
        responseType: 'token id_token',
        scope: environment.Auth0.scope,
        prompt: 'none'
    });

    public userProfile;

    constructor(private router: Router,
        @Inject(AppStore) private store
    ) { }

    public login(username: string, password: string): void {
        this.auth0.login({
            realm: 'Username-Password-Authentication',
            username,
            password
        }, (err, authResult) => {
            if (err) {
                console.log(err);
                alert(`Error: ${err.error_description}. Check the console for further details.`);
                return;
            } else if (authResult && authResult.accessToken && authResult.idToken) {
                this.setSession(authResult);
            }
        });
    }

    public resetPassword(email): void {
        this.auth0.changePassword({
            connection: 'Username-Password-Authentication',
            email: email,
        }, (err) => {
            if (err) {
                console.log(err);
                return;
            } else {
                console.log('Password reset link sent!');
            }
        });
    }

    public signup(email: string, password: string): void {
        this.auth0.redirect.signupAndLogin({
            connection: 'Username-Password-Authentication',
            email,
            password,
        }, err => {
            if (err) {
                console.log(err);
                alert(`Error: ${err.description}. Check the console for further details.`);
                return;
            }
        });
    }

    public handleAuthentication(): void {
        this.auth0.parseHash((err, authResult) => {
            if (authResult && authResult.accessToken && authResult.idToken) {
                this.setSession(authResult);
            } else if (err) {
                this.router.navigate(['/home']);
                console.log(err);
                alert(`Error: ${err.error}. Check the console for further details.`);
            }
        });
    }

    private setSession(authResult): void {
        // Set the time that the access token will expire at
        const expiresAt = JSON.stringify(
            (authResult.expiresIn * 1000) + new Date().getTime()
        );
        localStorage.setItem('access_token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);
        this.router.navigate(['/home/trade']);
    }

    public logout(): void {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        // Go back to the home route
        this.router.navigate(['/']);
    }

    public isAuthenticated(): boolean {
        // Check whether the current time is past the
        // access token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;
    }

    public getProfile(): void {
        const accessToken = localStorage.getItem('access_token');
        if (!accessToken) {
            throw new Error('Access token must exist to fetch profile');
        }

        this.auth0.client.userInfo(accessToken, (err, profile) => {
            if (profile) {
                this.userProfile = profile;
                this.store.dispatch(ProfileActions.saveProfile(profile));
            }
        });

    }

    public bootstrap(): void {
        const idToken = localStorage.getItem('id_token');
        const accessToken = localStorage.getItem('access_token');
        const expiresAt = localStorage.getItem('expires_at');
        if (!idToken || !accessToken || !expiresAt) {
            // this.login();
        } else {
            this.store.dispatch(ProfileActions.saveAuth(accessToken, idToken, expiresAt));
        }
    }
}
